<?php 
    include '../config.php';
    if ($logged) {
        header("Location: $base_url/");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />
		<style>
			body {
				background-image: url("../images/login-bg.jpg");
			}
		</style>
    </head>
    <body>
        <?php include '../header.php'; ?>
        <div class="container" style="margin-top: 0px; padding-top: 10px;">
            <div class="row">
                <div class="col-md-5">
                    <div class="panel panel-success shadow">
                        <div class="panel-heading" style="padding: 20px !important;">
                            <img src="../images/user-login.png" width="30px" style="margin-right: 10px; vertical-align: middle; position: absolute;" /> <font style="margin-left: 40px; color: #1e5b00;" size="5">Login</font>
                        </div>
                        <form method="post" action="login.php">
                            <div class="panel-body">                                
                                <?php
                                    if (isset($_GET['error'])) {
                                        if ($_GET['error'] == "1") { ?>
                                <div id="err1">
                                    <div class="alert alert-warning">
                                        <b>Error:</b> The account you're trying to access does not exists.
                                    </div>
                                </div>
                                        <?php }
                                    }
                                ?>
                                <?php
                                    if (isset($_GET['alert'])) {
                                        if ($_GET['alert'] == 'not_logged') { ?>
                                <div class="alert alert-warning">
                                    You must login first!
                                </div> 
                                        <?php }
                                    }
                                ?>
                                <label for="username">Usernane:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </span>
                                    <input type="text" name="username" class="form-control" placeholder="username" required />
                                </div>
                                <label for="username">Password:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </span>
                                    <input type="password" name="password" class="form-control" placeholder="*************" required />
                                </div><br>
                                <a href="">Forgot password?</a>
                            </div>
                            <div class="panel-footer">
                                <button data-loading-text="Logging In..." class="btn btn-success js-loading-button" type="submit">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include '../footer.php'; ?>
        <script>
            $(function () {
            $('.js-loading-button').on('click', function () {
            var btn = $(this).button('loading')
            setTimeout(function (){
            btn.button('reset')
            }, 10000)
            })
            })
        </script>
        <script src="../assets/js/button.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
