<nav class="navbar navbar-congrad navbar-static-top shadow" style="margin-bottom: 0px; position: fixed; width: 100%; display: block;">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" style="padding-left: 15px; padding-right: 15px;" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span style="color: #2b9600;" class="glyphicon glyphicon-list"></span>
            </button>
            <a class="navbar-brand" style="padding: 2px !important; margin-right: 20px;" href="<?php echo $base_url. '/'; ?>"><img src="<?php echo $base_url ?>/images/banner.png" width="200px" alt="Congrad"/></a>            
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo $base_url. '/' ?>" style="color: #ffffff;">Home</a></li>
                <?php
                if ($not_logged)
                { ?>
                <li><a href="<?php echo $base_url ?>/login/" style="color: #ffffff;">Sign In</a></li>
                <?php } ?>
                <?php
                if ($logged)
                { ?>
                <li><a style="color: #ffffff;" href="<?php echo $base_url ?>/events/">Events</a></li>
                <li><a style="color: #ffffff;" href="<?php echo $base_url ?>/gallery/">Gallery</a></li>
                <li class="dropdown">
                    <a href="#" style="color: #ffffff;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Alumni <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="border: 1px solid #eeeeee;">
                        <?php if ($logged_admin) { ?>
                        <li>
                            <a href="<?php echo $base_url ?>/alumni/" class="nav-list-color-white">Manage Graduates</a>
                        </li>
                        <?php } ?>
                        <li>
                            <a style="color: #000000;" href="<?php echo $base_url ?>/alumni/alumni-registered.php">Alumni Registered</a>
                        </li>
                        <li>
                            <a href="<?php echo $base_url ?>/alumni/employments/" class="nav-list-color-white">Employment Status</a>
                        </li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
            <style>
                input[name='search_alumni']:focus {
                    -webkit-box-shadow: 0px 0px 0px 0px rgba(50, 50, 50, 0.15);
                    -moz-box-shadow: 0px 0px 0px 0px rgba(50, 50, 50, 0.15);
                    box-shadow: 0px 0px 0px 0px rgba(50, 50, 50, 0.15);
                }
                .input-group:focus {
                    -webkit-box-shadow: 0px 10px 0px 0px rgba(50, 50, 50, 0.15);
                    -moz-box-shadow: 0px 10px 0px 0px rgba(50, 50, 50, 0.15);
                    box-shadow: 0px 10px 0px 0px rgba(50, 50, 50, 0.15);
                }
            </style>
            <?php if ($logged) { ?>
            <!--<form method="get" action="<?php echo $base_url ?>/alumni/search-alumni.php" class="navbar-form navbar-left" role="search" >
                <div class="input-group">
                    <span class="input-group-addon" style="background-color: #ffffff; border: 0px solid #ffffff;">
                    <span class="glyphicon glyphicon-search"></span>
                </span>
                    <input style="border: 0px solid #ffffff; width: 300px;" type="text" name="search_alumni" class="form-control" placeholder="Search Alumni" required />
                </div>
            </form>-->
            <?php } ?>
            <?php
            if ($logged)
            { ?>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo $base_url ?>/messages/" style="color: #ffffff"><small><span class="glyphicon glyphicon-envelope"></span></small> Message(s) <span class="badge"><?php echo $count_new_messages ?></span></a></li>
                <li class="dropdown">
                    <a href="#" style="color: #ffffff;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $logged_username; ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="border: 1px solid #eeeeee;">
                        <li><a href="<?php echo $user_profile_link ?>" class="nav-list-color-white"><img src="<?php echo $base_url ?>/profile/<?php echo $user_dp_link ?>" width="25px" style="border: 1px solid #ffffff" class="#ffffff" /> View My Profile</a></li>
                        <?php
                            if ($logged) { ?>
                        <li>
                            <a href="<?php echo $base_url ?>/user-settings/" style="color: #000000;"><span class="glyphicon glyphicon-edit"></span> User Settings</a>
                        </li>
                            <?php }
                        ?>
                        <li><a href="<?php echo $base_url. '/logout.php' ?>" class="nav-list-color-white"><small><span class="glyphicon glyphicon-log-out"></span></small> Logout</a></li>
                    </ul>
                </li>
            </ul>
            <?php } ?>
        </div>
    </div>
</nav>
<style>
    .panel {
        border-radius: 0px;
    }
    .panel-heading {
        border-radius: 0px;
    }
</style>
<div style="padding-top: 50px;"></div>
<?php
    if ($logged) {
        if ($no_prof_link) { ?>
<div class="container" style="padding: 0px; margin-top: 20px; margin-bottom: 0px;">
    <div class="alert alert-warning">Unable to access profile. Please generate first your own profile link. <a href="<?php echo $base_url ?>/profile/generate.php">Click here.</a></div>
</div>
        <?php }
    }
?>