<?php
    include '../../config.php';
?>
<?php
    if (isset($_GET['msg_id'])) {
        $msg_id = $_GET['msg_id'];
        
        /* get the message data */
        $message = $mysqli->query("SELECT * FROM messages WHERE msg_id = $msg_id");
        $message_data = $message->fetch_assoc();
        
        /* Update message status from unread to read */
        $update_msg_statsu = $mysqli->query("UPDATE messages SET msg_status = 'read' WHERE msg_id = $msg_id");
        
        /* Get the sender info */
        $sender_info = $mysqli->query("SELECT * FROM graduates WHERE graduate_id = ".$message_data['msg_sender_graduate_id']."");
        $sender_info_data = $sender_info->fetch_assoc();
    } else {
        header("Location: ../");
    }
?>
<?php
    $count_all_messages = $mysqli->query("SELECT * FROM messages WHERE msg_receiver_graduate_id = $user_graduate_id");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Message from <?php echo $sender_info_data['graduate_firstname'] . ' ' . $sender_info_data['graduate_middlename'] . ' ' . $sender_info_data['graduate_surname']; ?></title>
        <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/msgs.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />
    </head>
    <body>
        <!-- Reply modal -->
        <div id="reply" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h5 class="modal-title"><span class="glyphicon glyphicon-envelope"></span> Repy to: <b><?php echo $sender_info_data['graduate_firstname'] . ' ' . $sender_info_data['graduate_middlename'] . ' ' . $sender_info_data['graduate_surname']; ?></b></h5>
                    </div>
                    <form method="post" action="reply.php?sender_id=<?php echo $user_graduate_id ?>&receiver_id=<?php echo $message_data['msg_sender_graduate_id'] ?>&msg_id=<?php echo $msg_id ?>">
                        <div class="modal-body">
                            <textarea name="msg_reply" class="form-control" required></textarea>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="reset" class="btn btn-danger">Reset</button>
                                <button type="submit" class="btn btn-primary">Reply</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include '../../header.php'; ?>
        <div class="container" style="padding-top: 10px;">
        <?php include '../modals.php'; ?>
        <div class="mail-box">
            <aside class="sm-side">
                <div class="user-head">
                    <a class="inbox-avatar">
                        <img src="../../profile/<?php echo $user_dp_link ?>" width="55px" />
                    </a>
                    <div class="user-name">
                        <h5><?php echo $logged_fullname; ?></h5>
                        <span>Messages (<?php echo $count_new_messages ?>/<?php echo $count_all_messages->num_rows; ?>)</span>
                    </div>
                </div>
                <div class="inbox-body">
                    <a href="../index.php" style="padding: 10px; margin-top: 5px;" class="btn btn-primary btn-block">Back to Messages</a>
                </div>
            </aside>
            <aside class="lg-side">
                <div class="inbox-head">
                    <h3><span class="glyphicon glyphicon-envelope"></span> Messages</h3>
                </div>
                <div class="inbox-body">
                    <?php
                        if (isset($_GET['alert'])) {
                            if ($_GET['alert'] == 'sent') { ?>
                                <div class="alert alert-success">
                                    Message Sent!
                                </div>
                            <?php }
                        }
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span class="glyphicon glyphicon-envelope"></span> From: <b><?php echo $sender_info_data['graduate_firstname'] . ' ' . $sender_info_data['graduate_middlename'] . ' ' . $sender_info_data['graduate_surname']; ?></b></h5>
                        </div>
                        <div class="panel-body">
                            <?php
                                echo $message_data['msg_content'];
                            ?>
                        </div>
                        <div class="panel-footer">
                            <a data-toggle="modal" data-target="#reply" class="btn btn-default"><span class="glyphicon glyphicon-send"></span> Reply</a>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
        </div>
        <?php include '../../footer.php'; ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>