<div id="newPM" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
                <h5 style="margin: 0px;"><small><span data-toggle="tooltip" data-placement="top" title="Send PM" class="glyphicon glyphicon-envelope"></span></small> New Message</h5>
            </div>
            <form role="form" action="" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="subject">Subject:</label>
                        <input type="text" class="form-control" name="subject" />
                    </div>
                    <div class="form-group">
                        <label for="msg">Message:</label>
                        <textarea name="msg" style="margin-bottom: 0px !important; min-height: 200px;" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="reset" class="btn btn-warning"><small><span class="glyphicon glyphicon-refresh"></span></small> Reset</button>
                        <button type="submit" class="btn btn-primary"><small><span class="glyphicon glyphicon-send"></span></small> Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>