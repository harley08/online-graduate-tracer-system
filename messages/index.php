<?php include '../config.php'; ?>
<?php
    if ($not_logged) {
        header("Location: ../login/");
    }
?>
<?php
    $count_all_messages = $mysqli->query("SELECT * FROM messages WHERE msg_receiver_graduate_id = $user_graduate_id");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Messages</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/msgs.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />
    </head>
    <body>
        <?php include '../header.php'; ?>
        <div class="container" style="padding-top: 10px;">
        <?php include 'modals.php'; ?>
            <div class="mail-box" style="margin-bottom: 15px;">
            <aside class="sm-side">
                <div class="user-head">
                    <a href="<?php echo $user_profile_link ?>" class="inbox-avatar">
                        <img src="../profile/<?php echo $user_dp_link ?>" width="55px" />
                    </a>
                    <div class="user-name">
                        <h5><?php echo $logged_fullname; ?></h5>
                        <span>Messages (<?php echo $count_new_messages ?>/<?php echo $count_all_messages->num_rows; ?>)</span>
                    </div>
                </div>
            </aside>
            <aside class="lg-side">
                <div class="inbox-head">
                    <h3><span class="glyphicon glyphicon-envelope"></span> Messages</h3>
                </div>
                <div class="inbox-body">
                    <div style="margin-bottom: 15px;" align="right">
                        <b><?php echo $count_all_messages->num_rows; ?></b> total messages
                    </div>
                    <table class="table table-inbox table-hover">
                        <tbody>
                            <?php
                                $messages = $mysqli->query("SELECT * FROM messages WHERE msg_receiver_graduate_id = $user_graduate_id ORDER BY msg_id DESC");
                                while ($message_data = $messages->fetch_assoc()) {
                                    
                                    /* Get the sender info */
                                    $sender_info = $mysqli->query("SELECT * FROM graduates WHERE graduate_id = ".$message_data['msg_sender_graduate_id']."");
                                    $sender_info_data = $sender_info->fetch_assoc();
                                    
                                    if ($message_data['msg_status'] == 'unread') { ?>
                            <tr class="unread">
                                <td>
                                    <!--<input type="checkbox" />--> <span class="glyphicon glyphicon-envelope"></span> <?php echo $sender_info_data['graduate_firstname'] . ' ' . $sender_info_data['graduate_middlename'] . ' ' . $sender_info_data['graduate_surname']; ?>
                                </td>
                                <td>
                                    <?php echo $message_data['msg_content'] ?>
                                </td>
                                <td>
                                    <?php
                                        echo date("F j, Y h:m A", strtotime($message_data['msg_date']));
                                    ?>
                                </td>
                                <td>
                                    <a href="action/view-message.php?sender_id=<?php echo $message_data['msg_sender_graduate_id'] ?>&msg_id=<?php echo $message_data['msg_id'] ?>">View</a>
                                </td>
                            </tr>
                                    <?php } else { ?>
                            <tr>
                                <td>
                                    <!--<input type="checkbox" />--> <span class="glyphicon glyphicon-envelope"></span> <?php echo $sender_info_data['graduate_firstname'] . ' ' . $sender_info_data['graduate_middlename'] . ' ' . $sender_info_data['graduate_surname']; ?>
                                </td>
                                <td>
                                    <?php echo $message_data['msg_content'] ?>
                                </td>
                                <td>
                                    <?php
                                        echo date("F j, Y h:m A", strtotime($message_data['msg_date']));
                                    ?>
                                </td>
                                <td>
                                    <a href="action/view-message.php?sender_id=<?php echo $message_data['msg_sender_graduate_id'] ?>&msg_id=<?php echo $message_data['msg_id'] ?>">View</a>
                                </td>
                            </tr>
                            <?php }
                                }
                            ?>
                        </tbody>
                    </table>
                    <!--<div style="margin-top: 15px;">
                        <input type="checkbox" /> Check All <a href=""><small><span style="margin-left: 10px;" class="glyphicon glyphicon-trash"></span></small> Delete</a> <a href=""><small><span style="margin-left: 10px;" class="glyphicon glyphicon-envelope"></span></small> Mark as unread</a>
                    </div>-->
                </div>
            </aside>
        </div>
        </div>
        <?php include '../footer.php'; ?>
        <script>
            $(function(){
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // Get the name of active tab
            var activeTab = $(e.target).text();
            // Get the name of previous tab
            var previousTab = $(e.relatedTarget).text();
            $(".active-tab span").html(activeTab);
            $(".previous-tab span").html(previousTab);
            }); 
            });
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
