<!-- Begin Update Account Info Modal -->
<div id="updateAccount" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hiddem="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title"><span class="glyphicon glyphicon-edit"></span> Update Account Information</h5>
            </div>
            <form method="post" action="action/update-account.php">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="text" name="username" class="form-control" value="<?php echo $logged_username ?>" />
                    </div>
                    <div class="form-group">
                        <label for="current_pw">Current Password:</label>
                        <input type="password" name="current_pw" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="new_pw">New Password:</label>
                        <input type="password" name="new_pw" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="re_enter_new_pw">Re-enter New Password:</label>
                        <input type="password" name="re_enter_new_pw" class="form-control" />
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">                        
                        <button type="reset" class="btn btn-danger">Reset</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Update Account Info Modal -->

<!-- Begin Update Personal Info Modal -->
<div id="updatePersonal" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">
                    <span class="glyphicon glyphicon-edit"></span> Update Personal Information
                </h5>
            </div>
            <?php
                $mydata = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = $user_graduate_id");
                $my_data = $mydata->fetch_assoc();
            ?>
            <form method="post" action="action/update-personal-info.php">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="surname">Surname:</label>
                        <input type="text" name="surname" class="form-control" value="<?php echo $my_data['alum_surname'] ?>" />
                    </div>
                    <div class="form-group">
                        <label for="first_name">First Name:</label>
                        <input type="text" name="first_name" class="form-control" value="<?php echo $my_data['alumn_firstname'] ?>" />
                    </div>
                    <div class="form-group">
                        <label for="middle_name">Middle Name:</label>
                        <input type="text" name="middle_name" class="form-control" value="<?php echo $my_data['alum_middlename'] ?>" />
                    </div>
                    <div class="form-group">
                        <label for="bday">Birthday:</label>
                        <input type="date" class="form-control" name="bday" value="<?php echo $my_data['alum_birthday'] ?>" />
                    </div>
                    <div class="form-group">
                        <label for="gender">Gender:</label>
                        <?php
                            if ($my_data['alum_gender'] == "Male") { ?>
                                <input type="radio" name="gender" value="Male" checked /> Male
                                <input type="radio" name="gender" value="Female" />Female
                            <?php } else { ?>
                                <input type="radio" name="gender" value="Male" /> Male
                                <input type="radio" name="gender" value="Female" checked />Female
                            <?php }
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="age">Age:</label>
                        <input type="number" name="age" class="form-control" value="<?php echo $my_data['alum_age'] ?>" />
                    </div>
                    <div class="form-group">
                        <label for="civil_status">Civil Status:</label>
                        <select name="civil_status" class="form-control">
                            <option value="<?php echo $my_data['alum_civil_status'] ?>"><?php echo $my_data['alum_civil_status'] ?></option>
                            <option value="">-------------------------------------------------------------------------------------------------------------------------------------------------------------</option>
                            <option value="">-- Select --</option>
                            <option value="Single">Single</option>
                            <option value="Maried">Maried</option>
                            <option value="Separated">Separated</option>
                            <option value="Single Parent">Single Parent</option>
                            <option value="Window or Widower">Window or Widower</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="present_address">Present Address:</label>
                        <input type="text" name="present_address" class="form-control" value="<?php echo $my_data['alum_present_address'] ?>" />
                    </div>
                    <div class="form-group">
                        <label for="permanent_address">Permanent Address:</label>
                        <input type="text" name="permanent_address" class="form-control" value="<?php echo $my_data['alum_permanent_address'] ?>" />
                    </div>
                    <div class="form-group">
                        <label for="contact">Contact #:</label>
                        <input type="text" name="contact" class="form-control" value="<?php echo $my_data['alum_contact_phone'] ?>" />
                    </div>
                    <div class="form-group">
                        <label for="landline">Landline #:</label>
                        <input type="text" name="landline" class="form-control" value="<?php echo $my_data['alum_contact_landline'] ?>" />
                    </div>
                    <div class="form-group">
                        <label for="email">Email Address:</label>
                        <input type="email" class="form-control" name="email" placeholder="@" value="<?php echo $my_data['alum_email'] ?>" />
                     </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="reset" class="btn btn-danger">Reset</button>
                        <button type="submit" class="btn btn-primary" name="updateInfo">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Update Personal Info Modal -->

<!-- Begin Add Education Modal -->
<div id="addEduc" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">
                    <span class="glyphicon glyphicon-education"></span> Add Education
                </h5>
            </div>
            <form method="post" action="action/add-education.php">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="school">School:</label>
                        <input type="text" class="form-control" name="school" />
                    </div>
                    <div class="form-group">
                        <label for="school">Course:</label>
                        <input type="text" name="course" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="major">Major</label>
                        <input type="text" class="form-control" name="major" />
                    </div>
                    <div class="form-group">
                        <label for="year_graduated">Year Graduated:</label>
                        <input type="text" name="year_graduated" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="educ_level">Education Level:</label>
                        <select name="educ_level" class="form-control">
                            <option value="Graduate Study">Graduate Study</option>
                        </select>
                    </div>
                </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="reset" class="btn btn-danger">Reset</button>
                    <button type="submit" class="btn btn-primary" name="add_education">Add</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Education Modal -->

<!-- Start Add Employed Status Modal -->
<div id="addEmployed" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">
                    <span class="glyphicon glyphicon-tasks"></span> Add Employed Status
                </h5>
            </div>
            <form method="post" action="action/add-employed.php">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="date_from">From:</label>
                        <input type="date" class="form-control" name="date_from" />
                    </div>
                    <div class="form-group">
                        <label for="date_to">To:</label>
                        <input type="date" class="form-control" name="date_to" />
                    </div>
                    <div class="form-group">
                        <label for="company_name">Company Name:</label>
                        <input type="text" class="form-control" name="company_name" />
                    </div>
                    <div class="form-group">
                        <label for="company_address">Company Address:</label>
                        <input type="text" class="form-control" name="company_address" />
                    </div>
                    <div class="form-group">
                        <label for="company_position">Company Position:</label>
                        <input type="text" class="form-control" name="company_position" />
                    </div>
                    <div class="form-group">
                        <label for="employment_status">Employment Status:</label>
                        <select name="employment_status" class="form-control" required>
                            <option value="">-- Select --</option>
                            <option value="Permanent">Permanent</option>
                            <option value="Contractual">Contractual</option>
                            <option value="Casual">Casual</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="reset" class="btn btn-danger">Reset</button>
                        <button type="submit" class="btn btn-primary" name="add_employed">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Employed Status Modal -->

<!-- Start Add Self-Employed Status -->
<div id="addSelf_Employed" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">
                    <span class="glyphicon glyphicon-tasks"></span> Add Self-Employed Status
                </h5>
            </div>
            <form method="post" action="action/add-self-employed.php">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="business_type">Business Type:</label>
                        <input type="text" name="business_type" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="date">Since when?</label>
                        <input type="date" name="date" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="reasons">Reasons:</label>
                        <textarea name="reasons" class="form-control"></textarea>
                    </div>
                </div>
                    <div class="modal-footer">
                        <div class="btn-group">
                            <button type="reset" class="btn btn-danger">Reset</button>
                            <button type="submit" class="btn btn-primary" name="add_self_employed">Add</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Self-Employed Status -->

<!-- Start Add Unemployed Status -->
<div id="addUnemployed" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">
                    <span class="glyphicon glyphicon-tasks"></span> Add Unemployed Status
                </h5>
            </div>
            <form method="post" action="action/add-unemployed.php">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="date">Since when?</label>
                        <input type="date" name="date" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="reasons">Reasons:</label>
                        <textarea class="form-control" name="reasons"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="reset" class="btn btn-danger">Reset</button>
                        <button type="submit" class="btn btn-primary" name="add_unemployed">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Unemployed Status -->