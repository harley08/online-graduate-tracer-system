<?php include '../config.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>User Settings</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/msgs.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />
    </head>
    <body>
        <?php include '../header.php'; ?>
        <div class="container" style="padding-top: 10px !important;">
            <div class="panel panel-default shadow">
                <div class="panel-heading">
                    <h5 class="panel-title"><span class="glyphicon glyphicon-edit"></span> User Settings</h5>
                </div>
                <div class="panel-body">
                    <?php include 'modals.php'; ?>
                    <div class="alert alert-info">
                        <span class="glyphicon glyphicon-info-sign"></span> User Information
                    </div>
                    <h5><span class="glyphicon glyphicon-lock"></span> Account Information <small><a data-toggle="modal" data-target="#updateAccount"><span style="margin-left: 15px;" class="glyphicon glyphicon-pencil"></span></a></small></h5>
                    <p>
                        <b style="margin-right: 20px;">Username:</b> <?php echo $logged_username ?><br>
                        <b style="margin-right: 20px;">Password:</b> **********<br>
                    </p>
                    <hr>
                    <h5><span class="glyphicon glyphicon-user"></span> Personal Infomation <small><a data-toggle="modal" data-target="#updatePersonal"><span style="margin-left: 15px;" class="glyphicon glyphicon-pencil"></span></a></small></h5>
                    <?php
                        $profile_info = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = $user_graduate_id");
                        $profInfo_data = $profile_info->fetch_assoc();
                    ?>
                    <p>
                        <b style="margin-right: 20px;">Name:</b> <?php echo $logged_fullname ?><br>
                        <b style="margin-right: 20px;">Gender:</b> <?php echo $profInfo_data['alum_gender'] ?><br>
                        <b style="margin-right: 20px;">Birthday:</b> <?php echo $profInfo_data['alum_birthday'] ?><br>
                        <b style="margin-right: 20px;">Age:</b> <?php echo $profInfo_data['alum_age'] ?><br>
                        <b style="margin-right: 20px;">Civil Status:</b> <?php echo $profInfo_data['alum_civil_status'] ?><br>
                        <b style="margin-right: 20px;">Present Address:</b> <?php echo $profInfo_data['alum_present_address'] ?><br>
                        <b style="margin-right: 20px;">Permanent Address:</b> <?php echo $profInfo_data['alum_permanent_address'] ?><br>
                        <b style="margin-right: 20px;">Contact #:</b> <?php echo $profInfo_data['alum_contact_phone'] ?><br>
                        <b style="margin-right: 20px;">Landline #:</b> <?php echo $profInfo_data['alum_contact_landline'] ?><br>
                        <b style="margin-right: 20px;">Email:</b> <?php echo $profInfo_data['alum_email'] ?>
                    </p>
                    <hr>
                    <div class="pull-right">
                        <a data-toggle="modal" data-target="#addEduc"><small><span class="glyphicon glyphicon-education"></span></small> Add Education</a>
                    </div>
                    <h5><span class="glyphicon glyphicon-education"></span> Educational Background</h5>
                    <table class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>School</th>
                                <th>Course</th>
                                <th>Major</th>
                                <th>Year Graduated</th>
                                <th>Education Level</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $educational_background = $mysqli->query("SELECT * FROM alumni_educational_background WHERE graduate_id = $user_graduate_id");
                                while($educ_data = $educational_background->fetch_assoc()) { ?>
                            <div id="editEducation<?php echo $educ_data['education_id'] ?>" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                            <h5 class="modal-title">
                                                <span class="glyphicon glyphicon-education"></span> <?php echo $educ_data['education_level'] ?>
                                            </h5>
                                        </div>
                                        <form method="post" action="action/update-education.php?id=<?php echo $educ_data['education_id'] ?>">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="school">School:</label>
                                                    <input type="text" name="school" class="form-control" value="<?php echo $educ_data['school'] ?>" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="course">Course:</label>
                                                    <input type="text" name="course" class="form-control" value="<?php echo $educ_data['course'] ?>" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="major">Major:</label>
                                                    <input type="text" name="major" class="form-control" value="<?php echo $educ_data['major'] ?>" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="year_graduated">Year Graduated:</label>
                                                    <input type="text" name="year_graduated" class="form-control" value="<?php echo $educ_data['year_graduated'] ?>" />
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="btn-group">
                                                    <button type="reset" class="btn btn-danger">Reset</button>
                                                    <button type="submit" class="btn btn-primary" name="update_education">Update</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <tr>
                                <td>
                                    <small><?php echo $educ_data['school'] ?></small>
                                </td>
                                <td>
                                    <small>
                                        <?php echo $educ_data['course'] ?>
                                    </small>
                                </td>
                                <td>
                                    <small>
                                        <?php echo $educ_data['major'] ?>
                                    </small>
                                </td>
                                <td>
                                    <small>
                                        <?php echo $educ_data['year_graduated'] ?>
                                    </small>
                                </td>
                                <td>
                                    <small>
                                        <?php echo $educ_data['education_level'] ?>
                                    </small>
                                </td>
                                <td>
                                    <a data-toggle="modal" data-target="#editEducation<?php echo $educ_data['education_id'] ?>"><span class="glyphicon glyphicon-edit"></span></a>
                                </td>
                            </tr>
                                <?php }
                            ?>
                        </tbody>
                    </table>
                    <hr>
                    <div class="pull-right">
                        <ul>
                            <li class="dropdown">
                                <a href="#" id="dropwork" role="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <small><span class="glyphicon glyphicon-tasks"></span></small> Add Work <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dropwork" style="border: 1px solid #ffffff;">
                                    <li><a data-toggle="modal" data-target="#addEmployed">Employed</a></li>
                                    <li><a data-toggle="modal" data-target="#addSelf_Employed">Self-Employed</a></li>
                                    <li><a data-toggle="modal" data-target="#addUnemployed">Unemployed</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <h5>
                        <span class="glyphicon glyphicon-tasks"></span> Work Experience
                    </h5>
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#employed" data-toggle="tab">Employed</a>
                        </li>
                        <li>
                            <a href="#unemployed" data-toggle="tab">Unemployed</a>
                        </li>
                        <li>
                            <a href="#self_employed" data-toggle="tab">Self Employed</a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab tab-content">
                        <div class="tab-pane active" id="employed">
                            <table class="table table-striped">
                                <?php 
                                $employed_tbl = $mysqli->query("SELECT * FROM work_experiences_employed WHERE graduate_id = $user_graduate_id");
                                $count_employed_tbl = $employed_tbl->num_rows;
                                        if ($count_employed_tbl == null) { ?>
                                    <tr class="table table-striped" valign="bottom">
                                        <div style="position: relative; text-align: center; padding: 20px;">no data available</div>
                                    </tr>
                                        <?php } else {
                                    ?>
                                <thead>
                                    <tr>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Company Name</th>
                                        <th>Company Address</th>
                                        <th>Position</th>
                                        <th>Employment Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php                                       
                                        while ($employed_tbl_data = $employed_tbl->fetch_assoc()) {
                                    ?>
                                <div id="updateEmployed<?php echo $employed_tbl_data['employed_id'] ?>" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                                <h5 class="modal-title">
                                                    <span class="glyphicon glyphicon-tasks"></span> Update Employed
                                                </h5>
                                            </div>
                                            <form method="post" action="action/update-employed.php?id=<?php echo $employed_tbl_data['employed_id'] ?>">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="date_from">Date From:</label>
                                                        <input type="date" class="form-control" name="date_from" value="<?php echo $employed_tbl_data['date_from'] ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="date_to">Date To:</label>
                                                        <input type="date" class="form-control" name="date_to" value="<?php echo $employed_tbl_data['date_to'] ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="company_name">Company Name:</label>
                                                        <input type="text" name="company_name" class="form-control" value="<?php echo $employed_tbl_data['company_name'] ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="company_address">Company Address:</label>
                                                        <input type="text" name="company_address" class="form-control" value="<?php echo $employed_tbl_data['company_address'] ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="position">Position:</label>
                                                        <input type="text" name="position" class="form-control" value="<?php echo $employed_tbl_data['company_position'] ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="employment_status">Employment Status:</label>
                                                        <select name="employment_status" class="form-control">
                                                            <option value="<?php echo $employed_tbl_data['employment_status'] ?>"><?php echo $employed_tbl_data['employment_status'] ?></option>
                                                            <option value="">-- Select --</option>
                                                            <option value="Permannent">Permanent</option>
                                                            <option value="Contractual">Contractual</option>
                                                            <option value="Casual">Casual</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="btn-group">
                                                        <button type="reset" class="btn btn-danger">Reset</button>
                                                        <button type="submit" class="btn btn-primary" name="updateEmployed">Update</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                    <tr>
                                        <td><small><?php echo $employed_tbl_data['date_from'] ?></small></td>
                                        <td><small><?php echo $employed_tbl_data['date_to'] ?></small></td>
                                        <td><small><?php echo $employed_tbl_data['company_name'] ?></small></td>
                                        <td><small><?php echo $employed_tbl_data['company_address'] ?></small></td>
                                        <td><small><?php echo $employed_tbl_data['company_position'] ?></small></td>
                                        <td><small><b><?php echo $employed_tbl_data['employment_status'] ?></b></small></td>
                                        <td>
                                            <a data-toggle="modal" data-target="#updateEmployed<?php echo $employed_tbl_data['employed_id'] ?>"><span class="glyphicon glyphicon-edit"></span></a>
                                        </td>
                                    </tr>
                                <?php } } ?>                              
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="unemployed">
                            <table border="0" class="table table-striped">
                                <?php
                                $unemployed_tbl = $mysqli->query("SELECT * FROM work_experiences_unemployed WHERE graduate_id = $user_graduate_id");
                                $count_unemployed_tbl = $unemployed_tbl->num_rows;
                                if ($count_unemployed_tbl == null) { ?>
                                <tr class="table table-striped" valign="bottom">
                                    <div style="position: relative; text-align: center; padding: 20px;">no data available</div>
                                </tr>
                                <?php } else {
                                ?>
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Reason</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    while ($unemployed_tbl_data = $unemployed_tbl->fetch_assoc()) {
                                    ?>
                                <div id="update_unemployed<?php echo $unemployed_tbl_data['unemployed_id'] ?>" tabindex="-1" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hdden="true">x</button>
                                                <h5 class="modal-title">
                                                    <span class="glyphicon glyphicon-tasks"></span> Update Unemployed
                                                </h5>
                                            </div>
                                            <form method="post" role="form" action="action/update-unemployed.php?id=<?php echo $unemployed_tbl_data['unemployed_id'] ?>">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="date">Date:</label>
                                                        <input type="date" name="date" class="form-control" value="<?php echo $unemployed_tbl_data['unemployed_date'] ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reasons">Reasons:</label>
                                                        <textarea name="reasons" class="form-control"><?php echo $unemployed_tbl_data['unemployed_reasons'] ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="btn-group">
                                                        <button type="reset" class="btn btn-danger">Reset</button>
                                                        <button type="submit" class="btn btn-primary" name="update_unemployed">Update</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                    <tr>
                                        <td><?php echo $unemployed_tbl_data['unemployed_date'] ?></td>
                                        <td><?php echo $unemployed_tbl_data['unemployed_reasons'] ?></td>
                                        <td>
                                            <a data-toggle="modal" data-target="#update_unemployed<?php echo $unemployed_tbl_data['unemployed_id'] ?>"><span class="glyphicon glyphicon-edit"></span></a>
                                        </td>
                                    </tr>                             
                                <?php } } ?>                                    
                                </tbody>   
                            </table>
                        </div>
                        <div class="tab-pane" id="self_employed">
                            <table class="table table-striped">
                                <thead>
                                <?php 
                                    $self_employed_tbl = $mysqli->query("SELECT * FROM work_experiences_self_employed WHERE graduate_id = $user_graduate_id");
                                    $count_self_employed_tbl = $self_employed_tbl->num_rows;
                                    if ($count_self_employed_tbl == null) { ?>
                                     <tr class="table table-striped" valign="bottom">
                                            <div style="position: relative; text-align: center; padding: 20px;">no data available</div>
                                    </tr>   
                                    <?php } else {
                                ?>
                                    <tr>
                                        <th>Date</th>
                                        <th>Business Type</th>
                                        <th>Reason</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while($self_employed_tbl_data = $self_employed_tbl->fetch_assoc()) {
                                    ?>
                                <div id="update_self_employed<?php echo $self_employed_tbl_data['self_employed'] ?>" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                                <h5 class="modal-title">
                                                    <span class="glyphicon glyphicon-tasks"></span> Update Self-Employed
                                                </h5>
                                            </div>
                                            <form role="form" method="post" action="action/update-self-employed.php?id=<?php echo $self_employed_tbl_data['self_employed'] ?>">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="date">Since when?</label>
                                                        <input type="date" name="date" class="form-control" value="<?php echo $self_employed_tbl_data['self_employed_date'] ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <lsbel for="business_type">Business Type:</lsbel>
                                                        <input type="text" name="business_type" class="form-control" value="<?php echo $self_employed_tbl_data['self_employed_business_type'] ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reasons">Reasons:</label>
                                                        <textarea name="reasons" class="form-control"><?php echo $self_employed_tbl_data['self_employed_reasons'] ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="btn-group">
                                                        <button type="reset" class="btn btn-danger">Reset</button>
                                                        <button type="submit" class="btn btn-primary" name="update_self_employed">Update</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                    <tr>
                                        <td><?php echo $self_employed_tbl_data['self_employed_date'] ?></td>
                                        <td><?php echo $self_employed_tbl_data['self_employed_business_type'] ?></td>
                                        <td><?php echo $self_employed_tbl_data['self_employed_reasons'] ?></td>
                                        <td>
                                            <a data-toggle="modal" data-target="#update_self_employed<?php echo $self_employed_tbl_data['self_employed'] ?>"><span class="glyphicon glyphicon-edit"></span></a>
                                        </td>
                                    </tr>
                                    <?php } } ?>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include '../footer.php'; ?>
        <script>
            $(function () {
            $('.js-loading-button').on('click', function () {
            var btn = $(this).button('loading')
            setTimeout(function (){
            btn.button('reset')
            }, 10000)
            })
            })
            
            $(function () {
            $('.js-popover').popover()
            })
            
            $(function () {
            $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
