<?php
    include '../config.php';
    if (isset($_GET['shout_id'])) {
        $shout_id = $_GET['shout_id'];
        $shout = $mysqli->query("SELECT * FROM shouts WHERE shout_id = $shout_id");
        $shout_data = $shout->fetch_assoc();
        
        $count_comments = $mysqli->query("SELECT * FROM shout_comments WHERE shout_id = ".$shout_data['shout_id']."");
        $countComments = $count_comments->num_rows;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Post of <?php show_fullname($shout_data['graduate_id']) ?></title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/msgs.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/fancybox/jquery.fancybox.js" type="text/javascript"></script>
        <link rel="icon" href="../images/favicon.png" />
        <style>
            .comments {
                background-color: #ffffff;
                padding: 10px;
                margin-bottom: 10px;
            }
            .comment {
                padding: 10px;
                border-bottom: 1px solid #ecebeb;
            }
        </style>
        <script type="text/javascript">
            $("[data-fancybox]").fancybox({ });
        </script>
    </head>
    <body>
        <?php include '../header.php'; ?>
        <div class="container" style="padding-top: 10px;">
            <div class="panel panel-default shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        <?php show_photo_profile($shout_data['graduate_id'], "40px") ?> <?php show_fullname($shout_data['graduate_id']) ?>
                    </div>
                </div>
                <div class="panel-body">                   
                    <?php echo $shout_data['shout_msg'] ?>
                     <?php
                    if ($shout_data['shout_photo'] != null) { ?>
                    <br>
                    <img src="../<?php echo $shout_data['shout_photo'] ?>" style="max-width: 99%;" />
                    <?php }
                    ?>
                </div>
                <div class="panel-footer">
                    <span class="glyphicon glyphicon-comment"></span> Comments (<b><?php echo $countComments ?></b>)<br><br>
                    <div class="comments">
                        <?php
                            if ($countComments == null) { ?>
                        <center>no comments yet</center>
                            <?php }
                        ?>
                        <?php
                            /* limits of comments to be displayed */
                            $comment_display_limit = 10;
                            
                            /* if the button page is triggered */
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'] + 1;
                                $offset = $comment_display_limit * $page;
                            } else {
                                $page = 0;
                                $offset = 0;
                            }    
                            
                            $left_comments = $countComments - ($page * $comment_display_limit); 
                            if ($left_comments < $comment_display_limit) {
                                $comment_display_limit = $left_comments;
                            }
                            ?>
                        <?php
                        if ($left_comments > $comment_display_limit) {
                            if ($page > 0) { 
                                $last = $page - 2?>
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>&page=<?php echo $last ?>">view previous</a>
                            </center>
                        </div> 
                            <?php } } else if ($left_comments < $comment_display_limit) { ?>
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>">view from the top</a>
                            </center>
                        </div>
                             <?php } else if ($left_comments == $comment_display_limit && $countComments != 0) { 
                                 $last = $page - 2?>
                        <div class="comment">
                            <center>
                                <a href="?shout_id=<?php echo $shout_data['shout_id'] ?>&page=<?php echo $last ?>">view previous</a>
                            </center>
                        </div>
                             <?php }
                        ?>
                            <?php 
                            $comments = $mysqli->query("SELECT * FROM shout_comments WHERE shout_id = ".$shout_data['shout_id']." ORDER BY comment_id ASC LIMIT $offset, $comment_display_limit") or die($mysqli->error);
                            
                            while ($comments_data = $comments->fetch_assoc()) {
                                /* get the user profile link */
                                $profile_link_comment = $mysqli->query("SELECT * FROM profile_links WHERE graduate_id = ".$comments_data['graduate_id']."");
                                $data_profile_link_comment = $profile_link_comment->fetch_assoc();
                                
                                $user_commented = $mysqli->query("SELECT * FROM graduates WHERE graduate_id = ".$comments_data['graduate_id']."");
                                $data_user_commented = $user_commented->fetch_assoc();
                                
                                $dp_commented_user = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = '".$comments_data['graduate_id']."' ORDER BY dp_id DESC");
                                $data_dp_commented_user = $dp_commented_user->fetch_assoc();
                                ?>
                        <div class="comment">
                            <img src="<?php echo $base_url ?>/profile/<?php echo $data_dp_commented_user['dp_link'] ?>" width="20px" class="shadow" style="margin: 0px !important; border: 2px solid #ffffff;" /> <b><a href="<?php echo $data_profile_link_comment['alumn_profile_hash_link'] ?>"><?php echo $data_user_commented['graduate_firstname'] . ' ' . $data_user_commented['graduate_middlename'] . ' ' . $data_user_commented['graduate_surname'] ?></a></b> <small><?php echo date("F j, Y h:i A", strtotime($comments_data['comment_date'])) ?></small><br>
                            <div style="padding-left: 27px; margin-top: 5px;">
                                <?php if ($comments_data['comment_photo_link'] != null) { ?>
                                <img src="<?php echo $base_url ?>/<?php echo $comments_data['comment_photo_link'] ?>" width="200px" style="margin-bottom: 5px;" /><br>
                                <?php } ?>
                                <?php echo $comments_data['comment_content'] ?>
                            </div>
                        </div>
                            <?php }
                        ?>
                        <?php
                        if ($left_comments > $comment_display_limit) {
                            if ($page == 0) {
                                ?>                        
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>&page=<?php echo $page ?>">see more...</a>
                            </center>
                        </div>
                            <?php } else if ($page > 0 ) { ?>
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>&page=<?php echo $page ?>">see more...</a>
                            </center>
                        </div>   
                        <?php } } else if ($left_comments < $comment_display_limit) {
                               $last = $page - 2;?>
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>&page=<?php echo $last ?>">view previews...</a>
                            </center>
                        </div>       
                           <?php }
                        ?>
                    </div>
                    <form method="post" action="comment.php?shout_id=<?php echo $shout_data['shout_id'] ?>" role="form" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Quick Comment:</label>
                            <textarea style="margin-bottom: 5px;" name="comment" class="form-control"></textarea>
                            <input type="file" name="comment_photo" />
                        </div>
                        <div class="btn-group">
                            <button type="reset" class="btn btn-danger">Reset</button>
                            <button type="submit" class="btn btn-primary">Comment</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include '../footer.php'; ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
