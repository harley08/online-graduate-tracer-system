<?php
    /* shouts display limit */
    $shout_display_limit = 10;
    
    /* get the total count of all shouts */
    $count_all_shouts = $mysqli->query("SELECT * FROM shout_comments");
    $count_AllShouts = $count_all_shouts->num_rows;
    
    /* if the see more button is triggered */
    if (isset($_GET['page'])) {
        $page = $_GET['page'] + 1;
        $offset = $shout_display_limit * $page;
    } else {
        $page = 0;
        $offset = 0;
    }
    
    $left_shouts = $count_AllShouts - ($page * $shout_display_limit);
    
    $query_shouts = $mysqli->query("SELECT * FROM shouts INNER JOIN alumni_accounts WHERE shouts.graduate_id = alumni_accounts.graduate_id ORDER BY shout_id DESC LIMIT $offset, $shout_display_limit");
    
    while ($shout_data = $query_shouts->fetch_assoc()) {
        
        $shout_graduate_id = $shout_data['graduate_id'];
        
        $query_dp_link = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = $shout_graduate_id ORDER BY dp_id DESC");
        $fetch_dp_link = $query_dp_link->fetch_assoc();
        $shout_dp_link = $fetch_dp_link['dp_link'];
        
        $query_shout_fullname = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = $shout_graduate_id");
        $data_shout_fullname = $query_shout_fullname->fetch_assoc();
        $shout_alum_surname = $data_shout_fullname['alum_surname'];
        $shout_alumn_firstname = $data_shout_fullname['alumn_firstname'];
        $shout_alum_middlename	 = $data_shout_fullname['alum_middlename'];
        $shout_user_fullname = $shout_alumn_firstname . ' ' . $shout_alum_middlename . ' ' . $shout_alum_surname;
        if ($shout_user_fullname == NULL) {
            $$shout_user_fullname = $shout_data['account_username'];
        }
        
        /* Count the shout comments */
        $count_comments = $mysqli->query("SELECT * FROM shout_comments WHERE shout_id = ".$shout_data['shout_id']."");
        $countComments = $count_comments->num_rows;
        ?>
<div class="panel panel-default shadow" style="border-radius: 0px; border: 1px solid #e6e6e6;">
    <div class="panel-heading" style="padding: 10px;">
        <a><img src="<?php echo $base_url . '/profile/' . $shout_dp_link ?>" class="shadow" style="margin: 0px !important; border: 2px solid #ffffff; margin-bottom: 5px !important;" width="50px" /></a>
        <span style="margin-left: 10px;"><a href="<?php get_profile_link($shout_data['graduate_id']) ?>"><?php echo $shout_user_fullname; ?></a></span>
        <?php 
            if ($logged_admin) {
                ?>
        <!--<font size="1px"><span class="glyphicon glyphicon-list pull-right"></span></font>-->
        <?php
            }
        ?>
    </div>
    <div class="panel-body">
        <?php convert_smiley($shout_data['shout_msg']) ?>
        <?php
            if ($shout_data['shout_photo'] != null) { ?>
        <br>
        <a href="<?php echo $shout_data['shout_photo'] ?>" data-fancybox="group" data-caption="<?php $shout_data['shout_msg'] ?>">
        <img src="<?php echo $shout_data['shout_photo'] ?>" width="50%" />
        </a>
            <?php }
        ?>
    </div>
    <div class="panel-footer" style="background-color: #f8f8f8">
        <small>
            <font style="color: #616161">
        <?php
            echo date("F j, Y", strtotime($shout_data['shout_date']));
        ?>
            </font>
        </small>
    </div>
    <div class="panel-footer">
        <?php
            $likes = $mysqli->query("SELECT * FROM shout_likes WHERE shout_id = ".$shout_data['shout_id']."");
            $like_data = $mysqli->query("SELECT * FROM shout_likes WHERE shout_id = ".$shout_data['shout_id']." AND graduate_id = $user_graduate_id");
            $likeData = $like_data->num_rows;
            $count_like = $likes->num_rows;
        ?>
        <a style="margin-right: 15px;" href="shouts/shout-comments.php?shout_id=<?php echo $shout_data['shout_id'] ?>"><small><span class="glyphicon glyphicon-comment"></span></small> Comment <b><?php echo $countComments ?></b></a> 
        <?php
            if ($count_like == null && $likeData == null) { ?>
                <a href="shouts/like.php?shout_id=<?php echo $shout_data['shout_id'] ?>"><span class="glyphicon glyphicon-thumbs-up"></span> Like <b><?php echo $count_like ?></b></a>
            <?php } else if ($likeData == null) { ?>
                <a href="shouts/like.php?shout_id=<?php echo $shout_data['shout_id'] ?>"><span class="glyphicon glyphicon-thumbs-up"></span> Like <b><?php echo $count_like ?></b></a>
            <?php } else if ($count_like != null) { ?>
                <a href="shouts/unlike.php?shout_id=<?php echo $shout_data['shout_id'] ?>"><span class="glyphicon glyphicon-thumbs-up"></span> Unlike <b><?php echo $count_like ?></b></a>
            <?php }
        ?>
        <?php
            if ($countComments >= 1) {
                $show_last_comment = $mysqli->query("SELECT * FROM shout_comments WHERE shout_id = ".$shout_data['shout_id']." ORDER BY comment_id DESC") or die($mysqli->error);
                $show_last_comment_data = $show_last_comment->fetch_assoc(); ?>
        <div class="shadow" style="background-color: #ffffff; border-radius: 3px; padding: 10px; margin: 3px;">
            <small>Last commented by:</small><br>
            <?php show_photo_profile($show_last_comment_data['graduate_id'], "30px") ?> <b><?php echo show_fullname($show_last_comment_data['graduate_id']) ?></b><br>
            <div style="margin-top: 5px; padding-left: 35px;">
                <?php echo $show_last_comment_data['comment_content'] ?>
            </div>
        </div>
            <?php }
        ?>
    </div>
</div>
        <?php
    }
?>