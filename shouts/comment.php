<?php
    include '../config.php';
    if (isset($_GET['shout_id'])) {
        $shout_id = $_GET['shout_id'];
        $comment = $_POST['comment'];
        $date = date("Y-m-d H:i:s");
        if (isset($_FILES['comment_photo'])) {
            move_uploaded_file($_FILES['comment_photo']['tmp_name'], "../images/shouts_photos/" . $_FILES['comment_photo']['name']);
            $photo_link = "";
            if ($_FILES['comment_photo']['name'] == null) {
                $photo_link = "";
            } else {
                $photo_link = "images/shouts_photos/" . $_FILES['comment_photo']['name'];
            }
            $comment = $mysqli->query("INSERT INTO shout_comments (shout_id, graduate_id, comment_content, comment_photo_link, comment_date) VALUES ($shout_id, $user_graduate_id, '$comment', '$photo_link', '$date')");
            if ($comment) {
                header("Location: shout-comments.php?shout_id=$shout_id");
            }
        }
    }
?>