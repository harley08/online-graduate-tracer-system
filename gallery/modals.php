<div id="createAlbum" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="glyphicon glyphicon-film"></span> Create Photo Album</h5>
            </div>
            <form method="post" action="actions/create-album.php"> 
                <div class="modal-body">
                    <div class="form-group">
                        <label>Title:</label>
                        <input type="text" name="album_title" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>Description:</label>
                        <textarea name="album_description" class="form-control" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="reset" class="btn btn-danger">Reset</button>
                        <button type="submit" class="btn btn-primary">Create Album</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="uploadPhoto" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
                <h5 class="modal-title"><span class="glyphicon glyphicon-film"></span> Upload Photos</h5>
            </div>
            <form method="post" action="actions/photo-upload.php" enctype="multipart/form-data"> 
                <div class="modal-body">
                    <div class="form-group">
                        <input type="file" name="photo1" />
                        <input type="file" name="photo2" />
                        <input type="file" name="photo3" />
                    </div>
                    <div class="form-group">
                        <label>Caption:</label>
                        <textarea name="caption" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Upload to:</label>
                        <select name="photo_album" class="form-control" required>
                            <option value="">-- Choose photo album --</option>
                            <?php
                                $photo_albums = $mysqli->query("SELECT * FROM gallery_albums WHERE graduate_id = $user_graduate_id");
                                $count_photo_albums = $photo_albums->num_rows;
                                if ($count_photo_albums == null) {
                                    echo '<option value="">PLEASE CREATE AN ALBUM!</option>';
                                }
                                while ($photo_album_data = $photo_albums->fetch_assoc()) { ?>
                            <option value="<?php echo $photo_album_data['album_title'] ?>"><?php echo $photo_album_data['album_title'] ?></option>
                                <?php }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="upload" class="btn btn-primary">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>