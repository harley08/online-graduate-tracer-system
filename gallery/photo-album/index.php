<?php include '../../config.php'; ?>
<?php
    if (isset($_GET['id'])) {
        $album_id = $_GET['id'];
        
        $query_album = $mysqli->query("SELECT * FROM gallery_albums WHERE album_id = $album_id");
        $album_data = $query_album->fetch_assoc();
    } else {
        header("Location: ../");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $album_data['album_title']; ?></title>
        <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/fancybox/jquery.fancybox.js" type="text/javascript"></script>
        <link rel="icon" href="../../images/favicon.png" />
        <script type="text/javascript">
            $("[data-fancybox]").fancybox({ });
        </script>
    </head>
    <body>
        <style>
            .gallery-contents {
                background-color: #f8f8f8;
                padding-top: 10px;
                padding-left: 0px !important;
            }
            .photo {
                background-color: #c0c0c0;
                width: 150px;
                height: 200px;
                display: inline-block;
                overflow: hidden;
                text-align: center;
                border: 2px solid #ffffff;
            }
            .photo:hover {
                border-color: #66afe9;
                outline: 0;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
            }
            .photo img {
                height: 100%;
            }
        </style>
        <?php include '../../header.php'; ?>
        <div class="container" style="padding-top: 10px !important;">
            <div class="panel panel-default shadow">
                <div class="panel-heading">
                    <h5 class="panel-title"><span class="glyphicon glyphicon-film"></span> <a href="<?php echo $base_url ?>/gallery/" style="color: #429b00;">Gallery</a> / <span class="glyphicon glyphicon-camera"></span> <?php echo $album_data['album_title'] ?></h5>                    
                </div>
                <div class="panel-body">
                    <div style="margin-bottom: 10px; background-color: #ebebeb; padding: 10px; border-radius: 3px;" class="shadow">
                    <?php
                        /* Get the name of the user who created the photo album */
                        $get_id = $mysqli->query("SELECT * FROM gallery_albums WHERE album_id = ".$_GET['id']."");
                        $getID = $get_id->fetch_assoc(); ?>
                        <a href="<?php get_profile_link($getID['graduate_id']) ?>">
                        <?php show_photo_profile($getID['graduate_id'], "50px;"); ?>
                        </a>
                        <?php echo ' ';
                        echo '<b>';
                        show_fullname($getID['graduate_id']);
                        echo '</b>';
                    ?>
                    </div>
                    <div class="gallery-contents shadow" align="center">
                        <?php
                            $photos = $mysqli->query("SELECT * FROM gallery_photos WHERE album_id = $album_id");
                            $count_photos = $photos->num_rows;
                            if ($count_photos == null) {
                                echo '<center>no photos uploaded yet</center>';
                            }
                            while ($photo_data = $photos->fetch_assoc()) { 
                                $query_uploader = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = ".$photo_data['graduate_id']."");
                                $uploader_data = $query_uploader->fetch_assoc();
                                $uploader_surname = $uploader_data['alum_surname'];
                                $uploader_firstname = $uploader_data['alumn_firstname'];
                                $uploader_middlename = $uploader_data['alum_middlename'];
                                $uploader_fullname = $uploader_firstname . ' ' . $uploader_middlename . ' ' . $uploader_surname;
                                ?>
                        <div class="photo shadow">
                            <a href="../<?php echo $photo_data['photo_link'] ?>" data-fancybox="group" data-caption="<?php echo $photo_data['photo_description'] ?>">
                                <img src="../<?php echo $photo_data['photo_link'] ?>" />
                            </a>
                            <div id="photo<?php echo $photo_data['photo_id'] ?>" tabindex="-1" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">x</button>
                                            <h5 class="modal-title"><?php echo $photo_data['photo_description']; ?></h5>
                                        </div>
                                        <div class="modal-body">
                                            <img style="background-color: #c0c0c0;" src="../<?php echo $photo_data['photo_link'] ?>" width="100%" />
                                            <br><br>                                            
                                            <div align="left">
                                                Uploaded on <b><?php echo $photo_data['photo_date_uploaded']; ?></b> by <b><?php echo $uploader_fullname ?></b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <?php }
                        ?>
                    </div>
                    <div class="panel-body">
                        <?php
                            $comments = $mysqli->query("SELECT * FROM gallery_album_comments WHERE album_id = $album_id ORDER BY comment_id ASC");
                            $count_comments = $comments->num_rows;
                        ?>
                        <h5><span class="glyphicon glyphicon-comment"></span> Comments (<?php echo $count_comments ?>)</h5>
                        <style>
                            .comments {
                                background-color: #f2f2f2;
                                padding: 2px;
                            }
                            .comment {
                                background-color: #ffffff;
                                padding: 10px;
                                margin-bottom: 2px;
                            }
                        </style>
                        <div class="comments shadow">
                            <?php
                                if ($count_comments == null) { ?>
                            <div class="comment">
                                <center>no comments yet</center>
                            </div>
                                <?php }
                            ?>
                        <?php
                            while ($comment_data = $comments->fetch_assoc()) { ?>
                        <div class="comment">
                            <img src="../../images/user.png" width="40px" /> 
                            <span style="margin-left: 5px;"><?php echo $comment_data['comment_msg'] ?></span>
                        </div>
                            <?php }
                        ?>
                        </div><br>
                        <form method="post" action="actions/comment.php?id=<?php echo $album_id ?>">
                            <div class="form-group">
                                <label>Quick Comment:</label>
                                <textarea name="comment" class="form-control" required></textarea>
                            </div>
                            <div class="btn-group">
                                <button type="reset" class="btn btn-danger">Reset</button>
                                <button type="submit" class="btn btn-primary">Comment</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include '../../footer.php'; ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
