<?php
    include '../../config.php';
    if (isset($_POST['upload'])) {
        $album = $_POST['photo_album'];
        $query_album_id = $mysqli->query("SELECT * FROM gallery_albums WHERE album_title = '$album'");
        $fetch_album_id = $query_album_id->fetch_assoc();
        $album_id = $fetch_album_id['album_id'];
        
        $photo_description = $_POST['caption'];
        
        $date_uploaded  = date("Y-m-d H:i:s");
        
        if (isset($_FILES['photo1']) && $_FILES['photo1']['name'] != null) {
            $photo1 = addslashes(file_get_contents($_FILES['photo1']['tmp_name']));
            $photo1_name = addslashes($_FILES['photo1']['name']);
            
            move_uploaded_file($_FILES['photo1']['tmp_name'], "../images/" . $_FILES['photo1']['name']);
            $location = "images/" . $_FILES['photo1']['name'];
            
            $upload_photo1 = $mysqli->query("INSERT INTO gallery_photos (album_id, graduate_id, photo_link, photo_description, photo_date_uploaded) VALUES ($album_id, $user_graduate_id, '$location', '$photo_description', '$date_uploaded')");
            if ($upload_photo1) {
                header("Location: ../");
            }
            if (!$upload_photo1) {
                echo 'Error: Photo 1. ' . $mysqli->error;
            }
        }
        if (isset($_FILES['photo2']) && $_FILES['photo2']['name'] != null) {
            $photo2 = addslashes(file_get_contents($_FILES['photo2']['tmp_name']));
            $photo2_name = addslashes($_FILES['photo2']['name']);
            
            move_uploaded_file($_FILES['photo2']['tmp_name'], "../images/" . $_FILES['photo2']['name']);
            $location2 = "images/" . $_FILES['photo2']['name'];
            
            $upload_photo2 = $mysqli->query("INSERT INTO gallery_photos (album_id, graduate_id, photo_link, photo_description, photo_date_uploaded) VALUES ($album_id, $user_graduate_id, '$location2', '$photo_description', '$date_uploaded')");
            if ($upload_photo2) {
                header("Location: ../");
            }
            if (!$upload_photo2) {
                echo 'Error: Photo 2. ' . $mysqli->error;
            }
        }
        if (isset($_FILES['photo3']) && $_FILES['photo3']['name'] != null) {
            $photo3 = addslashes(file_get_contents($_FILES['photo3']['tmp_name']));
            $photo3_name = addslashes($_FILES['photo3']['name']);
            
            move_uploaded_file($_FILES['photo3']['tmp_name'], "../images/" . $_FILES['photo3']['name']);
            $location3 = "images/" . $_FILES['photo3']['name'];
            
            $upload_photo3 = $mysqli->query("INSERT INTO gallery_photos (album_id, graduate_id, photo_link, photo_description, photo_date_uploaded) VALUES ($album_id, $user_graduate_id, '$location3', '$photo_description', '$date_uploaded')");
            if ($upload_photo3) {
                header("Location: ../");
            }
            if (!$upload_photo3) {
                echo 'Error: Photo 3. ' . $mysqli->error;
            }
        }
        if (isset($_FILES['photo1']) || isset($_FILES['photo2']) || isset($_FILES['photo3'])) {
            /* Post a shout to the newsfeed that the user uploaded a photo to the album */
            $date_posted = date("F j, Y", strtotime($date_uploaded));
            $post = '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>'.$logged_fullname.'</b> uploaded a photo to the album <a href="'.$base_url.'/gallery/photo-album/?id='.$album_id.'&album='.$album.'">'.$album.'</a> on '.$date_posted.'.</div></div>';
            $post_shout = $mysqli->query("INSERT INTO shouts (graduate_id, shout_msg, shout_date) VALUES ($user_graduate_id, '$post', '$date_uploaded')");
        }
    }
?>