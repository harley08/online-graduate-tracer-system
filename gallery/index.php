<?php
    include '../config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Gallery</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />
    </head>
    <body>
        <style>
            .gallery-contents {
                background-color: #f8f8f8;
                margin-top: 20px;
                padding-left: 0px !important;
            }
        </style>
        <?php include '../header.php'; ?>        
        <?php
            include 'modals.php';
        ?>
        <div class="container" style="padding-top: 10px !important;">
            <a data-toggle="modal" data-target="#uploadPhoto" style="margin-bottom: 10px; border-radius: 0px !important;" class="btn btn-primary"><span class="glyphicon glyphicon-upload"></span> Upload Photos</a>
            <a data-toggle="modal" data-target="#createAlbum" style="margin-bottom: 10px; border-radius: 0px !important;" class="btn btn-success"><span class="glyphicon glyphicon-film"></span> Create Photo Album</a>
            <div class="panel panel-default shadow">
                <div class="panel-body">
                    <center>
                        <h4><span class="glyphicon glyphicon-camera"></span> Gallery</h4>
                    </center>
                    <div class="gallery-contents shadow" align="center">
                        <style>
                            .album-content {
                                width: 200px;
                                border: 2px solid #f5f5f5;
                                margin: 10px;
                                margin-right: 0px !important;
                                display: inline-table;
                            }
                            .album-content:hover {
                                border-color: #66afe9;
                                outline: 0;
                                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
                                box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
                            }
                            .album {
                                background-image: url("../images/photo-album-default-background.png");
                                background-color: #c0c0c0;
                                width: 198px;
                                height: 200px;
                                overflow: hidden;
                            }
                            .album-title {
                                background-color: #e8e9e8;
                                padding: 10px;
                                width: 198px;
                                border-top: 1px solid #ffffff;
                            }
                            .photo {
                                width: 200px;
                                height: 200px; 
                            }
                            .photo:hover {
                                width: 200px;
                                height: 200px;
                            }
                            .photo img {
                                width: 100%;
                            }
                        </style>
                        <?php
                            $albums = $mysqli->query("SELECT * FROM gallery_albums");
                            $count_albums = $albums->num_rows;
                            while ($album_data = $albums->fetch_assoc()) { ?>
                        <a href="photo-album/?id=<?php echo $album_data['album_id'] ?>&album=<?php echo $album_data['album_title'] ?>" data-toggle="tooltip" data-placement="top" title="Click to view photos">
                        <div class="album-content">
                            <div class="album shadow">
                                <div class="photo">
                                    <?php
                                        $galley_photos = $mysqli->query("SELECT * FROM gallery_photos WHERE album_id = ".$album_data['album_id']." ORDER BY photo_id DESC");
                                        $count_gallery_photos = $galley_photos->num_rows;
                                        for ($i = 1; $i <= 3; $i++) { 
                                        $gallery_photo_data = $galley_photos->fetch_assoc(); ?>
                                        <img src="<?php echo $gallery_photo_data['photo_link']; ?>" class="img img-responsive" />
                                        <?php }
                                    ?>
                                </div>
                            </div>
                            <div class="album-title">
                                <small>
                                    <?php echo $album_data['album_title'] ?>
                                </small>
                            </div>
                        </div>
                        </a>
                            <?php }
                            if ($count_albums == null) {
                                echo '<center style="padding: 50px;">no albums created yet</center>';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include '../footer.php'; ?>
        <script>
            $(function () {
            $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
