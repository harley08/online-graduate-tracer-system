<div id="folder-modal<?php echo $i ?>" class="mymodal">
    <div class="panel panel-default shadow" style="border-radius: 5px">
        <div class="panel-heading">
            <h5 class="panel-title"><span class="glyphicon glyphicon-education"></span> Batch <?php echo $folder_data['batch_year'] ?></h5>
        </div>
        <div class="panel-body" style="overflow: scroll; height: 450px;">
            <table id="folder-table<?php echo $i ?>" class="table table-stripe table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Course</th>
                        <th>Major</th>
                        <th>Email / Contact #</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $batch_graduates = $mysqli->query("SELECT * FROM graduates WHERE graduate_batch = '$folder_name'");
                        while ($batch_graduates_data = $batch_graduates->fetch_assoc()) { ?>
                    <tr>
                        <td><?php echo $batch_graduates_data['graduate_firstname'] ?> <?php echo $batch_graduates_data['graduate_middlename'] ?> <?php echo $batch_graduates_data['graduate_surname'] ?></td>
                        <td><?php echo $batch_graduates_data['graduate_course'] ?></td>
                        <td><?php echo $batch_graduates_data['graduate_major'] ?></td>
                        <td><?php echo $batch_graduates_data['graduate_email'] ?> / <?php echo $batch_graduates_data['graduate_contact_number'] ?></td>
                        <td><?php echo $batch_graduates_data['graduate_address'] ?></td>
                        <td>
                            <span class="glyphicon glyphicon-edit"></span> <span class="glyphicon glyphicon-trash"></span>
                        </td>
                    </tr>
                        <?php }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="panel-footer">
            <a href="#close" class="btn btn-danger">Close</a>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
    $('#folder-table<?php echo $i ?>').dataTable();
    });
</script>