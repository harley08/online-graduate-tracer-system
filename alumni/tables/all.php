<table id="example" class="table table-inbox table-responsive table-hover" width="100%">
<thead>
<tr>
<th>Full Name</th>
<th>Course</th>
<th>Major</th>
<th>Batch</th>
<th>Email / Contact</th>
<th>Address</th>
<?php
    if ($logged_admin) {
        echo '<th>Action</th>';
    }
?>
</tr>
</thead>
<tbody>
    <?php
        $mysqli = new mysqli('localhost', 'root', '', 'congrad');
        $graduates = $mysqli->query("SELECT * FROM graduates WHERE graduate_id != 1");
        $count = $graduates->num_rows;
        for ($i = 1; $i <= $count; $i++) {
            $data = $graduates->fetch_assoc();
            
            $id = $data['graduate_id'];
            $firstname = $data['graduate_firstname'];
            $middlename = $data['graduate_middlename'];
            $lastname = $data['graduate_surname'];
            $course = $data['graduate_course'];
            $major = $data['graduate_major'];
            $batch = $data['graduate_batch'];
            $contact_number = $data['graduate_contact_number'];
            $email = $data['graduate_email'];
            $address = $data['graduate_address'];
            
            $get_profile_link = $mysqli->query("SELECT * FROM profile_links WHERE graduate_id = $id");
            $fetch_record = $get_profile_link->fetch_assoc();
            
            $alumni_profile_link = $fetch_record['alumn_profile_hash_link'];
                       
            echo '<tr>'; ?>
            <?php include 'update-alumni-modal.php'; ?>
            <?php echo '<td><a title="'.$firstname.' profile" href="'.$alumni_profile_link.'"><b><small>'.$firstname.' '.$middlename.' '.$lastname.'</small></b></a></td>'
            . '<td><small>'.$course.'</small></td>'
            . '<td><small>'.$major.'</small></td>'
            . '<td><small>'.$batch.'</small></td>'
            . '<td><small>'.$email.' / '.$contact_number.'</small></td>'                    
            . '<td><small>'.$address.'</small></td>';
            if ($logged_admin) {
            echo '<td><small><a href="#updateGrad'.$id.'"><span class="glyphicon glyphicon-edit"></span></a> <a data-toggle="tooltip" data-placement="top" title="Delete" href="delete-alumni.php?id='.$id.'"><span class="glyphicon glyphicon-trash"></span></a></small></td>';
            }
            echo '</tr>';
        }
    ?>
</tbody>
</table>
<script>
        $(document).ready(function(){
            $('#example').dataTable();
        });
</script>