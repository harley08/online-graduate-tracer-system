<table id="example" class="table table-striped table-responsive table-hover" width="100%">
<thead>
<tr>
<th>Full Name</th>
<th>Gender</th>
<th>Address</th>
<th>Education</th>
<th>Batch</th>
<?php
    if ($logged_admin) {
        echo '<th>Action</th>';
    }
?>
</tr>
<tbody>
<tr>
    <?php
        $mysqli = new mysqli('localhost', 'root', '', 'congrad');
        $alumni = $mysqli->query("SELECT * FROM alumni WHERE alumn_valid = 0");
        $count = $alumni->num_rows;
        for ($i = 1; $i <= $count; $i++) {
            $data = $alumni->fetch_assoc();
            
            $fullname = $data['alumn_fullname'];
            $username = $data['alumn_username'];
            $email = $data['alumn_email'];
            $sex = $data['alumn_sex'];
            $contact = $data['alumn_contact'];
            $address = $data['alumn_address'];
            $education = $data['alumn_education'];
            $batch = $data['alumn_batch'];
            $id = $data['alumn_id'];
            
            $get_profile_link = $mysqli->query("SELECT * FROM profile_links WHERE alumn_id = $id");
            $fetch_record = $get_profile_link->fetch_assoc();
            
            $alumni_profile_link = $fetch_record['alumn_profile_hash_link'];
            
            echo '<tr>'
            . '<td><a data-toggle="tooltip" data-placement="top" title="'.$fullname.' Profile" profile" href="'.$alumni_profile_link.'"><b><small>'.$fullname.'</small></b></a></td>'
            . '<td><small>'.$sex.'</small></td>'
            . '<td><small>'.$address.'</small></td>'
            . '<td><small>'.$education.'</small></td>'                    
            . '<td><small>'.$batch.'</small></td>';
            if ($logged_admin) {
            echo '<td><small><a data-toggle="modal" data-target="#approveModal'.$i.'"><span data-toggle="tooltip" data-placement="top" title="Approve" class="glyphicon glyphicon-thumbs-up"></span></a> <a data-toggle="tooltip" data-placement="top" title="Delete" href="../actions/delete.php?id='.$id.'"><span class="glyphicon glyphicon-trash"></span></a></small></td>';
            }
            echo '</tr>';
            include 'modals.php';
        }
    ?>
</tr>
</tbody>
</thead>
</table>
<script>
        $(document).ready(function(){
            $('#example').dataTable();
        });
        
        $(function () {
        $('[data-toggle="tooltip"]').tooltip()
        })
</script>