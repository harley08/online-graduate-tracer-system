<?php include '../config.php'; ?>
<?php
    $search = $_GET['search_alumni'];
    
    $searched_word_count = str_word_count($search);
    
    $count_result = $mysqli->query("SELECT * FROM alumni WHERE alumn_fullname LIKE '%$search%'");
    $count_searched = $count_result->num_rows;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Search Result <?php echo $count_searched ?></title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.dataTables.js" type="text/javascript"></script>
        <link rel="icon" href="../images/favicon.png" />
    </head>
    <body>
        <?php include '../header.php' ?>
        <div class="container container-padding-10">
            <div class="panel panel-default shadow">
                <div class="panel-heading" align="center">
                    <?php
                        $search = $_GET['search_alumni'];
                        $count_result = $mysqli->query("SELECT * FROM graduates WHERE alumn_fullname LIKE '%$search%'");
                        $count_searched = $count_result->num_rows;
                    ?>
                    <h5 class="panel-title"><span class="glyphicon glyphicon-search"></span> Search Result <?php echo $count_searched ?></h5>
                </div>
                <div class="panel-body" style="padding-bottom: 0px;">
                    <table class="table table-striped table-responsive table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Full Name</th>
                                <th>Gender</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Education</th>
                            </tr>
                        <tbody>
                            <?php
                                if ($_GET['search_alumni']) {
                                    $search_fullname = $_GET['search_alumni'];
                                    $search = $mysqli->query("SELECT * FROM alumni WHERE alumn_fullname LIKE '%$search_fullname%'");
                                    $count_search = $search->num_rows;
                                    for ($i = 1; $i <= $count_search; $i++) {
                                        $searched_data = $search->fetch_assoc();
                                        $searched_fullname = $searched_data['alumn_fullname'];
                                        $searched_gender = $searched_data['alumn_sex'];
                                        $searched_email = $searched_data['alumn_email'];
                                        $searched_address = $searched_data['alumn_address'];
                                        $searched_education = $searched_data['alumn_education'];
                                        $username = $searched_data['alumn_username'];
                                        $dp = $mysqli->query("SELECT * FROM profile_pictures WHERE dp_user = '$username' ORDER BY dp_id DESC");
                                        $dp_data = $dp->fetch_assoc();
                                        $dp_link = $dp_data['dp_link'];
                                        $id = $searched_data['alumn_id'];
                                        $link = $mysqli->query("SELECT * FROM profile_links WHERE alumn_id = $id ORDER BY profile_id DESC");
                                        $link_data = $link->fetch_assoc();
                                        $prof_link = $link_data['alumn_profile_hash_link'];
                                        ?>
                            <tr>
                                <td><img src="<?php echo $base_url ?>/profile/<?php echo $dp_link ?>" width="50px" class="shadow" style="border: 2px solid #ffffff;" /></td>
                                <td><a href="<?php echo $prof_link ?>"><?php echo $searched_fullname ?></a></td>
                                <td><?php echo $searched_gender ?></td>
                                <td><?php echo $searched_email ?></td>
                                <td><?php echo $searched_address ?></td>
                                <td><?php echo $searched_education ?></td>
                            </tr>
                            <?php
                                    }
                                }
                            ?>
                        </tbody>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <?php include '../footer.php' ?>
        <script>window.jQuery || document.write('<script src="../assets/js/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/js/popover.js" type="text/javascript"></script>
    </body>
</html>
