<div class="shadow" style="background-color: #f9f8f8; padding: 20px;">
<table id="example" class="table table-striped table-responsive table-hover shadow" width="100%">
<thead>
<tr>
<th>Full Name</th>
<th>From - To</th>
<th>Company Name</th>
<th>Company Address</th>
<th>Company Position</th>
<th>Employment Status</th>
<?php
    if ($logged_admin) {
        echo '<th>Action</th>';
    }
?>
</tr>
<tbody>
    <?php
        $mysqli = new mysqli('localhost', 'root', '', 'congrad');
        $graduates = $mysqli->query("SELECT * FROM work_experiences_employed");
        $count = $graduates->num_rows;
        for ($i = 1; $i <= $count; $i++) {
            $data = $graduates->fetch_assoc();
            
            $id = $data['graduate_id'];
            $get_name = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = $id");
            $name_data = $get_name->fetch_assoc();
            $surname = $name_data['alum_surname'];
            $firstname = $name_data['alumn_firstname'];
            $middlename = $name_data['alum_middlename'];
            
            $date_from = $data['date_to'];
            $date_to = $data['date_to'];
            $company_name = $data['company_name'];
            $company_address = $data['company_address'];
            $company_position = $data['company_position'];
            $employment_status = $data['employment_status'];
            
            $get_profile_link = $mysqli->query("SELECT * FROM profile_links WHERE graduate_id = $id");
            $fetch_record = $get_profile_link->fetch_assoc();
            
            $alumni_profile_link = $fetch_record['alumn_profile_hash_link'];
            
            echo '<tr>'
            . '<td><a title="'.$firstname.' profile" href="'.$alumni_profile_link.'"><b><small>'.$firstname.' '.$middlename.' '.$surname.'</small></b></a></td>'
            . '<td>'
                    . '<small>'.$date_from.' - '.$date_to.'</small>';
                    echo '</td>';
            echo '<td><small>'.$company_name.'</small>';
            echo '</td>'
            . '<td><small>'.$company_address.'</small>';
            echo '</td>'
            . '<td><small>'.$company_position.'</small>';
            echo '</td>'                    
            . '<td><small>'.$employment_status.'</small>';
            echo '</td>';
            if ($logged_admin) {
            echo '<td><small><a href="actions/update.php?id='.$id.'#update" title="Update Information"><span class="glyphicon glyphicon-edit"></span></a> <a data-toggle="tooltip" data-placement="top" title="Delete" href="actions/delete.php?id='.$id.'"><span class="glyphicon glyphicon-trash"></span></a></small></td>';
            }
            echo '</tr>';
        }
    ?>
</tbody>
</thead>
</table>
</div>
<script>
        $(document).ready(function(){
            $('#example').dataTable();
        });
</script>