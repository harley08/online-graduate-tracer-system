<div class="shadow" style="background-color: #f9f8f8; padding: 20px;">
<table id="example2" class="table table-striped table-responsive table-hover shadow" width="100%">
<thead>
<tr>
<th>Full Name</th>
<th>Unemployed Date</th>
<th>Reasons</th>
<?php
    if ($logged_admin) {
        echo '<th>Action</th>';
    }
?>
</tr>
<tbody>
    <?php
        $mysqli = new mysqli('localhost', 'root', '', 'congrad');
        $graduates = $mysqli->query("SELECT * FROM work_experiences_unemployed ORDER BY unemployed_id ASC");
        $count = $graduates->num_rows;
        for ($i = 1; $i <= $count; $i++) {
            $data = $graduates->fetch_assoc();
            
            $id = $data['graduate_id'];
            $get_name = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = $id");
            $name_data = $get_name->fetch_assoc();
            $surname = $name_data['alum_surname'];
            $firstname = $name_data['alumn_firstname'];
            $middlename = $name_data['alum_middlename'];
            
            $unemployed_date = $data['unemployed_date'];
            $unemployed_reasons = $data['unemployed_reasons'];
            
            echo '<tr>'
            . '<td><a><b><small>'.$firstname.' '.$middlename.' '.$surname.'</small></b></a></td>'

            . '<td><small>'.$unemployed_date.'</small></td>'
            . '<td><small>'.$unemployed_reasons.'</small></td>';
            if ($logged_admin) {
            echo '<td><small><a href="actions/update.php?id='.$id.'#update" title="Update Information"><span class="glyphicon glyphicon-edit"></span></a> <a data-toggle="tooltip" data-placement="top" title="Delete" href="actions/delete.php?id='.$id.'"><span class="glyphicon glyphicon-trash"></span></a></small></td>';
            }
            echo '</tr>';
        }
    ?>
</tbody>
</thead>
</table>
</div>
<script>
        $(document).ready(function(){
            $('#example2').dataTable();
        });
</script>