<?php include '../../config.php' ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Employment Status</title>
        <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/js/jquery.dataTables.js" type="text/javascript"></script>
        <link rel="icon" href="../../images/favicon.png" />
        <style>
            input[type='search'],select[name='example_length'] {
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            color: #555555;
            vertical-align: middle;
            background-color: #ffffff;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
             -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            }
            input[type='search']:focus,select[name='example_length']:focus, input[type='search']:hover,select[name='example_length']:hover {
            border-color: #66afe9;
            outline: 0;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
            }
            .panel {
                -webkit-box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
                -moz-box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
                box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
            }
            tbody tr {
                padding: 20px !important;
            }
            td {
                padding: 10px !important;
            }
            label {
                padding: 10px;
            }
            .label-none {
                padding: 0px;
            }
            .dataTables_paginate {
                padding: 10px;
            }
            .dataTables_info {
                padding: 10px;
            }
            tbody, table {
                border-bottom: 1px solid #c0c0c0 !important;
            }
            thead {
                background-color: #ffffff;
                padding: 10px !important;
                color: #2c5600;
            }
        </style>
    </head>
    <body>
        <?php 
            include '../../header.php';
        ?>
        <style>
            .active:focus {
                color: #ffffff !important;
            }
            .active a:focus {
                color: #ffffff !important;
            }
            ul li a {
                color: #ffffff;
            }
        </style>
        <div class="container" style="padding-top: 10px; padding-bottom: 10px; margin-bottom: 0px;">
            <ul style="background-color: #407e00; padding-top: 10px; padding-left: 10px; border-bottom: 0px solid #ccc; border-top-left-radius: 3px; border-top-right-radius: 3px;" id="myTab" class="nav nav-tabs shadow">
                <li class="active">
                    <a href="#employed" data-toggle="tab">Employed</a>
                </li>
                <li>
                    <a href="#self_employed" data-toggle="tab">Self Employed</a>
                </li>
                <li>
                    <a href="#unemployed" data-toggle="tab">Unemployed</a>
                </li>
                <li class="pull-right">
                    <button type="button" onclick="printPreview()" class="btn btn-default" style="margin-right: 20px;"><small><span class="glyphicon glyphicon-print"></span></small> Print Preview</button>
                </li>
            </ul>
            <script>
                function printPreview() {
                    print();
                }
            </script>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane active" id="employed">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?php include 'tables/employed-table.php'; ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="self_employed">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?php include 'tables/self-employed-table.php'; ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="unemployed">
                    <div class="panel panel-default">
                        <div class="panel-body">
                           <?php include 'tables/unemployed-table.php'; ?>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <?php
            include '../../footer.php';
        ?>        
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
