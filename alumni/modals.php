<!-- Add Graduate Modal -->
<div class="modal fade" id="addAlumni" tabindex="-1" role="dialog" arialabelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Add Graduate</h5>
            </div>
            <form role="form" action="actions/add-graduate.php" method="post">
            <div class="modal-body">
                    <div class="form-group">
                        <label class="label-none" for="surname">Surname:</label>
                        <input type="text" style="width: 200px;" placeholder="type here..."  name="surname" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label class="label-none" for="firstname">First name:</label>
                        <input type="text" style="width: 200px;" placeholder="type here..." name="firstname" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label class="label-none" for="middlename">Middle Name:</label>
                        <input type="text" style="width: 200px;" placeholder="type here..." name="middlename" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label class="label-none" for="course">Course:</label>
                        <select name="course" class="form-control" required="" style="width: 200px;">
                            <option value="">--- Select Course --</option>
                            <option value="Bachelor of Science in Information Technology">BSIT</option>
                            <option value="Bachelor of Science in Hotel & Tourism Management">BSHTM</option>
                            <option value="Bachelor of Science in Hotel & Restaurant Management">BSHRM</option>
                            <option value="Bachelor of Science in Computer Engineering">BSCPE</option>
                            <option value="Bachelor of Science in Entrepreneurship">BSENTREP</option>
                            <option value="Bachelor of Science in Fisheries">BSFI</option>
                            <option value="Bachelor of Science in Criminology">BSCRIM</option>
                            <option value="Bachelor of Secondary Education">BSED</option>
                            <option value="Bachelor of Elementary Education">BEED</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="label-none" for="major">Major:</label>
                        <input type="text" style="width: 200px;" placeholder="type here..." name="major" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="label-none" for="batch">Batch:</label>
                        <select style="width: 200px;" name="batch" class="form-control">
                        <option value="">-- Select Batch --</option>
                        <?php
                            $folders = $mysqli->query("SELECT * FROM batch_folders");
                            $folderCount = $folders->num_rows;
                            for ($i = 1; $i <= $folderCount; $i++) {
                                $folder_data = $folders->fetch_assoc();
                                $folder_name = $folder_data['batch_year']; ?>
                        <option value="<?php echo $folder_name ?>"><?php echo $folder_name ?></option>
                            <?php }
                        ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="label-none" for="contact_number">Contact Number:</label>
                        <input type="text" name="contact_number" class="form-control" placeholder="type here..." style="width: 200px;" />
                    </div>
                    <div class="form-group">
                        <label class="label-none" for="email">Email:</label>
                        <input type="text" name="email" style="width: 200px;" class="form-control" placeholder="type here..." />
                    </div>
                    <div class="form-group">
                        <label class="label-none" for="birthday">Birthday:</label>
                        <input type="date" class="form-control" style="width: 200px;" name="birthday" required />
                    </div>
                    <div class="form-group">
                        <label class="label-none" for="address">Address:</label>
                        <input type="text" name="address" placeholder="type here..." class="form-control" required />
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" class="btn btn-primary" value="Add" />
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Create Batch Folder Modal -->
<div id="createBatchFolder" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title"><span style="margin-right: 5px;" class="glyphicon glyphicon-folder-open"></span> Create Batch Folder</h5>
            </div>
            <form role="form" action="actions/create-batch-folder.php" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Enter Batch Folder:</label>
                        <input type="text" name="batch" class="form-control" required placeholder="e.g. 2009-2010" />
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="reset" name="reset" class="btn btn-danger">Reset Field</button>
                        <button type="submit" name="create" class="btn btn-primary">Create Folder</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>