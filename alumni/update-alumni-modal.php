<div id="updateGrad<?php echo $id ?>" class="mymodal">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h5 class="panel-title">
                <b><?php echo $firstname . ' ' . $middlename . ' ' . $lastname ?></b>
            </h5>
        </div>
        <form method="post" role="form" action="update-alumni.php?id=<?php echo $id ?>">
        <div class="panel-body" style="height: 550px; overflow: scroll;">
            <div class="row">
            <div class="form-group">                
                <div class="col-sm-3">
                    <label for="lastname">Last Name:</label>
                    <input type="text" name="lastname" class="form-control" value="<?php echo $lastname ?>" />
                </div>
            </div>
            </div>
            <div class="row">
            <div class="form-group">
                <div class="col-sm-3">
                    <label for="firstname">First Name:</label>
                    <input type="text" class="form-control" name="firstname" value="<?php echo $firstname ?>" />
                </div>
            </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="middlename">Middle Name:</label>
                        <input type="text" name="middlename" class="form-control" value="<?php echo $middlename ?>" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="couse">Couse:</label>
                        <input type="text" name="couse" class="form-control" value="<?php echo $course ?>" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="major">Major:</label>
                        <input type="text" name="major" class="form-control" value="<?php echo $major ?>" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="batch">Batch:</label>
                        <input type="text" name="batch" class="form-control" value="<?php echo $batch ?>" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="contact">Contact #:</label>
                        <input type="text" name="contact" class="form-control" value="<?php echo $contact_number ?>" />
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="form-group">
                    <div class="col-sm-5">
                        <label for="email">Email:</label>
                        <input type="text" name="email" class="form-control" value="<?php echo $email ?>" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="address">Address:</label>
                        <input type="text" name="address" class="form-control" value="<?php echo $address ?>" />
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="btn-group">
                <a href="#close" class="btn btn-danger">Cancel</a>
                <button type="submit" class="btn btn-primary" name="update_graduate">Update</button>
            </div>
        </div>
        </form>
    </div>
</div>