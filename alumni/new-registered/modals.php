<!-- Modal approve user -->
<div class="modal fade" id="approveModal<?php echo $i ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 style="margin: 0px;" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Confirm Approve</h5>
            </div>
            <div class="modal-body">
                - <b><?php echo $fullname ?></b><br> 
               - <b><?php echo $sex ?></b><br>
               - <b><?php echo $address ?></b><br>
               - <b><?php echo $education ?></b><br>
               - <b><?php echo $batch ?></b>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="../actions/approve.php?id=<?php echo $id ?>" class="btn btn-primary">Approve</a>
                </div>
            </div>
        </div>
    </div>
</div>