<?php include '../config.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Alumni Registered</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.dataTables.js" type="text/javascript"></script>
        <link rel="icon" href="../images/favicon.png" />
        <link href="../assets/css/mymodal.css" rel="stylesheet" type="text/css"/>
        <style>
            input[type='search'],select[name='example_length'] {
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            color: #555555;
            vertical-align: middle;
            background-color: #ffffff;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
             -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            }
            input[type='search']:focus,select[name='example_length']:focus, input[type='search']:hover,select[name='example_length']:hover {
            border-color: #66afe9;
            outline: 0;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
            }
            .panel {
                -webkit-box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
                -moz-box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
                box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
            }
            tbody tr {
                padding: 20px !important;
            }
            td {
                padding: 10px !important;
            }
            label {
                padding: 10px;
            }
            .label-none {
                padding: 0px;
            }
            .dataTables_paginate {
                padding: 10px;
            }
            .dataTables_info {
                padding: 10px;
            }
            tbody, table {
                border-bottom: 1px solid #c0c0c0 !important;
            }
            thead {
                padding: 10px !important;
            }
        </style>
    </head>
    <body>
        <?php include '../header.php'; ?>
        <div class="container" style="padding-top: 10px;">
            <?php
                $alumni_registered = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id != 1");
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <span class="glyphicon glyphicon-education"></span> Alumni Registered <b><?php echo $alumni_registered->num_rows; ?></b>
                    </h5>
                </div>
                <div class="panel-body">
                    <table id="registered" class="table table-striped tab-active">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Course Graduated</th>
                                <th>Batch</th>
                                <th>Present Address</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php                                
                                while ($alumni_data = $alumni_registered->fetch_assoc()) { 
                                    $get_educ_back = $mysqli->query("SELECT * FROM alumni_educational_background WHERE graduate_id = ".$alumni_data['graduate_id']."");
                                    $get_educ = $get_educ_back->fetch_assoc();
                                    ?>
                                    <tr>
                                        <td>
                                            <a href="<?php get_profile_link($alumni_data['graduate_id']) ?>">
                                                <?php echo $alumni_data['alumn_firstname'] . ' ' . $alumni_data['alum_middlename'] . ' ' . $alumni_data['alum_surname'] ?>
                                            </a>
                                        </td>
                                        <td>
                                            <?php echo $get_educ['course'] ?>
                                        </td>
                                        <td>
                                            <?php echo $get_educ['year_graduated'] ?>
                                        </td>
                                        <td>
                                            <?php echo $alumni_data['alum_present_address'] ?>
                                        </td>
                                        <td>
                                            <?php echo $alumni_data['alum_email'] ?>
                                        </td>
                                    </tr>
                                <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php include '../footer.php'; ?>
        <script>
            $(document).ready(function(){
                $('#registered').dataTable();
            });
        </script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
