<?php include '../../config.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Update Alumni Information</title>
        <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/js/jquery.dataTables.js" type="text/javascript"></script>
        <link rel="icon" href="../../images/favicon.png" />
    </head>
    <body>
        <?php include '../../header.php'; ?>
        <div class="container" style="padding-top: 15px;"
        <?php
            if (isset($_POST['update'])) {
                $id = $_GET['id'];
                $n_user = $_POST['username'];
                $n_fname = $_POST['fullname'];
                $n_address = $_POST['address'];
                $n_email = $_POST['email'];
                $n_contact = $_POST['contact'];
                $n_cs = $_POST['cs'];
                $n_sex = $_POST['sex'];
                $n_bday = $_POST['birthday'];
                $n_religion = $_POST['religion'];
                $n_education = $_POST['education'];
                $n_batch = $_POST['batch'];
                
                $update = $mysqli->query("UPDATE alumni SET alumn_username = '$n_user', alumn_fullname = '$n_fname', alumn_address = '$n_address', alumn_email = '$n_email', alumn_contact = '$n_contact', alumn_civil_status = '$n_cs', alumn_sex = '$n_sex', alumn_birthday = '$n_bday', alumn_religion = '$n_religion', alumn_education = '$n_education', alumn_batch = '$n_batch' WHERE alumn_id = $id");
                if ($update) {
                    echo '<div class="alert-success">Alumni Information Successfully Updated!</div>';
                }
            }
            if ($_GET['id']) {
                $id = $_GET['id'];
                $data = $mysqli->query("SELECT * FROM alumni WHERE alumn_id = $id");
                $get_data = $data->fetch_assoc();
                
                $username = $get_data['alumn_username'];
                $fullname = $get_data['alumn_fullname'];
                $address = $get_data['alumn_address'];
                $email = $get_data['alumn_email'];
                $contact = $get_data['alumn_contact'];
                $cs = $get_data['alumn_civil_status'];
                $sex = $get_data['alumn_sex'];
                $bday = $get_data['alumn_birthday'];
                $religion = $get_data['alumn_religion'];
                $education = $get_data['alumn_education'];
                $batch = $get_data['alumn_batch'];
            }
        ?>
        <div class="mymodal">
            <div class="panel panel-default" style="border-radius: 0px;">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-edit"></span> Update Alumni Information
                </div>
                <form method="post" action="">
                <div class="panel-body mymodal-scroll">
                    <table class="table" width="100%" cellspacing="10px" cellpadding="10">
                            <tr>
                                <td align="right" width="50%">
                                    <label for="username">Username :</label>
                                </td>
                                <td align="left" width="50%">
                                    <div class="col-sm-7">
                                        <input type="text" value="<?php echo $username ?>" class="form-control" name="username" required />
                                    </div>
                                </td>
                            </tr>
                                <tr>
                                    <td align="right" width="50%"><label for="name">Full Name  :</label></td>
                                    <td align="left" width="50%">
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" value="<?php echo $fullname ?>" name="fullname" required />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="50%">
                                        <label for="address">Address :</label>
                                    </td>
                                    <td align="left" width="50%">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" value="<?php echo $address ?>" name="address" required />
                                        </div>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td align="right" width="50%">
                                        <label for="email">Email Address :</label>
                                    </td>
                                    <td align="left" width="50%">
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" value="<?php echo $email ?>" name="email" required />
                                        </div>
                                    </td>
                                </tr>                                                              
                                <tr>
                                    <td align="right" width="50%">
                                        <label for="contact">Telephone/Contact # :</label>
                                    </td>
                                    <td align="left" width="50%">
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" value="<?php echo $contact ?>" name="contact" required />
                                        </div>
                                    </td>
                                </tr>                                                              
                                <tr>
                                    <td align="right" width="50%">
                                        <label for="cs">Civil Status :</label>
                                    </td>
                                    <td align="left" width="50%">
                                        <div class="col-sm-8">
                                            <select name="cs" class="form-control">
                                                <option value=""><?php echo $cs ?></option>
                                                <option value="">--Select--</option>
                                                <option value="Single">Single</option>
                                                <option value="Maried">Maried</option>
                                                <option value="Separated">Separated</option>
                                                <option value="Single Parent">Single Parent</option>
                                                <option value="Window or Widower">Window or Widower</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>                                                              
                                <tr>
                                    <td align="right" width="50%">
                                        <label for="sex">Sex :</label>
                                    </td>
                                    <td align="left" width="50%">
                                        <div class="col-sm-10">
                                            <input type="radio" name="sex" value="Male" required />Male 
                                            <input type="radio" name="sex" value="Female" required />Female
                                        </div>
                                    </td>
                                </tr>                                                      
                                <tr>
                                    <td align="right" width="50%">
                                        <label for="birthday">Birthday :</label>
                                    </td>
                                    <td align="left" width="50%">
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" value="<?php echo $bday ?>" name="birthday" required />
                                        </div>
                                    </td>
                                </tr>                                                           
                                <tr>
                                    <td align="right" width="50%">
                                        <label for="religion">Religious Affiliation :</label>
                                    </td>
                                    <td align="left" width="50%">
                                        <div class="col-sm-9">
                                            <select name="religion" class="form-control">
                                                <option><?php echo $religion ?></option>
                                                <option value="">--Select--</option>
                                                <option value="Roman Catholic">Roman Catholic</option>
                                                <option value="Iglesia Ni Cristo">Iglesia Ni Cristo</option>
                                                <option value="Methodist">Methodist</option>
                                                <option value="Jehovah's Witnesses">Jehovah's Witnesses</option>
                                                <option value="Born Again Christian">Born Again Christian</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>                                                          
                                <tr>
                                    <td align="right" width="50%">
                                        <label for="education">Education (Degree/College/School/Year Graduated/Honors) :</label>
                                    </td>
                                    <td align="left" width="50%">
                                        <div class="col-sm-12">
                                            <textarea name="education" value="<?php echo $education ?>" placeholder="<?php echo $education ?>" class="form-control" required></textarea>
                                        </div>
                                    </td>
                                </tr>     
                                <tr>
                                    <td align="right" width="50%">
                                        <label for="batch">Batch:</label>
                                    </td>
                                    <td align="left" width="50%">
                                        <div class="col-sm-8">
                                            <input type="text" name="batch" class="form-control" value="<?php echo $batch ?>" required />
                                        </div>
                                    </td>
                                </tr>    
                            </table>
                </div>
                <div class="panel-footer">
                    <div class="btn-group">
                        <a href="../index.php" class="btn btn-danger">Close</a>
                        <button href="" name="update" class="btn btn-primary">Update</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        </div>
        <?php include '../../footer.php'; ?>        
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
