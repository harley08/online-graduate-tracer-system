<?php
    include '../../config.php';
    if (isset($_POST['batch'])) {
        $batch = $_POST['batch'];
        $check_batch_if_exists = $mysqli->query("SELECT * FROM batch_folders WHERE batch_year = '$batch'");
        $count_batch_matched = $check_batch_if_exists->num_rows;
        if ($count_batch_matched >= 1) {
            echo 'The batch folder you entered already exists';
        } else if ($count_batch_matched == null) {
            $create_batch_folder = $mysqli->query("INSERT INTO batch_folders (batch_year) VALUES ('$batch')");
            if ($create_batch_folder) {
                header("Location: ../");
            }
        }
    }
?>