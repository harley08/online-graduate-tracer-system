<?php
    include '../../config.php';
    if ($not_logged) {
        header("Location: $base_url/");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Unemployed</title>
        <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/js/jquery.dataTables.js" type="text/javascript"></script>
        <link rel="icon" href="../../images/favicon.png" />
        <style>
            input[type='search'],select[name='example_length'] {
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            color: #555555;
            vertical-align: middle;
            background-color: #ffffff;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
             -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            }
            input[type='search']:focus,select[name='example_length']:focus, input[type='search']:hover,select[name='example_length']:hover {
            border-color: #66afe9;
            outline: 0;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
            }
            .panel {
                -webkit-box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
                -moz-box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
                box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
            }
            tbody tr {
                padding: 20px !important;
            }
            td {
                padding: 10px !important;
            }
            label {
                padding: 10px;
            }
            .label-none {
                padding: 0px;
            }
            .dataTables_paginate {
                padding: 10px;
            }
            .dataTables_info {
                padding: 10px;
            }
            tbody, table {
                border-bottom: 1px solid #c0c0c0 !important;
            }
            thead {
                padding: 10px !important;
            }
        </style>
    </head>
    <body>
        <?php 
            include '../../header.php';
        ?>
        <div class="container" style="padding-top: 10px; padding-bottom: 0px;">
            <div class="row">
                <div class="col-md-3">
                    <div class="panel panel-default shadow">
                        <div class="panel-body">
                            <button data-toggle="modal" data-target="#addAlumni" class="btn btn-success btn-block"><span class="glyphicon glyphicon-file pull-left"></span> Add Alumni</button>
                            <a href="" class="btn btn-success btn-block"><span class="glyphicon glyphicon-print pull-left"></span> Print Preview</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-file"></span> Records</div>
                        <?php include '../tables/all.php'; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
            include '../modals.php';
            include '../../footer.php';
        ?>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
