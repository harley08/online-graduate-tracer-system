<?php
    include '../config.php';
    if ($not_logged) {
        header("Location: $base_url/");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Graduates</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.dataTables.js" type="text/javascript"></script>
        <link rel="icon" href="../images/favicon.png" />
        <link href="../assets/css/mymodal.css" rel="stylesheet" type="text/css"/>
        <style>
            input[type='search'],select[name='example_length'] {
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            color: #555555;
            vertical-align: middle;
            background-color: #ffffff;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
             -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            }
            input[type='search']:focus,select[name='example_length']:focus, input[type='search']:hover,select[name='example_length']:hover {
            border-color: #66afe9;
            outline: 0;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
            }
            .panel {
                -webkit-box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
                -moz-box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
                box-shadow: 0px 0px 2px 0px rgba(50, 50, 50, 0.15);
            }
            tbody tr {
                padding: 20px !important;
            }
            td {
                padding: 10px !important;
            }
            label {
                padding: 10px;
            }
            .label-none {
                padding: 0px;
            }
            .dataTables_paginate {
                padding: 10px;
            }
            .dataTables_info {
                padding: 10px;
            }
            tbody, table {
                border-bottom: 1px solid #c0c0c0 !important;
            }
            thead {
                padding: 10px !important;
            }
        </style>
    </head>
    <body>
        <?php 
            include '../header.php';
        ?>
        <div class="container" style="padding-top: 10px; padding-bottom: 0px;">
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading" align="center">
                            <h5 class="panel-title"><img src="../images/folder.png" width="20px" class="pull-left" /> Batch Folders</h5>
                        </div>
                        <div class="batch-folder">
                            <form method="post" action="" role="form">
                                <small><span class="glyphicon glyphicon-search"></span> Quick folder search</small>
                                <input type="text" name="search_folder_txt" class="form-control" placeholder="e.g. 2009-2010" required />
                            </form>
                        </div>
                        <?php
                            if (isset($_POST['search_folder_txt'])) {
                                $batch_year = $_POST['search_folder_txt'];
                                $batch_folders = $mysqli->query("SELECT * FROM batch_folders WHERE batch_year = '$batch_year' ORDER BY batch_id ASC");
                            } else {
                            $batch_folders = $mysqli->query("SELECT * FROM batch_folders ORDER BY batch_id ASC");
                            }
                            $count_folders = $batch_folders->num_rows;
                            if ($count_folders == null) { ?>
                        <div class="panel-body">
                            <center>no folders created yet</center>
                        </div>
                            <?php }
                            for ($i = 1; $i <= $count_folders; $i++) {                                
                                $folder_data = $batch_folders->fetch_assoc();
                                $folder_name = $folder_data['batch_year']; ?>
                        <div id="accordion">
                            <div class="batch-folder">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#folder<?php echo $i ?>">
                                    <span style="margin-right: 10px;" class="glyphicon glyphicon-folder-open pull-left"></span> <?php echo $folder_name ?>
                                </a>
                            </div>
                            <div id="folder<?php echo $i ?>" class="panel-collapse collapse">
                                    <?php
                                        include 'batch-modals.php';
                                        $query_count_graduates_fromFolder = $mysqli->query("SELECT * FROM graduates WHERE graduate_batch = '$folder_name'");
                                        $countGraduatesFromFolder = $query_count_graduates_fromFolder->num_rows;
                                    ?>
                                <div style="background-color: #f8f8f8; padding: 10px;">
                                    <b><span class="glyphicon glyphicon-education"></span> Graduates <?php echo $countGraduatesFromFolder ?></b>
                                </div>
                                <div class="panel-body" style="border-bottom: 1px solid #c0c0c0;">
                                    <?php
                                        $graduates_lists = $mysqli->query("SELECT * FROM graduates WHERE graduate_batch = '$folder_name'");
                                        $count_graduates_fromFolder = $graduates_lists->num_rows;
                                        
                                        while ($folder_graduate_data = $graduates_lists->fetch_assoc()) {
                                            /* Replace Graduate Courses To ACRONYM */
                                            $folder_graduate_course = $folder_graduate_data['graduate_course'];
                                            if ($folder_graduate_course == "Bachelor of Science in Information Technology") {
                                                $folder_graduate_course = "BSIT";
                                            } else if ($folder_graduate_course == "Bachelor of Science in Hotel & Tourism Management") {
                                                $folder_graduate_course = "BSHTM";
                                            } else if ($folder_graduate_course == "Bachelor of Science in Hotel & Restaurant Management") {
                                                $folder_graduate_course = "BSHRM";
                                            } else if ($folder_graduate_course == "Bachelor of Science in Computer Engineering") {
                                                $folder_graduate_course = "BSCPE";
                                            } else if ($folder_graduate_course == "Bachelor of Science in Entrepreneurship") {
                                                $folder_graduate_course = "BSENTREP";
                                            } else if ($folder_graduate_course == "Bachelor of Science in Fisheries") {
                                                $folder_graduate_course = "BSFI";
                                            } else if ($folder_graduate_course == "Bachelor of Science in Criminology") {
                                                $folder_graduate_course = "BSCRIM";
                                            } else if ($folder_graduate_course == "Bachelor of Secondary Education") {
                                                $folder_graduate_course = "BSED";
                                            } else if ($folder_graduate_course == "Bachelor of Elementary Education") {
                                                $folder_graduate_course = "BEED";
                                            }
                                            
                                            $folder_graduate_surname = $folder_graduate_data['graduate_surname'];
                                            $folder_graduate_middlename = $folder_graduate_data['graduate_middlename'];
                                            $folder_graduate_firstname = $folder_graduate_data['graduate_firstname']; ?>
                                    <?php show_photo_profile($folder_graduate_data['graduate_id'], "20px") ?> <a href="<?php get_profile_link($folder_graduate_data['graduate_id']) ?>"><?php echo $folder_graduate_firstname ?> <?php echo $folder_graduate_middlename ?> <?php echo $folder_graduate_surname ?></a> <b><?php echo $folder_graduate_course ?></b><br>                                    
                                        <?php }
                                        if ($count_graduates_fromFolder != 0) {
                                            echo '<center style="margin-top: 10px;"><a href="#folder-modal'.$i.'"><small>View All</small></a></center>';
                                        }
                                        if ($count_graduates_fromFolder == null) {
                                            echo '<center>no graduates added yet</center>';
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                            <?php }
                        ?>
                        <div class="panel-footer">
                            <a data-toggle="modal" data-target="#createBatchFolder" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-folder-open pull-left"></span> Create Batch Folder</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-body">
                    <?php
                        if(isset($_GET['alert'])) {
                            if ($_GET['alert'] == "added") { ?>
                    <div class="alert alert-success shadow">
                        Graduate Added!
                    </div>
                            <?php } else if ($_GET['alert'] == "updated") { ?>
                            <div class="alert alert-success shadow">
                                Graduate Information Successfully Updated!
                            </div>
                    <?php  } else if ($_GET['alert'] == "deleted") { ?>
                        <div class="alert alert-success shadow">
                                Graduate Information Successfully Deleted!
                        </div>
                    <?php }
                     } ?>
                    Actions:<br>
                    <button style="border-radius: 0px;" data-toggle="modal" data-target="#addAlumni" class="btn btn-default"><span class="glyphicon glyphicon-user"></span> Add Graduate</button>
                    <button onclick="printPreview()" type="button" style="border-radius: 0px;" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print Preview</button>
                    <script>
                        function printPreview() {
                            print();
                        }
                    </script>
                    <?php include 'tables/all.php'; ?>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <?php
            include 'modals.php';
            include '../footer.php';
        ?>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
