<style>
    .modal-body img {
        width: 99%;
    }
</style>
<!-- View photo profile modal -->
<div id="viewdpModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title"><span class="glyphicon glyphicon-camera"></span> Profile Picture</h5>
            </div>
            <div class="modal-body" align="center">
                <img src="<?php echo $base_url . '/profile/' . $prof_dp_link ?>" style="max-width: 800px;" class="panel-ava-img" />
            </div>
        </div>
    </div>
</div>

<!-- Change profile picture modal -->
        <div class="modal fade" id="uploadPhoto" tabindex="-1" role="dialog" aria-labelleby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h5><span class="glyphicon glyphicon-camera"></span> Change Profile Picture</h5>
                    </div>
                    <form method="post" action="update-profile-pic.php" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="file" name="photo" required />
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="upload" class="btn btn-primary" value="Upload Photo">
                    </div>
                    </form>
                </div>
            </div>
        </div>
<!-- End change profile picture modal -->

<!-- Send Message Modal -->
<div id="message" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 style="margin: 0px;"><small><span class="glyphicon glyphicon-envelope"></span></small> To: <?php echo $prof_fullname ?></h5>
            </div>
            <form method="post" action="send-message.php?receiver_id=<?php echo $profile_id ?>&to=<?php echo $prof_fullname ?>&receiver_profile_link=<?php echo $_SERVER['REQUEST_URI'] ?>" role="form">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="msg">Your Message:</label>
                        <textarea name="msg" class="form-control" style="min-height: 200px;"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><small><span class="glyphicon glyphicon-collapse-down"></span></small> Cancel</button>
                        <button type="submit" class="btn btn-primary"><small><span class="glyphicon glyphicon-send"></span></small> Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- nav bar -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="row">
    <div class="col-sm-3">
        <ul class="nav nav-pills nav-stacked nav-email shadow mb-20">
            <?php if ($profile_user) { ?>
            <?php } ?>
            <?php if (!$profile_user) { ?>
            <li <?php if (!$profile_user) { echo 'class="active"'; } ?>>
                <a data-toggle="modal" data-target="#message"><span class="glyphicon glyphicon-envelope"></span> Send Message</a>
            </li>
            <?php } ?>
            <li><a href="my-events.php?graduate_id=<?php echo $profile_id ?>"> <i class="glyphicon glyphicon-calendar"></i> Events <span class="label label-info pull-right inbox-notification"><?php echo $count_created_events ?></span></a></li>
            <li>
                <?php
                    $count_shouts = $mysqli->query("SELECT * FROM shouts WHERE graduate_id = $profile_id");
                ?>
                <a href="user-shouts.php?id=<?php echo $profile_id ?>">
                    <span class="glyphicon glyphicon-comment"></span> My Shouts <span class="label label-info pull-right inbox-notification"><?php echo $count_shouts->num_rows; ?></span>
                </a>
            </li>
        </ul><!-- /.nav -->

        <!--<h5 class="nav-email-subtitle">More</h5>
        <ul class="nav nav-pills nav-stacked nav-email mb-20 rounded shadow">
            <li>
                <a href="#">
                    <i class="fa fa-folder-open"></i> Promotions  <span class="label label-danger pull-right">3</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-folder-open"></i> Job list
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-folder-open"></i> Backup
                </a>
            </li>
        </ul><!-- /.nav -->
        <div class="panel panel-default" style="margin-top: 10px;">
            <div class="panel-heading">
                <h5 class="panel-title"><span class="glyphicon glyphicon-camera"></span> Photos</h5>
            </div>
            <div class="panel-body">
                <style>
                    .photo {
                        background-color: #c0c0c0;
                        width: 30px;
                        height: 30px;
                        display: inline-block;
                        overflow: hidden;
                        text-align: center;
                        border: 2px solid #ffffff;
                    }
                    .photo img {
                        height: 100%;
                    }
                </style>
                <?php
                    $profile_photos = $mysqli->query("SELECT * FROM gallery_photos WHERE graduate_id = $profile_id");
                    $count_profile_photos = $profile_photos->num_rows;
                    if ($count_profile_photos >= 24) {
                    for ($i = 1; $i <= 24; $i++) { 
                    $profile_photos_data = $profile_photos->fetch_assoc()?>
                <div class="photo">
                    <img src="<?php echo $base_url . '/gallery/' . $profile_photos_data['photo_link'] ?>" />
                </div> 
                    <?php } } else { 
                        while ($profile_photos_data = $profile_photos->fetch_assoc()) {
                        ?>
                <div class="photo">
                    <img src="<?php echo $base_url . '/gallery/' . $profile_photos_data['photo_link'] ?>" />
                </div>  
                    <?php } }
                ?>
            </div>
            <div class="panel-footer">
                <center>
                    <a href="my-photos.php?id=<?php echo $profile_id ?>">View All Photos</a>
                </center>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        
        <!-- resumt -->
        <div class="panel panel-default" style="margin-top: 0px !important;">
               <div class="panel-heading resume-heading">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="col-xs-12 col-sm-4">
                            <style>
                                .chg-dp {
                                    color: #c0c0c0;
                                    position: relative;
                                    right: 0;
                                    bottom: 0 !important;
                                    left: 0;
                                    pointer-events: none;
                                    padding-top: 5px;
                                    padding-left: 5px;
                                }
                                .chg-dp:hover {
                                    color: #000000 !important;
                                }
                                .dp {
                                    background-image: url("<?php echo $base_url ?>/profile/<?php echo $prof_dp_link ?>");
                                    background-size: 250px;
                                    background-color: #fafafa;
                                    background-position: 50% center;
                                    background-repeat: no-repeat;
                                    min-width: 200px;
                                    min-height: 250px;
                                }
                                .profile-ava {                                    
                                    border: 5px solid #ffffff;
                                    width: 200px !important;
                                    height: 200px !important;
                                    overflow: hidden;
                                    vertical-align: middle;                                                                      
                                    display: table-cell;
                                    text-align: center;
                                }
                                .profile-ava img {
                                    max-height: 200px;
                                    max-width: 200px;
                                }
                            </style>
                           <figure>                               
                                <div class="profile-ava shadow">                                    
                               <?php if ($profile_user) { ?>
                                    <a style="position: absolute; bottom: 0; right: 0; margin-bottom: 10px; margin-right: 70px;" data-toggle="modal" data-target="#uploadPhoto" class=""><span class="glyphicon glyphicon-camera pc-view chg-dp"></span></a>
                                <?php } ?>
                                    <a href="<?php echo $base_url ?>/profile/<?php echo $prof_dp_link ?>" data-fancybox="group" data-caption="<?php $prof_fullname ?>">
                                        <img  src="<?php echo $base_url ?>/profile/<?php echo $prof_dp_link ?>" />
                                    </a>
                                </div>
                               <a class="mobile-view" data-toggle="modal" data-target="#uploadPhoto">Change Profile Picture</a>
                           </figure>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                           <ul class="list-group">
                              <li class="list-group-item"><b><?php echo $prof_fullname ?></b></li>
                              <li class="list-group-item"><?php echo $prof_company_position ?></li>
                              <li class="list-group-item"><?php echo $prof_company ?></li>
                              <li class="list-group-item"><span class="glyphicon glyphicon-phone"></span> <?php echo $prof_contact_phone . ' / ' . $prof_landline ?></li>
                              <li class="list-group-item"><span class="glyphicon glyphicon-envelope"></span> <?php echo $prof_email ?></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="bs-callout bs-callout-danger">
                  <h4>About</h4>
                  <p>
                      <?php echo $prof_gender_replace ?> graduated at the <b><?php echo $profile_educBackground_data['school'] ?></b> year <?php echo $profile_educBackground_data['year_graduated'] ?> with the course, <b><?php echo $profile_educBackground_data['course'] ?></b>. 
                      <?php echo $prof_gender_replace ?> is currently residing at the <b><?php echo $prof_alum_present_address ?></b>. 
                      <?php echo $prof_gender_replace ?> is now <b><?php echo $prof_alum_age ?> years old</b> and <b><?php echo $prof_alum_civil_status ?></b>.
                  </p>
               </div>
               <div class="bs-callout bs-callout-danger">
                  <h4>Educational Background</h4>
                  <table class="table table-striped table-responsive">
                      <thead>
                          <tr>
                              <td><b>School</b></td>
                              <td><b>Course</b></td>
                              <td><b>Major</b></td>
                              <td><b>Year Gradated</b></td>
                              <td><b>Level</b></td>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                            $profile_educ_background_list = $mysqli->query("SELECT * FROM alumni_educational_background WHERE graduate_id = $profile_id") or die($mysqli->error);
                            while ($profile_educ_background_data = $profile_educ_background_list->fetch_assoc()) { ?>
                          <tr>
                              <td><small><?php echo $profile_educ_background_data['school'] ?></small></td>
                              <td><small><?php echo $profile_educ_background_data['course'] ?></small></td>
                              <td><small><?php echo $profile_educ_background_data['major'] ?></small></td>
                              <td><small><?php echo $profile_educ_background_data['year_graduated'] ?></small></td>
                              <td><small><?php echo $profile_educ_background_data['education_level']; ?></small></td>
                          </tr>
                            <?php }
                          ?>
                      </tbody>
                  </table>
               </div>
               <div class="bs-callout bs-callout-danger">
                    <h4>Work Experience</h4>
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#employed" data-toggle="tab">Employed</a>
                        </li>
                        <li>
                            <a href="#unemployed" data-toggle="tab">Unemployed</a>
                        </li>
                        <li>
                            <a href="#self_employed" data-toggle="tab">Self Employed</a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active" id="employed">
                            <table class="table table-striped">
                                <?php 
                                $employed_tbl = $mysqli->query("SELECT * FROM work_experiences_employed WHERE graduate_id = $profile_id");
                                $count_employed_tbl = $employed_tbl->num_rows;
                                        if ($count_employed_tbl == null) { ?>
                                    <tr class="table table-striped" valign="bottom">
                                        <div style="position: relative; text-align: center; padding: 20px;">no data available</div>
                                    </tr>
                                        <?php } else {
                                    ?>
                                <thead>
                                    <tr>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Company Name</th>
                                        <th>Company Address</th>
                                        <th>Position</th>
                                        <th>Employment Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php                                       
                                        while ($employed_tbl_data = $employed_tbl->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td><small><?php echo $employed_tbl_data['date_from'] ?></small></td>
                                        <td><small><?php echo $employed_tbl_data['date_to'] ?></small></td>
                                        <td><small><?php echo $employed_tbl_data['company_name'] ?></small></td>
                                        <td><small><?php echo $employed_tbl_data['company_address'] ?></small></td>
                                        <td><small><?php echo $employed_tbl_data['company_position'] ?></small></td>
                                        <td><small><b><?php echo $employed_tbl_data['employment_status'] ?></b></small></td>
                                    </tr>                                    
                                </tbody>
                                <?php } } ?>
                            </table>
                        </div>
                        <div class="tab-pane" id="unemployed">
                            <table border="0" class="table table-striped">
                                <?php
                                $unemployed_tbl = $mysqli->query("SELECT * FROM work_experiences_unemployed WHERE graduate_id = $profile_id");
                                $count_unemployed_tbl = $unemployed_tbl->num_rows;
                                if ($count_unemployed_tbl == null) { ?>
                                <tr class="table table-striped" valign="bottom">
                                    <div style="position: relative; text-align: center; padding: 20px;">no data available</div>
                                </tr>
                                <?php } else {
                                ?>
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Reason</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    while ($unemployed_tbl_data = $unemployed_tbl->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td><?php echo $unemployed_tbl_data['unemployed_date'] ?></td>
                                        <td><?php echo $unemployed_tbl_data['unemployed_reasons'] ?></td>
                                    </tr>
                                </tbody>                                
                                <?php } } ?>
                            </table>

                        </div>
                        <div class="tab-pane" id="self_employed">
                            <table class="table table-striped">
                                <thead>
                                <?php 
                                    $self_employed_tbl = $mysqli->query("SELECT * FROM work_experiences_self_employed WHERE graduate_id = $profile_id");
                                    $count_self_employed_tbl = $self_employed_tbl->num_rows;
                                    if ($count_self_employed_tbl == null) { ?>
                                     <tr class="table table-striped" valign="bottom">
                                            <div style="position: relative; text-align: center; padding: 20px;">no data available</div>
                                    </tr>   
                                    <?php } else {
                                ?>
                                    <tr>
                                        <th>Date</th>
                                        <th>Business Type</th>
                                        <th>Reason</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while($self_employed_tbl_data = $self_employed_tbl->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td><?php echo $self_employed_tbl_data['self_employed_date'] ?></td>
                                        <td><?php echo $self_employed_tbl_data['self_employed_business_type'] ?></td>
                                        <td><?php echo $self_employed_tbl_data['self_employed_reasons'] ?></td>
                                    </tr>
                                </tbody>
                                    <?php } } ?>
                            </table>
                        </div>
                    </div>
               </div>
            <?php
                if ($logged_admin || $logged_fullname == $prof_fullname) {
            ?>
            <div class="bs-callout bs-callout-danger">                
                <?php
                    $survey_questions = $mysqli->query("SELECT * FROM alumni_questions WHERE graduate_id = $profile_id");
                    $survey_questions_data = $survey_questions->fetch_assoc(); 
                ?>
                <div id="update_survey" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">x</button>
                                <h5 class="modal-title">Update My Survey Answers</h5>
                            </div>
                            <form method="post" role="form" action="update-survey.php?id=<?php echo $profile_id ?>">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="job_satisfy">Are you satisfy with your job right now?</label>                                    
                                    <?php
                                        if ($survey_questions_data['job_satisfy'] == "Yes") { ?>
                                    <input type="checkbox" checked value="Yes" name="job_satisfy" /> Yes <input type="checkbox" value="No" name="job_satisfy" /> No
                                        <?php } else { ?>
                                    <input type="checkbox" value="Yes" name="job_satisfy" /> Yes <input type="checkbox" value="No" name="job_satisfy" checked /> No        
                                        <?php }
                                    ?>                                    
                                </div>
                                <div class="form-group">
                                    <label for="job_satisfy_reason">Reason:</label>
                                    <textarea name="job_satisfy_reason" class="form-control"><?php echo $survey_questions_data['job_satisfy_reason'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="resons_why_accept_present_job"> Kindly state your reason/s of accepting your present job.</label>
                                    <textarea name="resons_why_accept_present_job" class="form-control"><?php echo $survey_questions_data['reasons_present_job'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="benefits_from_job">What are the benefits you derived from your job? Please specify.</label>
                                    <textarea name="benefits_from_job" class="form-control"><?php echo $survey_questions_data['benefits_from_job'] ?></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="btn-group">
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    <button type="submit" class="btn btn-primary" name="update_survey">Update</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                <h4>Survey Question <?php if ($profile_user) { ?> <a style="margin-left: 20px;" data-toggle="modal" data-target="#update_survey"><small><span class="glyphicon glyphicon-pencil"></span></small></a> <?php } ?></h4>
                <p>
                    <b>1.</b> Are you satisfied with your job right now? <b><?php echo $survey_questions_data['job_satisfy'] ?></b><br>
                    State your reasons:<br>
                    <u><?php echo $survey_questions_data['job_satisfy_reason'] ?></u>
                </p>
                <p>
                    <b>2.</b> Kindly state your reason/s of accepting your present job.<br>
                    <u><?php echo $survey_questions_data['reasons_present_job'] ?></u>
                </p>
                <p>
                    <b>3.</b> What are the benefits you derived from your job?  Please specify.<br>
                    <u><?php echo $survey_questions_data['benefits_from_job'] ?></u>
                </p>
            </div>
                <?php } ?>
            </div>
         </div>
        <!-- resume -->

    </div>