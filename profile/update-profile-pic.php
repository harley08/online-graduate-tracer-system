<?php include '../config.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Change Profile Picture</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />
    </head>
    <body>
        <?php
            if (isset($_POST['upload'])) {
                $image = addslashes(file_get_contents($_FILES['photo']['tmp_name']));
                $image_name = addslashes($_FILES['photo']['name']);
                $image_size = getimagesize($_FILES['photo']['tmp_name']);
                
                move_uploaded_file($_FILES['photo']['tmp_name'], "photos/dp/". $_FILES['photo']['name']);
                $location = "photos/dp/" . $_FILES['photo']['name'];
                
                $save = $mysqli->query("INSERT INTO profile_pictures (graduate_id, dp_link) VALUES ($user_graduate_id, '$location')");
                
                if ($save) {
                    header("Location: $user_profile_link");
                } else {
                    echo $mysqli->error;
                }
            }
        ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
