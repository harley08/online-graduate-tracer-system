<?php
    include '../config.php';
    if ($logged) {       
        
        /* Get logged user alumni id */
        $alumni_data = $mysqli->query("SELECT * FROM alumni_accounts WHERE account_username = '$logged_username'");
        $almni_data_get = $alumni_data->fetch_assoc();
        $alumn_id = $almni_data_get['graduate_id'];
        
        /* Check if the user has already generated their profile hash link */
        $check_user_profile_link = $mysqli->query("SELECT * FROM profile_links WHERE graduate = $alumn_id");
        $check_user_if_already = $check_user_profile_link->num_rows;
        
        if ($check_user_if_already == 1) {
            header("Location: /cg/profile/generate.php?#alert");
        } else if ($check_user_if_already == null) {        
        $profile_hash_link = $base_url .'/profile/?' . md5($logged_username);
        $generate_profile_hash = $mysqli->query("INSERT INTO profile_links (graduate_id, alumn_profile_hash_link) VALUES ($alumn_id, '$profile_hash_link')");
        if ($generate_profile_hash) {
            header("Location: $base_url/logout.php");
        } else {
            header("Location: $base_url");
        }
        } 
    }
?>
<div id="alert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title"><span class="glyphicon glyphicon-warning-sign"></span> Alert</h5>
            </div>
            <div class="modal-body">
                You alreay generated your own profile link.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>