<?php
    include '../config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php show_fullname($_GET['id']) ?>'s shouts</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/fancybox/jquery.fancybox.js" type="text/javascript"></script>
        <link rel="icon" href="../images/favicon.png" />
        <script type="text/javascript">
            $("[data-fancybox]").fancybox({ });
        </script>
    </head>
    <body>
        <?php include '../header.php'; ?>
        <div class="container" style="padding-top: 10px;">
            <div class="panel panel-default shadow">
                <div class="panel-body">
                    <div class="pull-right">
                        <a href="<?php get_profile_link($_GET['id']) ?>">
                            Back to Profile
                        </a>
                    </div>
                    <?php
                    show_photo_profile($_GET['id'], "20px")
                    ?>
                    <b>
                    <?php 
                    show_fullname($_GET['id']);
                    ?>
                    </b>'s shouts
                </div>
            </div>
            <?php
    /* shouts display limit */
    $shout_display_limit = 10;
    
    /* get the total count of all shouts */
    $count_all_shouts = $mysqli->query("SELECT * FROM shout_comments");
    $count_AllShouts = $count_all_shouts->num_rows;
    
    /* if the see more button is triggered */
    if (isset($_GET['page'])) {
        $page = $_GET['page'] + 1;
        $offset = $shout_display_limit * $page;
    } else {
        $page = 0;
        $offset = 0;
    }
    
    $left_shouts = $count_AllShouts - ($page * $shout_display_limit);
    
    $query_shouts = $mysqli->query("SELECT * FROM shouts WHERE graduate_id = ".$_GET['id']." ORDER BY shout_id DESC LIMIT $offset, $shout_display_limit");
    
    while ($shout_data = $query_shouts->fetch_assoc()) {
        
        $shout_graduate_id = $shout_data['graduate_id'];
        
        $query_dp_link = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = $shout_graduate_id ORDER BY dp_id DESC");
        $fetch_dp_link = $query_dp_link->fetch_assoc();
        $shout_dp_link = $fetch_dp_link['dp_link'];
        
        $query_shout_fullname = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = $shout_graduate_id");
        $data_shout_fullname = $query_shout_fullname->fetch_assoc();
        $shout_alum_surname = $data_shout_fullname['alum_surname'];
        $shout_alumn_firstname = $data_shout_fullname['alumn_firstname'];
        $shout_alum_middlename	 = $data_shout_fullname['alum_middlename'];
        $shout_user_fullname = $shout_alumn_firstname . ' ' . $shout_alum_middlename . ' ' . $shout_alum_surname;
        if ($shout_user_fullname == NULL) {
            $$shout_user_fullname = $shout_data['account_username'];
        }
        
        /* Count the shout comments */
        $count_comments = $mysqli->query("SELECT * FROM shout_comments WHERE shout_id = ".$shout_data['shout_id']."");
        $countComments = $count_comments->num_rows;
        ?>
<div class="panel panel-default shadow" style="border-radius: 0px; border: 1px solid #e6e6e6;">
    <div class="panel-heading" style="padding: 10px;">
        <a><img src="<?php echo $base_url . '/profile/' . $shout_dp_link ?>" class="shadow" style="margin: 0px !important; border: 2px solid #ffffff; margin-bottom: 5px !important;" width="50px" /></a>
        <span style="margin-left: 10px;"><a href="<?php get_profile_link($shout_data['graduate_id']) ?>"><?php echo $shout_user_fullname; ?></a></span>
        <?php 
            if ($logged_admin) {
                ?>
        <font size="1px"><span class="glyphicon glyphicon-list pull-right"></span></font>
        <?php
            }
        ?>
    </div>
    <div class="panel-body">
        <?php convert_smiley($shout_data['shout_msg']) ?>
        <?php
            if ($shout_data['shout_photo'] != null) { ?>
        <br>
        <a href="../<?php echo $shout_data['shout_photo'] ?>" data-fancybox="group" data-caption="<?php echo $shout_data['shout_msg'] ?>">
        <img src="../<?php echo $shout_data['shout_photo'] ?>" width="50%" />
        </a>
            <?php }
        ?>
    </div>
    <div class="panel-footer" style="background-color: #f8f8f8">
        <small>
            <font style="color: #616161">
        <?php
            echo date("F j, Y", strtotime($shout_data['shout_date']));
        ?>
            </font>
        </small>
    </div>
    <div class="panel-footer">
        <a href="../shouts/shout-comments.php?shout_id=<?php echo $shout_data['shout_id'] ?>"><small><span class="glyphicon glyphicon-comment"></span></small> Comment(s) <b><?php echo $countComments ?></b></a>
        <?php
            if ($countComments >= 1) {
                $show_last_comment = $mysqli->query("SELECT * FROM shout_comments WHERE shout_id = ".$shout_data['shout_id']." ORDER BY comment_id DESC") or die($mysqli->error);
                $show_last_comment_data = $show_last_comment->fetch_assoc(); ?>
        <div class="shadow" style="background-color: #ffffff; border-radius: 3px; padding: 10px; margin: 3px;">
            <small>Last commented by:</small><br>
            <?php show_photo_profile($show_last_comment_data['graduate_id'], "30px") ?> <b><?php echo show_fullname($show_last_comment_data['graduate_id']) ?></b><br>
            <div style="margin-top: 5px; padding-left: 35px;">
                <?php echo $show_last_comment_data['comment_content'] ?>
            </div>
        </div>
            <?php }
        ?>
    </div>
</div>
        <?php
    }
?>
        </div>
        <?php include '../footer.php'; ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
