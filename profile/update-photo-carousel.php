        <div class="modal fade" id="carouselModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h5 style="margin: 0px;"><span class="glyphicon glyphicon-camera"></span> Upload</h5>
                    </div>
                    <form method="post" action="upload-carousel.php" enctype="multipart/form-data">
                        <div class="modal-body">
                            <input type="file" name="photo1" required />
                            <input type="file" name="photo2" required />
                            <input type="file" name="photo3" required />
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                <button type="submit" name="upload" class="btn btn-primary">Upload</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>