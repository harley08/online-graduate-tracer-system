<?php
    include '../config.php';
    include 'profiledata.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $prof_fullname ?>'s Profile</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/fancybox/jquery.fancybox.js" type="text/javascript"></script>
        <link rel="icon" href="../images/favicon.png" />
        <!--<style>
            .nav-tabs {
                background-color: #31c706 !important;
                padding: 10px;
                padding-bottom: 0px;
            }
            .nav-tabs li a {
                color: #ffffff;
            }
            .active li a {
                color: #31c706 !important;
            }
        </style>-->
        <script type="text/javascript">
            $("[data-fancybox]").fancybox({ });
        </script>
    </head>
    <body>
        <?php include '../header.php'; ?>
        <div class="container" style="padding-top: 10px; padding-bottom: 10px; margin-bottom: 0px !important;">
            <div class="pull-right">
                <button type="button" onclick="printProfile()" class="btn btn-default" style="margin-bottom: 5px;"><small><span class="glyphicon glyphicon-print"></span></small> Print Profile</button>
            </div>
            <script>
                function printProfile() {
                    print();
                }
            </script>
        <?php include 'modals.php'; ?>
<!--            <div class="row">
                <div class="col-md-3">
                    <div class="panel-ava" align="center">
                        <a data-toggle="modal" data-target="#viewdpModal"><img data-toggle="tooltip" data-placement="top" title="Click to view" src="<?php echo $base_url . '/profile/' . $prof_dp_link ?>" width="200px" class="panel-ava-img" /></a>
                        <?php
                            if (!$profile_user) {
                                ?>
                        <div class="panel-ava-menu">
                            <div class="btn-group">
                                <a data-toggle="modal" data-target="#message" class="btn btn-success"><small><span class="glyphicon glyphicon-envelope"></span></small> Send Message</a>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                        <div class="panel-ava-menu"><?php echo $prof_fullname ?></div>
                        <div class="panel-ava-menu">
                            Status: <b>Fresh Graduate</b>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 style="margin: 0px;"><span class="glyphicon glyphicon-calendar"></span> My Created Events</h5>
                        </div>
                        <div class="panel-body"></div>
                    </div>
                </div>
                <div class="col-md-9">
                    <?php include 'photo-carousel.php'; ?>
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#pInfo" href="" data-toggle="tab">
                                Personal Information
                            </a>
                        </li>
                        <li>
                            <a href="#accInfo" data-toggle="tab">
                                Account Information
                            </a>
                        </li>
                        <li>
                            <a href="#education" data-toggle="tab">
                                Education
                            </a>
                        </li>
                        <li>
                            <a href="#employment" data-toggle="tab">
                                Employment
                            </a>
                        </li>
                        <?php
                            if ($profile_user) { ?>
                        <li class="pull-right">
                            <a data-toggle="modal" data-target="#updateprofile" class="active-none"><span data-toggle="tooltip" data-placement="top" title="Edit Profile Information" class="glyphicon glyphicon-edit"></span></a>
                        </li> 
                          <?php  } 
                        ?>
                    </ul>  
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" style="background-color: #ffffff; color: #000000 !important;" id="pInfo">
                            <div class="panel-prof shadow" style="border-top: 0px solid #000000;">
                                <table width="100%" class="table" style="margin-bottom: 0px;">
                                    <tr>
                                        <td width="20%" align="right"><b>Fullname :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_fullname ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="20%" align="right"><b>Gender :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_sex ?></td>
                                    </tr>    
                                    <tr>
                                        <td width="20%" align="right"><b>Birthday :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_bday ?></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="right"><b>Civil Status :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_cs ?></td>
                                    </tr>    
                                    <tr>
                                        <td width="20%" align="right"><b>Religion :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_religion ?></td>
                                    </tr>                                   
                                    <tr>
                                        <td width="20%" align="right"><b>Address :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_address ?></td>
                                    </tr>    
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" style="background-color: #ffffff; color: #000000 !important;" id="accInfo">
                            <div class="panel-prof shadow" style="border-top: 0px solid #000000;">
                                <table width="100%" class="table" style="margin-bottom: 0px;">
                                    <tr>
                                        <td width="20%" align="right"><b>Username :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_username ?></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="right"><b>Contact :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_contact ?></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="right"><b>Email :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_email ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" style="background-color: #ffffff; color: #000000 !important;" id="education">
                            <div class="panel-prof shadow" style="border-top: 0px solid #000000;">
                                <table width="100%" class="table" style="margin-bottom: 0px;">
                                    <tr>
                                        <td width="20%" align="right"><b>School Graduated :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_fullname ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="20%" align="right"><b>Year Graduated :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_sex ?></td>
                                    </tr>    
                                    <tr>
                                        <td width="20%" align="right"><b>Honor(s) :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_bday ?></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="right"><b>Civil Status :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_cs ?></td>
                                    </tr>    
                                    <tr>
                                        <td width="20%" align="right"><b>Religion :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_religion ?></td>
                                    </tr>                                   
                                    <tr>
                                        <td width="20%" align="right"><b>Address :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_address ?></td>
                                    </tr>    
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" style="background-color: #ffffff; color: #000000 !important;" id="employment">
                            <div class="panel-prof shadow" style="border-top: 0px solid #000000;">
                                <table width="100%" class="table" style="margin-bottom: 0px;">
                                    <tr>
                                        <td width="20%" align="right"><b>School Graduated :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_fullname ?></td>
                                    </tr> 
                                    <tr>
                                        <td width="20%" align="right"><b>Year Graduated :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_sex ?></td>
                                    </tr>    
                                    <tr>
                                        <td width="20%" align="right"><b>Honor(s) :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_bday ?></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="right"><b>Civil Status :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_cs ?></td>
                                    </tr>    
                                    <tr>
                                        <td width="20%" align="right"><b>Religion :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_religion ?></td>
                                    </tr>                                   
                                    <tr>
                                        <td width="20%" align="right"><b>Address :</b></td>
                                        <td width="80%" align="left"><?php echo $prof_address ?></td>
                                    </tr>    
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php include 'update-photo-carousel.php'; ?>
                    <script>
                        $(function(){
                        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                        // Get the name of active tab
                        var activeTab = $(e.target).text();
                        // Get the name of previous tab
                        var previousTab = $(e.relatedTarget).text();
                        $(".active-tab span").html(activeTab);
                        $(".previous-tab span").html(previousTab);
                        }); 
                        });
                        $(function () {
                        $('[data-toggle="tooltip"]').tooltip()
                        })
                    </script>
                    <?php include 'profile-photos.php' ?>
                </div>
            </div> -->
        </div>
        <?php include '../footer.php'; ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>