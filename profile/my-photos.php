<?php include '../config.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <title><?php show_fullname($_GET['id']) ?>'s Photos</title>
    </head>
    <body>
        <style>
            .gallery-contents {
                background-color: #f8f8f8;
                padding-top: 10px;
                padding-left: 0px !important;
            }
            .photo {
                background-color: #c0c0c0;
                width: 150px;
                height: 200px;
                display: inline-block;
                overflow: hidden;
                text-align: center;
                border: 2px solid #ffffff;
            }
            .photo:hover {
                border-color: #66afe9;
                outline: 0;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
            }
            .photo img {
                height: 100%;
            }
        </style>
        <?php include '../header.php'; ?>
        <div class="container" style="padding-top: 10px !important;">
            <div class="panel panel-default shadow">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <a href="<?php get_profile_link($_GET['id']) ?>"><span class="glyphicon glyphicon-camera"></span> <?php show_fullname($_GET['id']) ?></a>'s Photos
                    </h5>
                </div>
                <div class="panel-body">
                    <div class="gallery-contents shadow" align="center">
                        <style>
                            .album-content {
                                width: 200px;
                                border: 2px solid #f5f5f5;
                                margin: 10px;
                                margin-right: 0px !important;
                                display: inline-table;
                            }
                            .album-content:hover {
                                border-color: #66afe9;
                                outline: 0;
                                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
                                box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
                            }
                            .album {
                                background-image: url("../images/photo-album-default-background.png");
                                background-color: #c0c0c0;
                                width: 198px;
                                height: 200px;
                                overflow: hidden;
                            }
                            .album-title {
                                background-color: #e8e9e8;
                                padding: 10px;
                                width: 198px;
                                border-top: 1px solid #ffffff;
                            }
                            .photo {
                                width: 200px;
                                height: 200px; 
                            }
                            .photo:hover {
                                width: 200px;
                                height: 200px;
                            }
                            .photo img {
                                width: 100%;
                            }
                        </style>
                        <?php
                            $albums = $mysqli->query("SELECT * FROM gallery_albums WHERE graduate_id = ".$_GET['id']."");
                            $count_albums = $albums->num_rows;
                            while ($album_data = $albums->fetch_assoc()) { ?>
                        <a href="<?php echo $base_url ?>/gallery/photo-album/?id=<?php echo $album_data['album_id'] ?>&album=<?php echo $album_data['album_title'] ?>" data-toggle="tooltip" data-placement="top" title="Click to view photos">
                        <div class="album-content">
                            <div class="album shadow">
                                <div class="photo">
                                    <?php
                                        $galley_photos = $mysqli->query("SELECT * FROM gallery_photos WHERE album_id = ".$album_data['album_id']." ORDER BY photo_id DESC");
                                        $count_gallery_photos = $galley_photos->num_rows;
                                        for ($i = 1; $i <= 3; $i++) { 
                                        $gallery_photo_data = $galley_photos->fetch_assoc(); ?>
                                        <img src="<?php echo $base_url . '/gallery/' . $gallery_photo_data['photo_link']; ?>" class="img img-responsive" />
                                        <?php }
                                    ?>
                                </div>
                            </div>
                            <div class="album-title">
                                <small>
                                    <?php echo $album_data['album_title'] ?>
                                </small>
                            </div>
                        </div>
                        </a>
                            <?php }
                            if ($count_albums == null) {
                                echo '<center style="padding: 50px;">no albums created yet</center>';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include '../footer.php'; ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
