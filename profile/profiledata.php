<?php
    $profile_hash = $mysqli->query("SELECT * FROM profile_links WHERE alumn_profile_hash_link = '".$_SERVER['REQUEST_URI']."' ORDER BY profile_id DESC");
    
    $profile_hash_count = $profile_hash->num_rows;
    
    if ($profile_hash_count == null) {
        header("Location: $base_url");
    }
    
    $profile_hash_data = $profile_hash->fetch_assoc();
    $profile_id = $profile_hash_data['graduate_id'];
    
    /* Get personal information */
    $profile_personalInfo = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = $profile_id");
    $profile_personalInfo_data = $profile_personalInfo->fetch_assoc();
    $prof_surname = $profile_personalInfo_data['alum_surname'];
    $prof_firstname = $profile_personalInfo_data['alumn_firstname'];
    $prof_middlename = $profile_personalInfo_data['alum_middlename'];
    $prof_fullname = $prof_firstname . ' ' . $prof_middlename . ' ' . $prof_surname;
    $prof_contact_phone = $profile_personalInfo_data['alum_contact_phone'];
    $prof_landline = $profile_personalInfo_data['alum_contact_landline'];
    $prof_email = $profile_personalInfo_data['alum_email'];
    $prof_gender_replace = $profile_personalInfo_data['alum_gender'];
    if ($prof_gender_replace == "Male") {
        $prof_gender_replace = "He";
    } else if ($prof_gender_replace == "Female") {
        $prof_gender_replace = "She";
    }
    $prof_alum_present_address = $profile_personalInfo_data['alum_present_address'];
    $prof_alum_age = $profile_personalInfo_data['alum_age'];
    $prof_alum_civil_status = $profile_personalInfo_data['alum_civil_status'];
    
    /* Get educational background */
    $profile_educBackground = $mysqli->query("SELECT * FROM alumni_educational_background WHERE graduate_id = $profile_id AND education_level = 'Tertiary'");
    $profile_educBackground_data = $profile_educBackground->fetch_assoc();
    
    $profile_user = $user_graduate_id == $profile_id;
    
    /* Get profile user image link */
    $query_dp_link = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = $profile_id ORDER BY dp_id DESC");
    $fetch_dp_link = $query_dp_link->fetch_assoc();
    $prof_dp_link = $fetch_dp_link['dp_link'];
    
    $prof_company = "n/a";
    $prof_company_position = "n/a";
    
    /* check if graduate is employed */
    $check_if_employed = $mysqli->query("SELECT * FROM work_experiences_employed WHERE graduate_id = $profile_id");
    $check_if_exist_employed = $check_if_employed->num_rows;
    if ($check_if_exist_employed != null) {
        /* get the company name and position */
        $employed_data = $mysqli->query("SELECT * FROM work_experiences_employed WHERE graduate_id = $profile_id ORDER BY employed_id DESC");
        $employedData = $employed_data->fetch_assoc();
        
        $prof_company = $employedData['company_name'];
        $prof_company_position = $employedData['company_position'];
        
        if ($prof_company == null) {
            $prof_company = "unemployed";
        }
        if ($prof_company_position == null) {
            $prof_company_position = "unemployed";
        }
    }
    
    /* count created events */
    $created_events = $mysqli->query("SELECT * FROM events WHERE graduate_id = $profile_id");
    $count_created_events = $created_events->num_rows;
?>