<?php
    $photo_carousel = $mysqli->query("SELECT * FROM photo_carousels WHERE carousel_user = '$prof_username'");
    $carousel_data = $photo_carousel->fetch_assoc();
    $carousel1_link = $carousel_data['carousel_1'];
    $carousel2_link = $carousel_data['carousel_2'];
    $carousel3_link = $carousel_data['carousel_3'];
?>
<div style="margin-bottom: 10px;" id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">  
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
      <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
    </ol>
    <div class="carousel-inner">
      <div class="item active">
          <img alt="First slide" src="<?php echo $base_url ?>/profile/<?php echo $carousel1_link ?>">
      </div>
      <div class="item">
        <img alt="Second slide" src="<?php echo $base_url ?>/profile/<?php echo $carousel2_link ?>">
      </div>
      <div class="item">
        <img alt="Third slide" src="<?php echo $base_url ?>/profile/<?php echo $carousel3_link ?>">
      </div>
    </div>
    <?php
        if ($profile_user) {
            ?>    
    <button data-toggle="modal" data-target="#carouselModal" style="bottom: 0; right: 0; margin-bottom: 10px; margin-right: 10px; position: absolute !important;" type="button" class="btn-hover pull-right"><span class="glyphicon glyphicon-camera"></span> Change Photo Carousels</button>
    <?php
        }
    ?>
    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>