<?php
    include '../config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo show_fullname($_GET['graduate_id']) ?> Events</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />
        <style>
            .event-cover-img {
                
            }
            .panel-hovered {
                
            }
            .panel-hovered:hover {
                border-color: #66afe9;
                outline: 0;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
            }
        </style>
    </head>
    <body>
        <?php include '../header.php'; ?>
        <div class="container" style="padding-top: 10px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <span class="glyphicon glyphicon-calendar"></span> <b><?php echo show_fullname($_GET['graduate_id']) ?></b> events
                    </h5>
                </div>
                <div class="panel-body">                    
                    <?php
                        $events = $mysqli->query("SELECT * FROM events WHERE graduate_id = ".$_GET['graduate_id']." ORDER BY event_id DESC");
                        $count_events = $events->num_rows;
                        if ($count_events == null) {
                            echo '<center><h4>no events posted yet</h4></center>';
                        }
                        while ($events_data = $events->fetch_assoc()) { 
                            /* Get the user who posted the event */
                            $poster = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = ".$events_data['graduate_id']."");
                            $poster_data = $poster->fetch_assoc();
                            
                            /* Get the poster dp */
                            $poster_dp = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = ".$events_data['graduate_id']." ORDER BY dp_id DESC");
                            $poster_dp_data = $poster_dp->fetch_assoc();
                            ?>                    
                    <div class="panel panel-default panel-hovered shadow">
                        <div class="event-cover-img">                       
                            <span style="position: absolute; margin-left: 20px; padding-top: 130px;"><h3><font style="color: #606060;"><span class="glyphicon glyphicon-calendar"></span> <a href="<?php echo $base_url ?>/events/action/view-event.php?id=<?php echo $events_data['event_id']  ?>&?<?php echo $events_data['event_title'] ?>"><?php echo $events_data['event_title'] ?></a></font> - <?php echo $events_data['event_date'] ?></h3></span>
                            <img src="../images/event-cover-img.png" width="100%" />
                        </div>
                        <div class="panel-body">
                            <div style="margin-bottom: 10px;">
                                <img src="<?php echo $base_url ?>/profile/<?php echo $poster_dp_data['dp_link'] ?>" width="30px" class="shadow" style="margin: 0px !important; border: 2px solid #ffffff;" /> <b><?php echo $poster_data['alumn_firstname'] . ' ' . $poster_data['alum_middlename'] . ' ' . $poster_data['alum_surname']; ?></b>
                            </div>
                            <?php echo $events_data['event_description'] ?>
                        </div>
                        <div class="panel-footer">
                            <b>Batch Involved:</b> <?php echo $events_data['graduate_batch'] ?> | <b>0</b> Total Discussions
                        </div>
                    </div>  
                        <?php }
                    ?>
                </div>
            </div>
        </div>
        <?php include '../footer.php'; ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
