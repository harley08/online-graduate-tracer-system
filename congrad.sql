-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2017 at 06:42 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `congrad`
--

-- --------------------------------------------------------

--
-- Table structure for table `alumni_accounts`
--

CREATE TABLE `alumni_accounts` (
  `account_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `account_username` text NOT NULL,
  `account_password` text NOT NULL,
  `account_role` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumni_accounts`
--

INSERT INTO `alumni_accounts` (`account_id`, `graduate_id`, `account_username`, `account_password`, `account_role`) VALUES
(1, 1, 'Administrator', '5f4dcc3b5aa765d61d8327deb882cf99', 'Administrator'),
(4, 2, 'harley08', 'f86c4171fa47d258798b467075647092', 'Normal User'),
(5, 3, 'warren12', '81dc9bdb52d04dc20036dbd8313ed055', 'Normal User'),
(6, 4, 'migsangel', '1b82ca30c60226da90587820c0692027', 'Normal User'),
(7, 6, 'neelyah', '81dc9bdb52d04dc20036dbd8313ed055', 'Normal User'),
(8, 7, 'jasper25', '81dc9bdb52d04dc20036dbd8313ed055', 'Normal User'),
(9, 8, 'lenlen', '81dc9bdb52d04dc20036dbd8313ed055', 'Normal User'),
(10, 10, 'ram', '81dc9bdb52d04dc20036dbd8313ed055', 'Normal User'),
(11, 9, 'donna', '81dc9bdb52d04dc20036dbd8313ed055', 'Normal User');

-- --------------------------------------------------------

--
-- Table structure for table `alumni_educational_background`
--

CREATE TABLE `alumni_educational_background` (
  `education_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `school` text NOT NULL,
  `course` text NOT NULL,
  `major` text NOT NULL,
  `year_graduated` text NOT NULL,
  `education_level` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumni_educational_background`
--

INSERT INTO `alumni_educational_background` (`education_id`, `graduate_id`, `school`, `course`, `major`, `year_graduated`, `education_level`) VALUES
(3, 2, 'MinSCAT Bongabong Campus', 'Bachelor of Science in Information Technology', 'n/a', '2017-2018', 'Tertiary'),
(4, 3, 'MinSCAT Bongabong Campus', 'Bachelor of Science in Information Technology', 'n/a', '2017-2018', 'n/a'),
(5, 4, 'MinSCAT Bongabong Campus', 'BSCS', 'Computer', '2013-2014', 'BSU'),
(6, 6, 'MinSCAT Bongabong Campus', 'Bachelor of Science in Information Technology', 'n/a', '2017-2018', 'n/a'),
(7, 7, 'MinSCAT Bongabong Campus', 'Bachelor of Science in Hotel & Tourism Management', 'n/a', '2017-2018', 'n/a'),
(8, 8, 'MinSCAT Bongabong Campus', 'Bachelor of Science in Information Technology', 'Computer', '2017-2018', 'Tertiary'),
(9, 8, 'BSU', 'MSIT', 'IT', 'n/a', 'Graduate Study'),
(10, 1, 'MinSCAT Bongabong Campus', 'Bachelor of Science in Information Technology', '', '2016-2017', 'Tertiary'),
(11, 2, 'MinSCAT Bongabong Campus', 'Master of Arts in Computer Science', 'Web Development', '2020-2022', 'Graduate Study'),
(12, 10, 'MinSCAT Bongabong Campus', 'Bachelor of Science in Information Technology', 'Computer Software', '2000-2001', 'Tertiary'),
(13, 10, 'n/a', 'n/a', 'n/a', 'n/a', 'Graduate Study'),
(14, 9, 'MinSCAT Bongabong Campus', 'Bachelor of Science in Information Technology', 'Computer', '2000-2001', 'Tertiary'),
(15, 9, 'n/a', 'n/a', 'n/a', 'n/a', 'Graduate Study');

-- --------------------------------------------------------

--
-- Table structure for table `alumni_personal_info`
--

CREATE TABLE `alumni_personal_info` (
  `info_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `alum_surname` text NOT NULL,
  `alumn_firstname` text NOT NULL,
  `alum_middlename` text NOT NULL,
  `alum_birthday` date NOT NULL,
  `alum_age` int(11) NOT NULL,
  `alum_gender` text NOT NULL,
  `alum_civil_status` text NOT NULL,
  `alum_present_address` text NOT NULL,
  `alum_permanent_address` text NOT NULL,
  `alum_contact_phone` text NOT NULL,
  `alum_contact_landline` text NOT NULL,
  `alum_email` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumni_personal_info`
--

INSERT INTO `alumni_personal_info` (`info_id`, `graduate_id`, `alum_surname`, `alumn_firstname`, `alum_middlename`, `alum_birthday`, `alum_age`, `alum_gender`, `alum_civil_status`, `alum_present_address`, `alum_permanent_address`, `alum_contact_phone`, `alum_contact_landline`, `alum_email`) VALUES
(3, 2, 'Lumagui', 'Harley', 'Ferrer', '1997-07-23', 19, 'Male', 'Single', 'Labasan, Bongabong, Oriental Mindoro', 'Labasan, Bongabong, Oriental Mindoro', '+638776246162', '000-0000', 'harl.lumagui0723@gmail.com'),
(4, 3, 'Mabunga', 'Jay Warren', 'Mandac', '1998-06-29', 18, 'Male', 'Single', 'BB1, Bongabong, Oriental Mindoro', 'BB1, Bongabong, Oriental Mindoro', '+638776246162', '000-0000', 'warren.mabunga@gmail.com'),
(5, 4, 'Binay', 'Glenda', 'Pacaul', '2017-03-01', 41, 'Female', 'Maried', 'Dalapian, Labasan, Bongabong, Oriental Mindoro', 'Dalapian, Labasan, Bongabong, Oriental Mindoro', '+638776246162', '000-0000', 'gcpacaul@yahoo.com'),
(6, 6, 'Dulag', 'Hayleen', 'Fernando', '1997-07-23', 19, 'Female', 'Single', 'Labasan, Bongabong, Oriental Mindoro', 'Balatasan, Bulalacao, Oriental Mindoro', '+639197704670', '000-0000', 'neelyah@yahoo.com'),
(7, 7, 'Lumagui', 'Jasper', 'Ferrer', '2005-07-25', 12, 'Male', 'Single', 'Labasan, Bongabong, Oriental Mindoro', 'Labasan, Bongabong, Oriental Mindoro', '+638776246162', '000-0000', 'jasper@gmail.com'),
(8, 8, 'Lumagui', 'Jesselyn', 'Ferrer', '2017-03-14', 21, 'Female', 'Separated', 'Calamba Laguna', 'Labasan, Bongabong, Oriental Mindoro', '+638776246162', '000-0000', 'jesselyn@yahoo.com'),
(9, 1, 'Lumagui', 'Harley', 'Ferrer', '2017-03-08', 19, 'Male', 'Single', 'Labasan, Bongabong, Oriental Mindoro', 'Labasan, Bongabong, Oriental Mindoro', '+639776246162', '000-0000', 'lumagui.harl0723@gmail.com'),
(10, 10, 'Osorio', 'Ramuel', 'Fernandez', '1995-05-10', 23, 'Male', 'Single', 'BB2, Bongabong, Oriental Mindoro', 'BB2, Bongabong, Oriental Mindoro', '+6358955656', '000-0000', 'ram@yahoo.com'),
(11, 9, 'Lumagui', 'Maria Donna', 'Ferrer', '1990-06-07', 26, 'Male', 'Maried', 'Labasan, Bongabong, Oriental Mindoro', 'Ipil, Bongabong, Oriental Mindoro', '+639776246162', '000-0000', 'mariadonna@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `alumni_questions`
--

CREATE TABLE `alumni_questions` (
  `question_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `job_satisfy` text NOT NULL,
  `job_satisfy_reason` text NOT NULL,
  `reasons_present_job` text NOT NULL,
  `benefits_from_job` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumni_questions`
--

INSERT INTO `alumni_questions` (`question_id`, `graduate_id`, `job_satisfy`, `job_satisfy_reason`, `reasons_present_job`, `benefits_from_job`) VALUES
(3, 2, 'Yes', 'Because this job is fitted to my skills.', 'Fitted to my skills.', 'Salary infringement.'),
(4, 3, 'Yes', 'Because this job is fitted to my skills.', 'I need money.', 'To help family.'),
(5, 4, 'Yes', 'working environment', 'in line with my specialization', 'salary infringement'),
(6, 6, 'Yes', 'working environment', 'in line with my specialization', 'salary infringement'),
(7, 7, 'Yes', 'Because this job is fitted to my skills.', 'in line with my specialization', 'salary infringement'),
(8, 8, 'Yes', 'Because this job is fitted to my skills.', 'in line with my specialization', 'salary infringement'),
(9, 1, 'Yes', 'Fit to my skills.', 'Fit to my skills.', 'Salary infringement.'),
(10, 10, 'No', 'Financial.', 'n/a', 'n/a'),
(11, 9, 'No', 'n/a', 'n/', 'n/a');

-- --------------------------------------------------------

--
-- Table structure for table `batch_folders`
--

CREATE TABLE `batch_folders` (
  `batch_id` int(11) NOT NULL,
  `batch_year` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_folders`
--

INSERT INTO `batch_folders` (`batch_id`, `batch_year`) VALUES
(1, '2017-2018'),
(2, '2013-2014'),
(3, '2000-2001'),
(4, '2015-2016');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `event_title` text NOT NULL,
  `event_description` text NOT NULL,
  `event_date` date NOT NULL,
  `graduate_batch` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `graduate_id`, `event_title`, `event_description`, `event_date`, `graduate_batch`) VALUES
(12, 9, 'Run 4 a Cost', 'Description .....', '2017-03-10', '2000-2001'),
(11, 2, 'Grand Alumni Homecoming Batch 2017-2018', '.......', '2017-06-22', '2017-2018');

-- --------------------------------------------------------

--
-- Table structure for table `events_comments`
--

CREATE TABLE `events_comments` (
  `comment_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `comment_msg` text NOT NULL,
  `comment_photo_link` text,
  `comment_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events_comments`
--

INSERT INTO `events_comments` (`comment_id`, `event_id`, `graduate_id`, `comment_msg`, `comment_photo_link`, `comment_date`) VALUES
(1, 2, 2, 'hey...', NULL, '2017-03-14 00:00:00'),
(2, 2, 3, 'hehe', NULL, '2017-03-14 00:00:00'),
(3, 2, 3, 'yehey...', NULL, '2017-03-14 00:00:00'),
(4, 2, 2, 'pangit ka', NULL, '2017-03-14 00:00:00'),
(5, 2, 2, 'hey', NULL, '2017-03-14 00:00:00'),
(6, 2, 2, 'test...', NULL, '2017-03-14 00:00:00'),
(7, 2, 2, '...', NULL, '2017-03-14 00:00:00'),
(8, 2, 2, 'photo', NULL, '2017-03-14 00:00:00'),
(9, 2, 2, 'photo...', NULL, '2017-03-14 00:00:00'),
(15, 2, 2, 'sfd', 'images/events_photos/1510638_1660480530837180_795053904702073421_n.jpg', '2017-03-14 00:00:00'),
(11, 2, 2, 'photo123', '', '2017-03-14 00:00:00'),
(14, 2, 2, 'sdgfgh', '', '2017-03-14 00:00:00'),
(13, 2, 2, 'photo...', 'images/events_photos/1486757_1545447445673823_3669477407982363285_n.jpg', '2017-03-14 00:00:00'),
(16, 2, 2, 'sdfsdf', '', '2017-03-14 00:00:00'),
(17, 2, 2, 'photo...', 'images/events_photos/10622937_1545446002340634_4021861504862843258_n.jpg', '2017-03-14 00:00:00'),
(18, 2, 2, 'no photo', '', '2017-03-14 00:00:00'),
(19, 2, 2, 'carl ', 'images/events_photos/10629734_1545448442340390_8353457309012682322_n.jpg', '2017-03-14 00:00:00'),
(20, 2, 2, 'no photo', '', '2017-03-14 00:00:00'),
(21, 2, 2, 'w/ photo', 'images/events_photos/10622937_1545446002340634_4021861504862843258_n.jpg', '2017-03-14 00:00:00'),
(22, 2, 2, '', 'images/events_photos/15241_1545446139007287_3748385504557464308_n.jpg', '2017-03-14 00:00:00'),
(23, 2, 3, 'wdf', '', '2017-03-14 00:00:00'),
(24, 2, 3, '', 'images/events_photos/10676345_1545446079007293_3890063435938473302_n.jpg', '2017-03-14 00:00:00'),
(25, 2, 3, 'photo', '', '2017-03-14 00:00:00'),
(26, 2, 3, '', 'images/events_photos/1486757_1545447445673823_3669477407982363285_n.jpg', '2017-03-14 00:00:00'),
(27, 2, 3, 'xcs.c', 'images/events_photos/14516370_1183832281696634_1913795204354220619_n.jpg', '2017-03-14 00:00:00'),
(28, 2, 3, 'fdgd', '', '2017-03-15 00:00:00'),
(29, 2, 3, 'regeg', '', '2017-03-15 06:17:28'),
(30, 2, 3, 'hehehe', '', '2017-03-14 08:09:18'),
(31, 2, 3, '', 'images/events_photos/14516370_1183832281696634_1913795204354220619_n.jpg', '2017-03-14 08:11:07'),
(32, 2, 2, 'hehe', '', '2017-03-14 13:09:49'),
(33, 1, 1, 'hey', '', '2017-03-15 02:53:16'),
(34, 12, 9, 'hello ', '', '2017-03-31 07:37:12'),
(35, 12, 9, '', 'images/events_photos/1604010-grill-pattern-riveted-to-brushed-steel-background - Copy.jpg', '2017-03-31 07:37:18'),
(36, 12, 9, 'hello', 'images/events_photos/692cAbstract-vector-backgrounds-vol-42.jpg', '2017-03-31 07:37:29');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_albums`
--

CREATE TABLE `gallery_albums` (
  `album_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `album_title` text NOT NULL,
  `album_description` text NOT NULL,
  `album_date_created` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_albums`
--

INSERT INTO `gallery_albums` (`album_id`, `graduate_id`, `album_title`, `album_description`, `album_date_created`) VALUES
(1, 1, 'Alumni Batch 2016-2017 Grand Homecoming', 'Venue at the MinSCAT Bongabong Gymansium.', '2017-03-10'),
(2, 2, 'Preparation 4 Grand Homecoming Batch 2012', '-', '2017-03-10'),
(3, 1, 'Batch 2015-2016 Grand Homecoming', 'Batch 2015-2016 Grand Homecoming', '2017-03-11'),
(4, 1, 'Alumni Batch 2010-2012', 'Alumni Batch 2010-2012', '2017-03-11'),
(5, 2, 'Alumni Batch 2009-2010 Grand Homecoming @ the MBC', 'Venue at the MBC gymnasium.', '2017-03-11'),
(7, 10, 'Grand Homecoming 2000-2001', 'descr.', '2017-03-30'),
(8, 9, 'Run 4 a Cost', 'Run 4 a cost.', '2017-03-31');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_album_comments`
--

CREATE TABLE `gallery_album_comments` (
  `comment_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `comment_msg` text NOT NULL,
  `comment_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_album_comments`
--

INSERT INTO `gallery_album_comments` (`comment_id`, `album_id`, `graduate_id`, `comment_msg`, `comment_date`) VALUES
(1, 1, 2, 'hahahaha ...', '2017-03-13'),
(2, 1, 2, 'hahahaa ...', '2017-03-13'),
(3, 1, 2, 'test comment ...', '2017-03-13'),
(4, 1, 2, 'paolo ...', '2017-03-13'),
(5, 2, 2, 'comment ...', '2017-03-13'),
(6, 1, 2, 'blah blah blah ....', '2017-03-13'),
(7, 2, 2, 'testing', '2017-03-21'),
(8, 7, 10, 'hahahaha', '2017-03-30'),
(9, 8, 9, 'hello ..... ', '2017-03-31');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_photos`
--

CREATE TABLE `gallery_photos` (
  `photo_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `photo_link` text NOT NULL,
  `photo_description` text NOT NULL,
  `photo_date_uploaded` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_photos`
--

INSERT INTO `gallery_photos` (`photo_id`, `album_id`, `graduate_id`, `photo_link`, `photo_description`, `photo_date_uploaded`) VALUES
(1, 2, 2, 'images/1f97480030eb5debbebae0395e6fa696_clipart-handshake-shake-hand-clipart-png_2400-1242.png', 'Venue at the MinSCAT Bongabong Campus', '2017-03-11'),
(2, 2, 2, 'images/egBpyWr.png', 'Venue at the MinSCAT Bongabong Campus', '2017-03-11'),
(3, 2, 2, 'images/IT Day 2016 Program frony.jpg', 'Venue at the MinSCAT Bongabong Campus', '2017-03-11'),
(4, 2, 2, 'images/IMG_9258.JPG', 'my caption...', '2017-03-11'),
(5, 2, 2, 'images/IMG_9264.JPG', 'my caption...', '2017-03-11'),
(6, 2, 2, 'images/IMG_9270.JPG', 'my caption...', '2017-03-11'),
(7, 1, 1, 'images/IMG_9264.JPG', 'paolo..', '2017-03-11'),
(8, 1, 1, 'images/IMG_9265.JPG', 'paolo..', '2017-03-11'),
(9, 1, 1, 'images/IMG_9266.JPG', 'paolo..', '2017-03-11'),
(10, 3, 1, 'images/IMG_9277.JPG', '-', '2017-03-11'),
(11, 3, 1, 'images/IMG_9301.JPG', '-', '2017-03-11'),
(12, 3, 1, 'images/IMG_9307.JPG', '-', '2017-03-11'),
(13, 4, 1, 'images/IMG_9272.JPG', 'paolo...', '2017-03-11'),
(16, 2, 2, 'images/14556696_1815480675337164_7094310504982171393_o.jpg', 'testing...', '2017-03-21'),
(39, 4, 1, 'images/205-free-abstract-vector-background-colorful-stripes-perspective - Copy.png', '', '2017-03-29'),
(19, 5, 2, 'images/10629734_1545448442340390_8353457309012682322_n.jpg', 'testing..', '2017-03-21'),
(20, 5, 2, 'images/10676345_1545446079007293_3890063435938473302_n.jpg', 'testing..', '2017-03-21'),
(21, 5, 2, 'images/10629734_1545448442340390_8353457309012682322_n.jpg', 'testing..', '2017-03-21'),
(22, 6, 3, 'images/15241_1545446139007287_3748385504557464308_n.jpg', 'upload...', '2017-03-21'),
(23, 6, 3, 'images/1486757_1545447445673823_3669477407982363285_n.jpg', 'upload...', '2017-03-21'),
(24, 6, 3, 'images/1510638_1660480530837180_795053904702073421_n.jpg', 'upload...', '2017-03-21'),
(25, 2, 2, 'images/1486757_1545447445673823_3669477407982363285_n.jpg', 'upload...', '2017-03-21'),
(38, 5, 2, 'images/Tulips.jpg', '', '2017-03-22'),
(37, 2, 2, 'images/Tulips.jpg', '', '2017-03-22'),
(28, 2, 2, 'images/1486757_1545447445673823_3669477407982363285_n.jpg', '', '2017-03-21'),
(31, 2, 2, 'images/10622937_1545446002340634_4021861504862843258_n.jpg', 'test', '2017-03-21'),
(32, 2, 2, 'images/Hydrangeas.jpg', '', '2017-03-21'),
(33, 2, 2, 'images/Tulips.jpg', '', '2017-03-21'),
(34, 2, 2, 'images/Chrysanthemum.jpg', '', '2017-03-21'),
(35, 2, 2, 'images/Jellyfish.jpg', '', '2017-03-21'),
(36, 2, 2, 'images/Koala.jpg', '', '2017-03-21'),
(40, 4, 1, 'images/7f9fe6d9fb60f68c161b087a76d42e4b.png', '', '2017-03-29'),
(41, 4, 1, 'images/1604010-grill-pattern-riveted-to-brushed-steel-background - Copy.jpg', '', '2017-03-29'),
(42, 7, 10, 'images/09-824x542.jpg', 'sfs', '2017-03-30'),
(43, 8, 9, 'images/IT Day Certificate Background.jpg', 'gfhfhg', '2017-03-31'),
(44, 8, 9, 'images/Programme Background.jpg', 'gfhfhg', '2017-03-31'),
(45, 8, 9, 'images/mV0OIAS.jpg', 'gfhfhg', '2017-03-31');

-- --------------------------------------------------------

--
-- Table structure for table `graduates`
--

CREATE TABLE `graduates` (
  `graduate_id` int(11) NOT NULL,
  `graduate_surname` text NOT NULL,
  `graduate_firstname` text NOT NULL,
  `graduate_middlename` text NOT NULL,
  `graduate_course` text NOT NULL,
  `graduate_major` text NOT NULL,
  `graduate_batch` text NOT NULL,
  `graduate_contact_number` text NOT NULL,
  `graduate_email` text NOT NULL,
  `graduate_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `graduates`
--

INSERT INTO `graduates` (`graduate_id`, `graduate_surname`, `graduate_firstname`, `graduate_middlename`, `graduate_course`, `graduate_major`, `graduate_batch`, `graduate_contact_number`, `graduate_email`, `graduate_address`) VALUES
(1, 'Lumagui', 'Harley', 'Ferrer', 'Bachelor of Science in Information Technology', 'n/a', '2009-2010', '+639*********', 'admin@congrad.com', 'Barangay, Municipacity, Province'),
(2, 'Lumagui', 'Harley', 'Ferrer', 'Bachelor of Science in Information Technology', '-', '2017-2018', '+639776246162', 'harl.lumagui0723@gmail.com', 'Labasan, Bongabong, Oriental Mindoro'),
(3, 'Mabunga', 'Jay Warren', 'Mandac', 'Bachelor of Science in Information Technology', '-', '2017-2018', '+639776246162', 'warren.mabunga@gmail.com', 'BB1, Bongabong, Oriental Mindoro'),
(4, 'Binay', 'Glenda', 'Pacaul', 'Bachelor of Science in Information Technology', '-', '2013-2014', '+639776246162', 'glendabinay@yahoo.com', 'Dalapian, Labasan, Bongabong, Oriental Mindoro'),
(10, 'Osorio', 'Ramuel', 'Fernandez', 'Bachelor of Science in Information Technology', 'Computer Software', '2000-2001', '+639776246162', 'ram@yahoo.com', 'BB2, Bongabong, Oriental Mindoro'),
(6, 'Dulag', 'Hayleen', 'Fernando', 'Bachelor of Science in Hotel & Tourism Management', '-', '2017-2018', '+639776246162', 'neelyah@yahoo.com', 'Balatasan, Bongabong, Oriental Mindoro'),
(8, 'Lumagui', 'Jesselyn', 'Ferrer', 'Bachelor of Science in Information Technology', '-', '2017-2018', '+639776246162', 'jesselyn@yahoo.com', 'Labasan, Bongabong, Oriental Mindoro'),
(9, 'Lumagui', 'Maria Donna', 'Ferrer', 'Bachelor of Science in Information Technology', '-', '2017-2018', '+639776246162', 'donna@yahoo.com', 'Labasan, Bongabong, Oriental Mindoro');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `msg_id` int(11) NOT NULL,
  `msg_sender_graduate_id` int(11) NOT NULL,
  `msg_receiver_graduate_id` int(11) NOT NULL,
  `msg_content` text NOT NULL,
  `msg_date` datetime NOT NULL,
  `msg_status` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`msg_id`, `msg_sender_graduate_id`, `msg_receiver_graduate_id`, `msg_content`, `msg_date`, `msg_status`) VALUES
(1, 2, 3, 'Warren ....', '2017-03-14 00:00:00', 'read'),
(2, 2, 3, 'hey...', '2017-03-14 00:00:00', 'read'),
(3, 2, 3, 'hey...', '2017-03-14 00:00:00', 'read'),
(4, 2, 3, 'hey...', '2017-03-14 00:00:00', 'read'),
(5, 3, 2, 'bakit harl?', '2017-03-15 00:00:08', 'read'),
(6, 3, 2, 'bakit harl?', '2017-03-15 00:01:55', 'read'),
(7, 3, 2, 'testing lang war...', '2017-03-15 00:02:39', 'read'),
(8, 2, 3, 'wala lang pre', '2017-03-15 00:03:18', 'read'),
(9, 3, 2, 'hehehe', '2017-03-15 00:04:18', 'read'),
(10, 2, 3, 'tawa ka pa ehh', '2017-03-15 00:06:06', 'read'),
(11, 2, 3, 'hey?', '2017-03-15 07:04:10', 'read'),
(12, 2, 3, 'hey', '2017-03-15 07:08:07', 'read'),
(13, 2, 3, 'hey', '2017-03-15 07:08:29', 'read'),
(14, 1, 2, 'message..', '2017-03-15 07:56:44', 'read'),
(15, 2, 1, 'hahahaha', '2017-03-21 04:56:17', 'read'),
(16, 3, 2, 'harl', '2017-03-21 04:56:51', 'read'),
(17, 2, 3, 'Bakit war?', '2017-03-21 04:57:27', 'read'),
(18, 2, 3, 'fy', '2017-03-24 02:39:24', 'read'),
(19, 3, 2, 'bano', '2017-03-24 02:40:10', 'read'),
(20, 2, 3, 'hi', '2017-03-30 01:11:39', 'read'),
(21, 10, 2, 'hello. pangit ni sha.', '2017-03-30 07:38:11', 'read'),
(22, 2, 10, 'pangit ka din!!!!!!11', '2017-03-30 07:38:50', 'read'),
(23, 9, 3, 'Hi ...', '2017-03-31 07:52:03', 'read'),
(24, 3, 9, 'hello.', '2017-03-31 07:53:11', 'read');

-- --------------------------------------------------------

--
-- Table structure for table `photo_carousels`
--

CREATE TABLE `photo_carousels` (
  `carousel_id` int(11) NOT NULL,
  `carousel_user` text NOT NULL,
  `carousel_1` text NOT NULL,
  `carousel_2` text NOT NULL,
  `carousel_3` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_links`
--

CREATE TABLE `profile_links` (
  `profile_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `alumn_profile_hash_link` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_links`
--

INSERT INTO `profile_links` (`profile_id`, `graduate_id`, `alumn_profile_hash_link`) VALUES
(1, 1, '/congrad/profile/?7b7bc2512ee1fedcd76bdc68926d4f7b'),
(2, 2, '/congrad/profile/?d4761485f57ed903e001300bc558b793'),
(3, 3, '/congrad/profile/?4401c0401b1d6a209e19ce8705a4d752'),
(4, 6, '/congrad/profile/?659722e769d6a3c3fcb6520a3598e65e'),
(5, 7, '/congrad/profile/?11cd04c7fef0cce318c2150374aa2fa0'),
(6, 8, '/congrad/profile/?d0ee3e858787e9a240b4bc044c1f4f84'),
(7, 10, '/congrad/profile/?4641999a7679fcaef2df0e26d11e3c72'),
(8, 9, '/congrad/profile/?32ff0880c7c75d751d0f9ef3a816eae2');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pictures`
--

CREATE TABLE `profile_pictures` (
  `dp_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `dp_link` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_pictures`
--

INSERT INTO `profile_pictures` (`dp_id`, `graduate_id`, `dp_link`) VALUES
(1, 1, 'photos/dp/user.png'),
(4, 2, 'photos/dp/user.png'),
(5, 3, 'photos/dp/user.png'),
(6, 1, 'photos/dp/minscat logo transparent.png'),
(7, 2, 'photos/dp/14516370_1183832281696634_1913795204354220619_n.jpg'),
(8, 4, 'photos/dp/user.png'),
(9, 6, 'photos/dp/user.png'),
(10, 7, 'photos/dp/user.png'),
(11, 8, 'photos/dp/user.png'),
(12, 8, 'photos/dp/IT Society Day 2016.jpg'),
(13, 3, 'photos/dp/1486757_1545447445673823_3669477407982363285_n.jpg'),
(14, 2, 'photos/dp/Programme Background.jpg'),
(15, 3, 'photos/dp/Programme Background.jpg'),
(16, 2, 'photos/dp/14516370_1183832281696634_1913795204354220619_n.jpg'),
(17, 10, 'photos/dp/user.png'),
(18, 9, 'photos/dp/user.png');

-- --------------------------------------------------------

--
-- Table structure for table `shouts`
--

CREATE TABLE `shouts` (
  `shout_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `shout_msg` text NOT NULL,
  `shout_photo` text,
  `shout_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shouts`
--

INSERT INTO `shouts` (`shout_id`, `graduate_id`, `shout_msg`, `shout_photo`, `shout_date`) VALUES
(1, 1, 'Alumni Batch 2016-2017 we will be having a meeting for a "Grand Alumni Homecoming" this coming April. Please comment down for inquiries or pm me.', NULL, '2017-03-10 00:00:00'),
(6, 2, 'Alumni Batch 2016-2017 we will be having a meeting for a "Grand Alumni Homecoming" this coming April. Please comment down for inquiries or pm me.', NULL, '2017-03-12 00:00:00'),
(5, 2, 'Alumni Batch 2016-2017 we will be having a meeting for a "Grand Alumni Homecoming" this coming April. Please comment down for inquiries or pm me.', NULL, '2017-03-12 00:00:00'),
(7, 2, 'Alumni Batch 2016-2017 we will be having a meeting for a "Grand Alumni Homecoming" this coming April. Please comment down for inquiries or pm me.', NULL, '2017-03-12 00:00:00'),
(8, 2, 'Alumni Batch 2016-2017 we will be having a meeting for a "Grand Alumni Homecoming" this coming April. Please comment down for inquiries or pm me.', NULL, '2017-03-12 00:00:00'),
(9, 2, 'Alumni Batch 2016-2017 we will be having a meeting for a "Grand Alumni Homecoming" this coming April. Please comment down for inquiries or pm me.', NULL, '2017-03-12 00:00:00'),
(10, 2, 'Alumni Batch 2016-2017 we will be having a meeting for a "Grand Alumni Homecoming" this coming April. Please comment down for inquiries or pm me.', NULL, '2017-03-12 00:00:00'),
(11, 2, 'Alumni Batch 2016-2017 we will be having a meeting for a "Grand Alumni Homecoming" this coming April. Please comment down for inquiries or pm me.', NULL, '2017-03-12 00:00:00'),
(12, 2, 'Alumni Batch 2016-2017 we will be having a meeting for a "Grand Alumni Homecoming" this coming April. Please comment down for inquiries or pm me.', NULL, '2017-03-12 00:00:00'),
(13, 2, 'Alumni Batch 2016-2017 we will be having a meeting for a "Grand Alumni Homecoming" this coming April. Please comment down for inquiries or pm me.', NULL, '2017-03-12 00:00:00'),
(14, 2, 'test', NULL, '2017-03-15 00:00:00'),
(15, 2, 'test', NULL, '2017-03-15 00:00:00'),
(16, 2, 'test', NULL, '2017-03-15 00:00:00'),
(17, 2, 'test', NULL, '2017-03-15 00:00:00'),
(18, 2, '<div class="panel panel-default"><div class="panel-body">Harley Ferrer Lumagui uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=2&album=Preparation 4 Grand Homecoming Batch 2012">Preparation 4 Grand Homecoming Batch 2012</a>.</div></div>', NULL, '2017-03-21 00:00:00'),
(19, 2, '<div class="panel panel-default"><div class="panel-body">Harley Ferrer Lumagui uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=5&album=Alumni Batch 2009-2010 Grand Homecoming @ the MBC">Alumni Batch 2009-2010 Grand Homecoming @ the MBC</a>.</div></div>', NULL, '2017-03-21 00:00:00'),
(20, 3, '<div class="panel panel-default"><div class="panel-body">Jay Warren Mandac Mabunga uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=6&album=Album ni Warren">Album ni Warren</a>.</div></div>', NULL, '2017-03-21 00:00:00'),
(21, 2, '<div class="panel panel-default"><div class="panel-body"><b>Harley Ferrer Lumagui</b> uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=2&album=Preparation 4 Grand Homecoming Batch 2012">Preparation 4 Grand Homecoming Batch 2012</a>.</div></div>', NULL, '2017-03-21 00:00:00'),
(22, 2, '<div class="panel panel-default"><div class="panel-body"><b>Harley Ferrer Lumagui</b> uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=2&album=Preparation 4 Grand Homecoming Batch 2012">Preparation 4 Grand Homecoming Batch 2012</a>.</div></div>', NULL, '2017-03-21 00:00:00'),
(23, 2, '<div class="panel panel-default"><div class="panel-body"><b>Harley Ferrer Lumagui</b> uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=2&album=Preparation 4 Grand Homecoming Batch 2012">Preparation 4 Grand Homecoming Batch 2012</a>.</div></div>', NULL, '2017-03-21 00:00:00'),
(24, 2, '<div class="panel panel-default"><div class="panel-body"><b>Harley Ferrer Lumagui</b> uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=2&album=Preparation 4 Grand Homecoming Batch 2012">Preparation 4 Grand Homecoming Batch 2012</a>.</div></div>', NULL, '2017-03-21 00:00:00'),
(25, 2, '<div class="panel panel-default"><div class="panel-body"><b>Harley Ferrer Lumagui</b> uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=2&album=Preparation 4 Grand Homecoming Batch 2012">Preparation 4 Grand Homecoming Batch 2012</a>.</div></div>', NULL, '2017-03-21 00:00:00'),
(26, 2, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Harley Ferrer Lumagui</b> uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=2&album=Preparation 4 Grand Homecoming Batch 2012">Preparation 4 Grand Homecoming Batch 2012</a>.</div></div>', NULL, '2017-03-22 00:00:00'),
(27, 2, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Harley Ferrer Lumagui</b> uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=5&album=Alumni Batch 2009-2010 Grand Homecoming @ the MBC">Alumni Batch 2009-2010 Grand Homecoming @ the MBC</a> on March 22, 2017.</div></div>', NULL, '2017-03-22 00:00:00'),
(28, 2, 'w/ photo', 'images/shouts_photos/IMG_3881.JPG', '2017-03-24 07:33:26'),
(29, 2, 'w/out photo', NULL, '2017-03-24 07:33:49'),
(30, 2, '[angel]', NULL, '2017-03-29 01:24:57'),
(31, 2, '[angel]', NULL, '2017-03-29 01:25:08'),
(32, 2, '[angel]', NULL, '2017-03-29 01:25:25'),
(33, 2, '[angel]', NULL, '2017-03-29 01:25:41'),
(34, 2, '[angel]', NULL, '2017-03-29 01:25:43'),
(35, 2, '[angel]', NULL, '2017-03-29 01:26:18'),
(36, 2, '[angel]', NULL, '2017-03-29 01:27:10'),
(37, 2, 'wahhhhhh [angry]', NULL, '2017-03-29 01:27:19'),
(38, 2, 'wahhhhhh [angry]', NULL, '2017-03-29 01:27:33'),
(39, 2, 'hahahaha [angel]  grrrrrrr [angry]', NULL, '2017-03-29 01:33:19'),
(40, 2, 'huhuhuhu  :(', NULL, '2017-03-29 01:34:22'),
(41, 2, ':(', NULL, '2017-03-29 01:34:26'),
(42, 2, '[shy]', NULL, '2017-03-29 01:35:13'),
(43, 2, ':P', NULL, '2017-03-29 01:37:12'),
(44, 2, '>.<', NULL, '2017-03-29 01:37:19'),
(45, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:38:15'),
(46, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:44:41'),
(47, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:44:43'),
(48, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:44:47'),
(49, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:44:48'),
(50, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:44:57'),
(51, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:45:07'),
(52, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:48:40'),
(53, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:49:32'),
(54, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:50:07'),
(55, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:53:18'),
(56, 2, 'hmmm, yummy :P', NULL, '2017-03-29 01:58:17'),
(57, 2, 'haha :)', NULL, '2017-03-29 02:09:17'),
(58, 2, 'huhuhu :(', NULL, '2017-03-29 02:09:42'),
(59, 2, '->', NULL, '2017-03-29 02:14:55'),
(60, 2, '[confused]', NULL, '2017-03-29 02:16:26'),
(61, 2, ':-s', NULL, '2017-03-29 02:17:53'),
(62, 2, ':-s', NULL, '2017-03-29 02:18:10'),
(63, 2, '[biggrin]', NULL, '2017-03-29 02:25:39'),
(64, 2, '[angel] [angry] -> @ [biggrin] *^_^* :-s [cool] [cry] [dodgy] [!] <3 [huh] [lightbulb] [my] [rolleyes] :( :P [undecided] [wink]', NULL, '2017-03-29 02:28:44'),
(65, 2, 'B-)', NULL, '2017-03-29 02:30:10'),
(66, 2, 'B-) Announcement to all Alumni Batch 2016-2017, we will be having a grand alumni homecoming :) this coming April 2017. Event will be posted soon. [angel] *^_^*', 'images/shouts_photos/2307759-colorful-bright-ink-splashes-and-kids-jumping-on-blue-background.jpg', '2017-03-29 02:38:24'),
(67, 1, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Harley Ferrer Lumagui</b> uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=4&album=Alumni Batch 2010-2012">Alumni Batch 2010-2012</a> on March 29, 2017.</div></div>', NULL, '2017-03-29 03:02:43'),
(68, 2, 'lumagui.harl0723@gmail.com', NULL, '2017-03-29 05:04:17'),
(69, 2, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Harley Ferrer Lumagui</b> added new event entitled as <a href="/congrad/>"The Grand Alumni Homecoming"</a> to be conducted on March 9, 2017.</div></div>', NULL, '2017-03-09 00:00:00'),
(70, 2, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Harley Ferrer Lumagui</b> added new event entitled as <a href="/congrad/">Batch "2013-2014" the Grand Comeback 2017</a> to be conducted on March 21, 2017.</div></div>', NULL, '2017-03-21 00:00:00'),
(71, 2, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Harley Ferrer Lumagui</b> added new event entitled as <a href="/congrad/view-event.php?id=9">"The Comeback" 2018</a> to be conducted on March 15, 2017.</div></div>', NULL, '2017-03-15 00:00:00'),
(72, 2, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Harley Ferrer Lumagui</b> added new event entitled as <a href="/congrad/events/action/view-event.php?id=10">Alumni Batch 2000-2001 Grand Alumni Homecoming 2018</a> to be conducted on March 18, 2017.</div></div>', NULL, '2017-03-18 00:00:00'),
(73, 10, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Ramuel Fernandez Osorio</b> uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=7&album=Grand Homecoming 2000-2001">Grand Homecoming 2000-2001</a> on March 30, 2017.</div></div>', NULL, '2017-03-30 07:30:58'),
(74, 10, 'Alumni Batch 2000-2001 [my] we will be having an alumni grandhomecoming :). Please comment below.  [angel] @ B-)', NULL, '2017-03-30 07:35:21'),
(75, 2, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Harley Ferrer Lumagui</b> added new event entitled as <a href="/congrad/events/action/view-event.php?id=11">Grand Alumni Homecoming Batch 2017-2018</a> to be conducted on June 22, 2017.</div></div>', NULL, '2017-06-22 00:00:00'),
(76, 1, '[angry]', NULL, '2017-03-31 06:23:29'),
(77, 1, '[angry]', NULL, '2017-03-31 06:24:30'),
(78, 1, 'Alumni Batch 2000-2001  we will be having an alumni grandhomecoming . Please comment below.    ', NULL, '2017-03-31 06:25:12'),
(79, 9, 'Alumni Batch 2000-2001 we will be having an alumni grandhomecoming . Please comment below.', NULL, '2017-03-31 07:28:50'),
(80, 9, 'Alumni Batch 2000-2001 we will be having 	[biggrin] an alumni grandhomecoming . Please comment below. [angel]', NULL, '2017-03-31 07:31:46'),
(81, 9, 'Alumni batch ......', 'images/shouts_photos/692cAbstract-vector-backgrounds-vol-42.jpg', '2017-03-31 07:32:34'),
(82, 9, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Maria Donna Ferrer Lumagui</b> added new event entitled as <a href="/congrad/events/action/view-event.php?id=12">Run 4 a Cost</a> to be conducted on March 10, 2017.</div></div>', NULL, '2017-03-10 00:00:00'),
(83, 9, '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>Maria Donna Ferrer Lumagui</b> uploaded a photo to the album <a href="/congrad/gallery/photo-album/?id=8&album=Run 4 a Cost">Run 4 a Cost</a> on March 31, 2017.</div></div>', NULL, '2017-03-31 07:41:18');

-- --------------------------------------------------------

--
-- Table structure for table `shout_comments`
--

CREATE TABLE `shout_comments` (
  `comment_id` int(11) NOT NULL,
  `shout_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_photo_link` text NOT NULL,
  `comment_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shout_comments`
--

INSERT INTO `shout_comments` (`comment_id`, `shout_id`, `graduate_id`, `comment_content`, `comment_photo_link`, `comment_date`) VALUES
(3, 13, 1, 'hrhr', '', '2017-03-15 02:57:29'),
(2, 13, 1, 'hey', '', '2017-03-15 02:51:29'),
(5, 13, 1, '', 'images/shouts_photos/1510638_1660480530837180_795053904702073421_n.jpg', '2017-03-15 02:59:47'),
(6, 13, 1, 'h', '', '2017-03-15 02:59:51'),
(7, 13, 1, 'photo', 'images/shouts_photos/10622937_1545446002340634_4021861504862843258_n.jpg', '2017-03-15 02:59:59'),
(8, 13, 2, 'pa comment ako :D hahaha xD', '', '2017-03-15 03:12:36'),
(9, 17, 2, 'haha', '', '2017-03-21 04:43:26'),
(10, 20, 3, 'comment', '', '2017-03-21 07:17:46'),
(11, 27, 2, 'yown nakapag upload na sya :D', 'images/shouts_photos/Tulips.jpg', '2017-03-22 07:14:55'),
(12, 28, 2, 'comment', '', '2017-03-25 11:31:06'),
(13, 28, 2, '', 'images/shouts_photos/IMG_3883.JPG', '2017-03-25 11:31:25'),
(14, 68, 2, 'hahahaha ', 'images/shouts_photos/2ndopticalflare5.jpg', '2017-03-30 01:08:51'),
(15, 74, 10, 'hahaha', '', '2017-03-30 07:35:30'),
(16, 74, 10, '', 'images/shouts_photos/205-free-abstract-vector-background-colorful-stripes-perspective - Copy.png', '2017-03-30 07:35:34'),
(17, 74, 10, 'hello', '', '2017-03-30 07:36:00'),
(18, 81, 9, 'hello', '', '2017-03-31 07:33:08'),
(19, 81, 9, '', 'images/shouts_photos/2.abstract-vector-backgrounds.jpg', '2017-03-31 07:33:18'),
(20, 81, 9, '\r\nhi', 'images/shouts_photos/7f9fe6d9fb60f68c161b087a76d42e4b.png', '2017-03-31 07:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `shout_likes`
--

CREATE TABLE `shout_likes` (
  `like_id` int(11) NOT NULL,
  `shout_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shout_likes`
--

INSERT INTO `shout_likes` (`like_id`, `shout_id`, `graduate_id`) VALUES
(6, 67, 2),
(3, 66, 2),
(4, 65, 2),
(10, 68, 2),
(8, 68, 3),
(11, 68, 1),
(13, 68, 8),
(14, 67, 8),
(15, 72, 2),
(16, 72, 1),
(18, 74, 10),
(19, 74, 2),
(21, 74, 1),
(22, 81, 9);

-- --------------------------------------------------------

--
-- Table structure for table `work_experiences_employed`
--

CREATE TABLE `work_experiences_employed` (
  `employed_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `company_name` text NOT NULL,
  `company_address` text NOT NULL,
  `company_position` text NOT NULL,
  `employment_status` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_experiences_employed`
--

INSERT INTO `work_experiences_employed` (`employed_id`, `graduate_id`, `date_from`, `date_to`, `company_name`, `company_address`, `company_position`, `employment_status`) VALUES
(5, 2, '2017-03-15', '2017-03-01', 'Imergex', 'Sampaloc, Manila', 'Programmer', 'Contractual'),
(6, 2, '2017-03-15', '2017-03-01', 'Imergex', 'Sampaloc, Manila', 'Programmer', 'Casual'),
(7, 2, '2017-03-15', '2017-03-01', 'MinSCAT Bongabong Campus', 'Labasan, Bongabong, Oriental Mindoro', 'Web Developer', 'Permannent'),
(8, 3, '2017-03-30', '2017-03-28', 'MinSCAT Main Campus', 'Alcate, Victoria, Oriental Mindoro', 'GFX Artist', 'Casual'),
(9, 4, '2017-01-02', '2017-03-15', 'MBC', 'Dalapian, Labasan, Bongabong, Oriental Mindoro', 'Instructor', 'Permanent'),
(10, 2, '2017-03-15', '2017-03-01', 'Imergex', 'Sampaloc, Manila', 'Programmer', 'Permannent'),
(11, 10, '2017-03-07', '2017-03-15', 'Imergex', 'Calamba, Laguna', 'Manager', 'Casual');

-- --------------------------------------------------------

--
-- Table structure for table `work_experiences_self_employed`
--

CREATE TABLE `work_experiences_self_employed` (
  `self_employed` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `self_employed_business_type` text NOT NULL,
  `self_employed_date` date NOT NULL,
  `self_employed_reasons` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_experiences_self_employed`
--

INSERT INTO `work_experiences_self_employed` (`self_employed`, `graduate_id`, `self_employed_business_type`, `self_employed_date`, `self_employed_reasons`) VALUES
(1, 7, 'Printing Press', '2016-01-10', 'Have enough money to build up own business.'),
(2, 8, 'Printing Press', '2017-12-01', 'Have enough money to build up own business.'),
(3, 2, 'Business Type....', '2017-03-14', 'Reasons....'),
(4, 2, 'Blah Blah Blah', '2017-03-07', 'sfckjj');

-- --------------------------------------------------------

--
-- Table structure for table `work_experiences_unemployed`
--

CREATE TABLE `work_experiences_unemployed` (
  `unemployed_id` int(11) NOT NULL,
  `graduate_id` int(11) NOT NULL,
  `unemployed_date` date NOT NULL,
  `unemployed_reasons` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_experiences_unemployed`
--

INSERT INTO `work_experiences_unemployed` (`unemployed_id`, `graduate_id`, `unemployed_date`, `unemployed_reasons`) VALUES
(1, 6, '2017-03-01', 'Fresh graduate.'),
(2, 2, '2017-03-23', 'Reasons ......k'),
(3, 2, '2017-03-16', 'Because .....hh'),
(4, 10, '2017-03-16', 'Financial.'),
(5, 9, '2017-03-07', 'Financial support.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumni_accounts`
--
ALTER TABLE `alumni_accounts`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `alumni_educational_background`
--
ALTER TABLE `alumni_educational_background`
  ADD PRIMARY KEY (`education_id`);

--
-- Indexes for table `alumni_personal_info`
--
ALTER TABLE `alumni_personal_info`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `alumni_questions`
--
ALTER TABLE `alumni_questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `batch_folders`
--
ALTER TABLE `batch_folders`
  ADD PRIMARY KEY (`batch_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `events_comments`
--
ALTER TABLE `events_comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `gallery_albums`
--
ALTER TABLE `gallery_albums`
  ADD PRIMARY KEY (`album_id`);

--
-- Indexes for table `gallery_album_comments`
--
ALTER TABLE `gallery_album_comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `gallery_photos`
--
ALTER TABLE `gallery_photos`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `graduates`
--
ALTER TABLE `graduates`
  ADD PRIMARY KEY (`graduate_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `photo_carousels`
--
ALTER TABLE `photo_carousels`
  ADD PRIMARY KEY (`carousel_id`);

--
-- Indexes for table `profile_links`
--
ALTER TABLE `profile_links`
  ADD PRIMARY KEY (`profile_id`);

--
-- Indexes for table `profile_pictures`
--
ALTER TABLE `profile_pictures`
  ADD PRIMARY KEY (`dp_id`);

--
-- Indexes for table `shouts`
--
ALTER TABLE `shouts`
  ADD PRIMARY KEY (`shout_id`);

--
-- Indexes for table `shout_comments`
--
ALTER TABLE `shout_comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `shout_likes`
--
ALTER TABLE `shout_likes`
  ADD PRIMARY KEY (`like_id`);

--
-- Indexes for table `work_experiences_employed`
--
ALTER TABLE `work_experiences_employed`
  ADD PRIMARY KEY (`employed_id`);

--
-- Indexes for table `work_experiences_self_employed`
--
ALTER TABLE `work_experiences_self_employed`
  ADD PRIMARY KEY (`self_employed`);

--
-- Indexes for table `work_experiences_unemployed`
--
ALTER TABLE `work_experiences_unemployed`
  ADD PRIMARY KEY (`unemployed_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumni_accounts`
--
ALTER TABLE `alumni_accounts`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `alumni_educational_background`
--
ALTER TABLE `alumni_educational_background`
  MODIFY `education_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `alumni_personal_info`
--
ALTER TABLE `alumni_personal_info`
  MODIFY `info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `alumni_questions`
--
ALTER TABLE `alumni_questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `batch_folders`
--
ALTER TABLE `batch_folders`
  MODIFY `batch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `events_comments`
--
ALTER TABLE `events_comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `gallery_albums`
--
ALTER TABLE `gallery_albums`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `gallery_album_comments`
--
ALTER TABLE `gallery_album_comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gallery_photos`
--
ALTER TABLE `gallery_photos`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `graduates`
--
ALTER TABLE `graduates`
  MODIFY `graduate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `photo_carousels`
--
ALTER TABLE `photo_carousels`
  MODIFY `carousel_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile_links`
--
ALTER TABLE `profile_links`
  MODIFY `profile_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `profile_pictures`
--
ALTER TABLE `profile_pictures`
  MODIFY `dp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `shouts`
--
ALTER TABLE `shouts`
  MODIFY `shout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `shout_comments`
--
ALTER TABLE `shout_comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `shout_likes`
--
ALTER TABLE `shout_likes`
  MODIFY `like_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `work_experiences_employed`
--
ALTER TABLE `work_experiences_employed`
  MODIFY `employed_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `work_experiences_self_employed`
--
ALTER TABLE `work_experiences_self_employed`
  MODIFY `self_employed` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `work_experiences_unemployed`
--
ALTER TABLE `work_experiences_unemployed`
  MODIFY `unemployed_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
