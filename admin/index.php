<?php 
    include '../config.php';
    if ($not_logged) {
        header("Location: ../login/?alert=not_logged");
    } else if (!$logged_admin) {
        header("Location: ../");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Settings</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />
    </head>
    <body>
        <?php include '../header.php'; ?>
        <?php include '../footer.php'; ?>
        <script>window.jQuery || document.write('<script src="../assets/js/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/js/popover.js" type="text/javascript"></script>
    </body>
</html>
