<div class="panel panel-primary shadow" style="border-radius: 0px">
    <div class="panel-heading">
        <h5 style="margin: 5px;" style="margin: 0px;"><span class="glyphicon glyphicon-calendar"></span> Events</h5>
    </div>
    <div class="panel-body">
        <?php
            $events = $mysqli->query("SELECT * FROM events ORDER BY event_date ASC");
            $count_events = $events->num_rows;
            while ($event_data = $events->fetch_assoc()) { ?>
        <div style="padding: 10px; border-bottom: 1px solid #efeeee;">
            <a href="<?php echo $base_url ?>/events/action/view-event.php?id=<?php echo $event_data['event_id'] ?>&<?php echo $event_data['event_title'] ?>">
                <span class="glyphicon glyphicon-calendar"></span> <?php echo date("F j, Y", strtotime($event_data['event_date'])); ?> <a href="<?php echo $base_url ?>/events/action/view-event.php?id=<?php echo $event_data['event_id'] ?>&<?php echo $event_data['event_title'] ?>"></a>
            </a>
        </div>
            <?php }
            if ($count_events == null) { ?>
        <div style="padding: 10px; border-bottom: 1px solid #efeeee;">
            <center>no events posted yet</center>
        </div>
            <?php }
        ?>
    </div>
</div>