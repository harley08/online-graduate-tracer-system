<?php
    $mysqli = new mysqli('localhost', 'root', '', 'congrad') or die(mysqli_error());
    session_start();
    
    /* System link base routes */
    $_SESSION['base_url'] = "/congrad";
    $base_url = $_SESSION['base_url'];
    
    /* Site content routes */
    $site_header = $base_url. 'header_includes/includes_header.php';
    $site_top = $base_url. 'header_includes/includes_top.php';
    
    /* Site images routes */
    $site_images = $base_url. 'images/';
    
    /* -- Authentication functions -- */
    $logged = isset($_SESSION['congrad_user']);
    $not_logged = !isset($_SESSION['congrad_user']);
    
    if ($logged) {
    $logged_username = $_SESSION['congrad_user'];
        
    $get_graduate_id = $mysqli->query("SELECT * FROM alumni_accounts WHERE account_username = '$logged_username'");
    $fetch_graduate_id = $get_graduate_id->fetch_assoc();
    $user_graduate_id = $fetch_graduate_id['graduate_id'];
        
     /* Get the logged users full name */
    $query_fullname = $mysqli->query("SELECT * FROM graduates WHERE graduate_id = $user_graduate_id");
    $fetch_fullname = $query_fullname->fetch_assoc();
    $logged_surname = $fetch_fullname['graduate_surname'];
    $logged_firstname = $fetch_fullname['graduate_firstname'];
    $logged_middlename = $fetch_fullname['graduate_middlename'];
    $logged_fullname = $logged_firstname . " " . $logged_middlename . " " . $logged_surname;
    
    $check_if_admin = $mysqli->query("SELECT * FROM alumni_accounts WHERE account_username = '$logged_username' AND account_role = 'Administrator'");
    $countAdmin = $check_if_admin->num_rows;
    $logged_admin = $countAdmin == 1;
    
    $get_profile_link1 = $mysqli->query("SELECT * FROM alumni_accounts WHERE account_username = '$logged_username'");
    $alumn_id1 = $get_profile_link1->fetch_assoc();
    $alumni_id_1 = $alumn_id1['graduate_id'];
    
    $get_profile_link2 = $mysqli->query("SELECT * FROM profile_links WHERE graduate_id = $alumni_id_1");
    $get_profile_hash_link = $get_profile_link2->fetch_assoc();
    $user_profile_link = $get_profile_hash_link['alumn_profile_hash_link'];
    
    $get_id = $mysqli->query("SELECT * FROM alumni_accounts WHERE account_username = '$logged_username'");
    $get_rec_id = $get_id->fetch_assoc();
    $id_rec = $get_rec_id['graduate_id'];
    
     /* check if the user already has a profile link*/
    $check_if_already_plink = $mysqli->query("SELECT * FROM profile_links WHERE graduate_id = $user_graduate_id");
    $check_if_already_plink_count = $check_if_already_plink->num_rows;
    $no_prof_link = $check_if_already_plink_count == null;
    
    /* Get the user profile picture link */
    $query_dp_link = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = '$user_graduate_id' ORDER BY dp_id DESC");
    $get_dp_link = $query_dp_link->fetch_assoc();
    $fetch_dp_link = $get_dp_link['dp_link'];
    $user_dp_link = $fetch_dp_link;
    
    /* Count user unread messages */
    $unread_msgs = $mysqli->query("SELECT * FROM messages WHERE msg_status = 'unread' AND msg_receiver_graduate_id = $user_graduate_id");
    $count_new_messages = $unread_msgs->num_rows;
    }
    
    function show_fullname($get_id) {
        $mysqli = new mysqli('localhost', 'root', '', 'congrad') or die(mysqli_error());
        $fullname = $mysqli->query("SELECT * FROM graduates WHERE graduate_id = $get_id");
        $data_fullname = $fullname->fetch_assoc();
        echo $data_fullname['graduate_firstname'] . ' ' . $data_fullname['graduate_middlename'] . ' ' . $data_fullname['graduate_surname'];
    }
    
    function show_photo_profile($id, $img_width) {
        $mysqli = new mysqli('localhost', 'root', '', 'congrad') or die(mysqli_error());
        $base_url = $_SESSION['base_url'];
        $query_img_link = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = $id ORDER BY dp_id DESC");
        $img_link = $query_img_link->fetch_assoc();
        $profile_img_link = "photos/dp/user.png";
        if ($img_link == null) {
            $profile_img_link = "photos/dp/user.png";
        } else {
            $profile_img_link = $img_link['dp_link'];
        }
        echo '<img src="'.$base_url.'/profile/'.$profile_img_link.'" width="'.$img_width.'" style="border: 2px solid #ffffff" class="shadow" />';
    }
    
    function get_profile_link($id) {
        $mysqli = new mysqli('localhost', 'root', '', 'congrad') or die(mysqli_error());
        $get_profile_link = $mysqli->query("SELECT * FROM profile_links WHERE graduate_id = $id");
        $profile_link_data = $get_profile_link->fetch_assoc();
        echo $profile_link_data['alumn_profile_hash_link'];
    }
    
    function convert_smiley($statement) {
        $_SESSION['base_url'] = "/congrad";
        $base_url = $_SESSION['base_url'];
        $characters = array("[angel]", "[angry]", "->", "@", "[biggrin]", "*^_^*", ":-s", "B-)", "[cry]", "[dodgy]", "[!]", "<3", "[huh]", "[lightbulb]", "[my]", "[rolleyes]", ":(", "[shy]", "[sick]", "[sleepy]", ":)", ":P", "[undecided]", "[wink]");
        $smilies = array('<img src="'.$base_url.'/images/smilies/angel.png" />', '<img src="'.$base_url.'/images/smilies/angry.png" />', '<img src="'.$base_url.'/images/smilies/arrow.png" />', '<img src="'.$base_url.'/images/smilies/at.png" />', '<img src="'.$base_url.'/images/smilies/biggrin.png" />', '<img src="'.$base_url.'/images/smilies/blush.png" />', '<img src="'.$base_url.'/images/smilies/confused.png" />', '<img src="'.$base_url.'/images/smilies/cool.png" />', '<img src="'.$base_url.'/images/smilies/cry.png" />', '<img src="'.$base_url.'/images/smilies/dodgy.png" />', '<img src="'.$base_url.'/images/smilies/exclamation.png" />', '<img src="'.$base_url.'/images/smilies/heart.png" />', '<img src="'.$base_url.'/images/smilies/huh.png" />', '<img src="'.$base_url.'/images/smilies/lightbulb.png" />', '<img src="'.$base_url.'/images/smilies/my.png" />', '<img src="'.$base_url.'/images/smilies/rolleyes.png" />', '<img src="'.$base_url.'/images/smilies/sad.png" />', '<img src="'.$base_url.'/images/smilies/shy.png" />', '<img src="'.$base_url.'/images/smilies/sick.png" />', '<img src="'.$base_url.'/images/smilies/sleepy.png" />', '<img src="'.$base_url.'/images/smilies/smile.png" />', '<img src="'.$base_url.'/images/smilies/tongue.png" />', '<img src="'.$base_url.'/images/smilies/undecided.png" />', '<img src="'.$base_url.'/images/smilies/wink.png" />');
        $covert = str_replace($characters, $smilies, $statement);
        echo $new_statement = $covert;
    }
?>