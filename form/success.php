<?php
    include '../config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Success</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />
    </head>
    <body>
        <div class="container">
            <div id="info">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5 class="panel-title">Successfully Submitted!</h5>
                    </div>
                    <div class="panel-body">
                        <p>
                            Thank you for your cooperation. You can now access the other site feature. God Bless.
                        </p>
                    </div>
                    <div class="panel-footer">
                        <a href="../login/index.php" class="btn btn-default">Login Now</a>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
