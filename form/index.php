<?php include '../config.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Alumni Form</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />        
        <script>
            $(function() { $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }); });
        </script>
        <style>
            table {
                margin-top: 20px;
                margin-bottom: 20px;
            }
            td {
                border-top: 0px solid #c0c0c0 !important;
            }
            .harl-form {
                border: 0px solid #ffffff !important;
                border-bottom: 2px solid #69b744 !important;
                border-radius: 0px !important;
                width: 200px !important;
                display: inline !important;
                padding: 10px;
            }
            .harl-form:focus {
            border-color: #66afe9;
            outline: 0;
            -webkit-box-shadow: inset 0 0px 0px rgba(0,0,0,.075), 0 0 0px rgba(102, 175, 233, .6);
            box-shadow: inset 0 0px 0px rgba(0,0,0,.075), 0 0 0px rgba(102, 175, 233, .6);
            }
            .harl-form::-moz-placeholder {
            color: #999;
            opacity: 1;
            }
            .harl-form:-ms-input-placeholder {
            color: #999;
            }
            .harl-form::-webkit-input-placeholder {
            color: #999;
            }
            .harl-form::-ms-expand {
            background-color: transparent;
            border: 0;
            }
            input[type='date'] {
                width: 160px !important;
            }
            input[name='age'] {
                width: 200px;
            }
            input[name='present_address'], input[name='permanent_address'] {
                width: 600px !important;
            }
            input[type='email'] {
                width: 500px !important;
            }
            input[name='tertiary_school'] {
                width: 500px !important;
            }
            input[name='tertiary_course'] {
                width: 500px !important;
            }
            input[name='tertiary_major'] {
                width: 500px !important;
            }
        </style>
    </head>
    <body>
        <?php include '../header.php'; ?>
        <div class="container" style="padding: 0px; margin-top: 15px;">
                    <div class="panel panel-default shadow">
                        <div class="panel-heading">
                            <h5 class="panel-title"><span class="glyphicon glyphicon-file"></span> Alumni Form</h5>
                        </div>                        
                        <form role="form" action="submit.php" method="post">
                            <div class="panel-body">
                                <?php
                                    if (isset($_GET['error'])) {
                                        if ($_GET['error'] == '2') { ?>
                                <div id="err1">
                                    <div class="alert alert-warning">
                                        <b>Error:</b> You are not a graduate MinSCAT Bongabong Campus. Please inquire to the guidance counselor.
                                    </div>
                                </div>
                                        <?php }
                                        if ($_GET['error'] == '1') { ?>
                                <div id="err1">
                                    <div class="alert alert-warning">
                                        <b>Error:</b> The account username has already exists!
                                    </div>
                                </div>
                                        <?php }
                                    }
                                ?>
                                <h4 style="margin-bottom: 20px;"><span class="glyphicon glyphicon-info-sign"></span> ACCOUNT INFORMATION</h4>
                                <div class="form-group">
                                    <label>Username:</label><br>
                                    <input type="text" name="username" placeholder="type username here..." class="harl-form" required />
                                </div>
                                <div class="form-group">
                                    <label>Password:</label><br>
                                    <input type="password" required name="password" placeholder="type password here..." class="harl-form" />
                                </div>
                                <h4 style="margin-bottom: 20px;"><span class="glyphicon glyphicon-info-sign"></span> PERSONAL INFORMATION</h4>
                                <div class="form-group">
                                    <label>Name:</label><br>
                                    <input style="margin-bottom: 5px;" required class="harl-form" type="text" name="surname" placeholder="Surname" />
                                    <input style="margin-bottom: 5px;" required class="harl-form" type="text" name="firstname" placeholder="First Name" />
                                    <input type="text" name="middlename" class="harl-form" placeholder="Middle Name" />
                                </div>
                                <div class="form-group">
                                    <label>Date of Birth:</label><br>
                                    <input type="date" required class="harl-form datepicker" name="birthday" />
                                </div>
                                <div class="form-group">
                                    <label>Age:</label><br>
                                    <input type="number" required name="age" class="harl-form" placeholder="type your age here..." name="age" />
                                </div>
                                <div class="form-group">
                                    <label>Gender:</label><br>
                                    <label class="checkbox-inline">
                                        <input type="radio" required name="gender" value="Male" /> Male
                                    </label>
                                    <label class="checkbox-inline">
                                    <input type="radio" required name="gender" value="Female" /> Female
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Civil Status:</label><br>
                                    <select class="harl-form" required name="civil_status">
                                        <option value="">-- Select --</option>
                                        <option value="Single">Single</option>
                                        <option value="Maried">Maried</option>
                                        <option value="Separated">Separated</option>
                                        <option value="Single Parent">Single Parent</option>
                                        <option value="Window or Widower">Window or Widower</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Present Address:</label><br>
                                    <input required class="harl-form" type="text" name="present_address" placeholder="type your present address here..." />
                                </div>
                                <div class="form-group">
                                    <label>Permanent Address:</label><br>
                                    <input class="harl-form" type="text" name="permanent_address" placeholder="type your permanent address here..." required />
                                </div>
                                <div class="form-group">
                                    <label>Contact Nos.</label><br>
                                    <input type="text" name="cellphone_number" class="harl-form" placeholder="+6390000000000" required />
                                    <input type="text" name="landline_number" class="harl-form" placeholder="000-0000" />
                                </div>
                                <div class="form-group">
                                    <label>Email Address:</label><br>
                                    <input type="email" name="email" class="harl-form" placeholder="your_email@email.com" />
                                </div>
                                <h4 style="margin-bottom: 20px; margin-top: 25px;"><span class="glyphicon glyphicon-info-sign"></span> EDUCATIONAL BACKGROUND</h4>
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                    <span class="glyphicon glyphicon-education"></span> Tertiary
                                            </h4>
                                        </div>
                                        <div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label>Name of School:</label><br>
                                                    <input type="text" name="tertiary_school" class="harl-form" placeholder="type school name here..." required />
                                                </div>
                                                <div class="form-group">
                                                    <label>Degree/Couse:</label><br>
                                                    <input type="text" name="tertiary_course" class="harl-form" placeholder="type degreee/course here..." required />
                                                </div>
                                                <div class="form-group">
                                                    <label>Major/Field of Specialization</label><br>
                                                    <input type="text" name="tertiary_major" class="harl-form" placeholder="type major here..." />
                                                </div>
                                                <div class="form-group">
                                                    <label>Year Graduated:</label><br>
                                                    <select class="harl-form" name="tertiary_year_graduated" required>
                                                        <option value="">-- Select Batch --</option>
                                                        <?php
                                                            $batch_folders = $mysqli->query("SELECT * FROM batch_folders");
                                                            $count_batch = $batch_folders->num_rows;
                                                            while ($batch_data = $batch_folders->fetch_assoc()) {                                                                
                                                                $batch_year = $batch_data['batch_year']; ?>
                                                        <option value="<?php echo $batch_year ?>"><?php echo $batch_year ?></option>
                                                            <?php }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <span class="glyphicon glyphicon-education"></span> Graduate Studies
                                            </h4>
                                        </div>
                                        <div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label>Name of school:</label><br>
                                                    <input type="text" name="grad_stud_school" class="harl-form" placeholder="type school name here..." />
                                                </div>
                                                <div class="form-group">
                                                    <label>Degree/Couse:</label><br>
                                                    <input type="text" name="grad_stud_course" class="harl-form" placeholder="type degreee/course here..." />
                                                </div>
                                                <div class="form-group">
                                                    <label>Major/Field of Specialization</label><br>
                                                    <input type="text" name="grad_stud_major" class="harl-form" placeholder="type major here..." />
                                                </div>
                                                <div class="form-group">
                                                    <label>Year Graduated:</label><br>
                                                    <input type="text" name="grad_stud_year_graduated" placeholder="2002-2001" class="harl-form" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h4 style="margin-bottom: 20px; margin-top: 25px;"><span class="glyphicon glyphicon-info-sign"></span> WORK EXPERIENCE</h4>
                                <b><i>Please check the appropriate box according to your type of employment</i></b><br><br>
                                    <input type="checkbox" name="chckbx_employed" /> Employed<br>
                                    <b><i><u>History of Employment</u></i></b> <i>(Include private employment. Start from most recent work experience.)</i>
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>Inclusive Dates of Employment</td>
                                                <td>Institution/Company Name <i>(Please write in full)</i></td>
                                                <td>Institution/Company Address</td>
                                                <td>Position Title</td>
                                                <td>Employment Status</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    From: <input name="employed_permanent_date_from" type="date" class="harl-form" style="border-bottom: 0px solid #ffffff !important;" /><br>
                                                    To: <input name="employed_permanent_date_to" type="date" class="harl-form" style="border-bottom: 0px solid #ffffff !important;" />
                                                </td>
                                                <td>
                                                    <input name="employed_permanent_company_name" style="width: 400px !important; border-bottom: 0px solid #ffffff !important;" type="text" class="harl-form" placeholder="type here..." />
                                                </td>
                                                <td>
                                                    <input name="employed_permanent_company_address" type="text" class="harl-form" placeholder="type here..." style="border-bottom: 0px solid #ffffff !important;" />
                                                </td>
                                                <td>
                                                    <input name="employed_permanent_company_position" type="text" class="harl-form" placeholder="type here..." style="width: 100px !important; border-bottom: 0px solid #ffffff !important;" />
                                                </td>
                                                <td>
                                                    Permanent <input name="employed_permanent_chckbx" type="checkbox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    From: <input name="employed_contractual_date_from" type="date" class="harl-form" style="border-bottom: 0px solid #ffffff !important;" /><br>
                                                    To: <input name="employed_contractual_date_to" type="date" class="harl-form" style="border-bottom: 0px solid #ffffff !important;" />
                                                </td>
                                                <td>
                                                    <input name="employed_contractual_company_name" style="width: 400px !important; border-bottom: 0px solid #ffffff !important;" type="text" class="harl-form" placeholder="type here..." />
                                                </td>
                                                <td>
                                                    <input name="employed_contractual_company_address" type="text" class="harl-form" style="border-bottom: 0px solid #ffffff !important;" placeholder="type here..." />
                                                </td>
                                                <td>
                                                    <input name="employed_contractual_company_position" type="text" class="harl-form" placeholder="type here..." style="width: 100px !important; border-bottom:  0px solid #ffffff !important;" />
                                                </td>
                                                <td>
                                                    Contractual <input name="employed_contractual_chckbx" type="checkbox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    From: <input name="employed_casual_date_from" type="date" class="harl-form" style="border-bottom: 0px solid #ffffff !important;" /><br>
                                                    To: <input name="employed_casual_date_to" type="date" class="harl-form" style="border-bottom: 0px solid #ffffff !important;" />
                                                </td>
                                                <td>
                                                    <input name="employed_casual_company_name" style="width: 400px !important; border-bottom: 0px solid #ffffff !important;" type="text" class="harl-form" placeholder="type here..." />
                                                </td>
                                                <td>
                                                    <input name="employed_casual_company_address" type="text" class="harl-form" placeholder="type here..." style="border-bottom: 0px solid #ffffff !important;" />
                                                </td>
                                                <td>
                                                    <input name="employed_casual_company_position" type="text" class="harl-form" placeholder="type here..." style="width: 100px !important; border-bottom: 0px solid #ffffff !important;" />
                                                </td>
                                                <td>
                                                    Casual <input name="employed_casual_chckbx" type="checkbox" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <input name="self_employed_chckbx" type="checkbox" /> Self Employed<br>
                                    Please specify the type of business: <input name="self_employed_business_type" type="text" class="harl-form" placeholder="type here...." /><br>
                                    Since when? <input name="self_employed_date" type="date" class="harl-form" /><br>
                                    Reason/s <input name="self_employed_reasons" type="text" placeholder="type here...." class="harl-form" style="width: 500px !important" /><br><br><br>
                                    <input name="unemployed_chckbx" type="checkbox" /> Unemployed<br>
                                    Since when? <input name="unemployed_date" type="date" class="harl-form" /><br>
                                    Reason/s <input name="unemployed_reasons" type="text" class="harl-form" placeholder="type here...." style="width: 500px !important" /><br><br><br>
                                    <b>Please answer the following questions briefly:</b><br>
                                    <ol style="margin-top: 5px;">
                                        <li>
                                            Are you satisfied with your job right now? <input name="job_satisfy" type="checkbox" value="Yes" /> Yes <input name="job_satisfy" value="No" type="checkbox" /> No<br>
                                            State your reasons<br>
                                            <input type="text" name="job_satisfy_reasons" placeholder="type here...." class="harl-form" style="width: 700px !important;" required />
                                        </li>
                                        <li style="margin-top: 20px;">
                                            Kindly state your reason/s of accepting your present job<br>
                                            <input name="reasons_why_accept_present_job" type="text" placeholder="type here...." class="harl-form" style="width: 700px !important;" required />
                                        </li>
                                        <li style="margin-top: 20px;">
                                            What are the benefits you derived from your job? Please specify.<br>
                                            <input name="benefits_from_job" placeholder="type here...." type="text" class="harl-form" style="width: 700px !important;" required />
                                        </li>
                                    </ol>
                            </div>
                            <div class="panel-footer" align="center">
                                <div class="btn-group">
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
            </div>
        </div>
        <?php include '../footer.php'; ?>
        <script src="../assets/js/collapse.js" type="text/javascript"></script>
        <script src="../assets/js/transition.js" type="text/javascript"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
