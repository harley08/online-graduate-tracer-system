<?php
    include '../config.php';
    if (isset($_POST['username'])) {
        
        /* Account information */
        $username = $_POST['username'];
        $password = $_POST['password'];
        
        /* Personal information */
        $surname = $_POST['surname'];
        $firstname = $_POST['firstname'];
        $middlename = $_POST['middlename'];
        $birthday = $_POST['birthday'];
        $age = $_POST['age'];
        $gender = $_POST['gender'];
        $civil_status = $_POST['civil_status'];
        $present_address = $_POST['present_address'];
        $permanent_address = $_POST['permanent_address'];
        $cellphone_number = $_POST['cellphone_number'];
        
        $landline_number = $_POST['landline_number'];
        if ($landline_number == null) {
            $landline_number = "000-0000";
        } else if ($landline_number != null) {
            $landline_number = $_POST['landline_number'];
        }
        
        $email = $_POST['email'];
        if ($email == null) {
            $email = "your_email@email.com";
        } else if ($email != null) {
            $email = $_POST['email'];
        }
        
        /* Educational background */
        $tertiary_school = $_POST['tertiary_school'];
        $tertiary_course = $_POST['tertiary_course'];
        
        $tertiary_major = $_POST['tertiary_major'];
        if ($tertiary_major == null) {
            $tertiary_major = "n/a";
        } else if ($tertiary_major != null) {
			$tertiary_major = $_POST['tertiary_major'];
		}
        
        $tertiary_year_graduated = $_POST['tertiary_year_graduated'];
        
        $grad_stud_school = $_POST['grad_stud_school'];
        if ($grad_stud_school == null) {
            $grad_stud_school = "n/a";
        } else if ($grad_stud_school != null) {
            $grad_stud_school = $_POST['grad_stud_school'];
        }
        
        $grad_stud_course = $_POST['grad_stud_course'];
        if ($grad_stud_course == null) {
            $grad_stud_course = "n/a";
        } else if ($grad_stud_course != null) {
            $grad_stud_course = $_POST['grad_stud_course'];
        }
        
        $grad_stud_major = $_POST['grad_stud_major'];
        if ($grad_stud_major == null) {
            $grad_stud_major = "n/a";
        } else if ($grad_stud_major != null) {
            $grad_stud_major = $_POST['grad_stud_major'];
        }	
	
        $grad_stud_year_graduated = $_POST['grad_stud_year_graduated'];
	if ($grad_stud_year_graduated == null) {
            $grad_stud_year_graduated = "n/a";
	} else if ($grad_stud_year_graduated != null) {
            $grad_stud_year_graduated = $_POST['grad_stud_year_graduated'];
	}
        
        /* Questions */
        $job_satisfy = $_POST['job_satisfy'];
        $job_satisfy_reasons = $_POST['job_satisfy_reasons'];
        $reasons_why_accept_present_job = $_POST['reasons_why_accept_present_job'];
        $benefits_from_job = $_POST['benefits_from_job'];
        
        /* Check username if existing */
        $check_username = $mysqli->query("SELECT * FROM alumni_accounts WHERE account_username = '$username'");
        $count_checkUsername = $check_username->num_rows;
        if ($count_checkUsername >= 1) {
            header("Location: index.php?error=1#err1");
        } else if ($count_checkUsername == null) {
            /* Check graduate name exists */
            $check_graduateName = $mysqli->query("SELECT * FROM graduates WHERE graduate_surname = '$surname' AND graduate_firstname = '$firstname' AND graduate_middlename = '$middlename'");
            $count_checkGraduateName = $check_graduateName->num_rows;
            if ($count_checkGraduateName == null) {
                header("Location: index.php?error=2#err2");
            } else if ($count_checkGraduateName != null) {
                /* Get the graduate id */
                $get_gradID = $check_graduateName->fetch_assoc();
                $graduate_id = $get_gradID['graduate_id'];
                
                /* Insert infos */
                
                /* Submit account information */
                $submit_accntInfo = $mysqli->query("INSERT INTO alumni_accounts (graduate_id, account_username, account_password, account_role) VALUES ($graduate_id, '$username', '".md5($password)."', 'Normal User')");
                if (!$submit_accntInfo) {
                    $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                    $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                    $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                    $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                    echo "<b>Error:</b> In account information." . $mysqli->error;
                }
                    
                /* Submit personal information */
                $submit_personalInfo = $mysqli->query("INSERT INTO alumni_personal_info (graduate_id, alum_surname, alumn_firstname, alum_middlename, alum_birthday, alum_age, alum_gender, alum_civil_status, alum_present_address, alum_permanent_address, alum_contact_phone, alum_contact_landline, alum_email) VALUES ($graduate_id, '$surname', '$firstname', '$middlename', '$birthday', $age, '$gender', '$civil_status', '$present_address', '$permanent_address', '$cellphone_number', '$landline_number', '$email')");
                if (!$submit_personalInfo) {
                    $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                    $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                    $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                    $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                    echo "<b>Error:</b> In personal information. " . $mysqli->error;
                }
                
                /* Submit educational background */
                
                /* submit tertiary > educational background */
                if (isset($_POST['tertiary_school'])) {
                    $submit_tertiary = $mysqli->query("INSERT INTO alumni_educational_background (graduate_id, school, course, major, year_graduated, education_level) VALUES ($graduate_id, '$tertiary_school', '$tertiary_course', '$tertiary_major', '$tertiary_year_graduated', 'Tertiary')");
                    if (!$submit_tertiary) {
                        $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                        $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                        $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                        $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                        echo "<b>Error:</b> In submitting your tertiary information. " . $mysqli->error;
                    }
                }
                if (isset($_POST['grad_stud_school'])) {
                    $submit_graduate_study = $mysqli->query("INSERT INTO alumni_educational_background (graduate_id, school, course, major, year_graduated, education_level) VALUES ($graduate_id, '$grad_stud_school', '$grad_stud_course', '$grad_stud_major', '$grad_stud_year_graduated', 'Graduate Study')");
                    if (!$submit_graduate_study) {
                        $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                        $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                        $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                        $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                        echo "<b>Error:</b> In submitting your graduate study information. " . $mysqli->error;
                    }
                }
                
                /* Submit Work Experiences */
                
                /* if employed */
                if (isset($_POST['chckbx_employed'])) {
                    if (isset($_POST['employed_permanent_chckbx'])) {
                        $employed_permanent_date_from = $_POST['employed_permanent_date_from'];
                        $employed_permanent_date_to = $_POST['employed_permanent_date_to'];
                        $employed_permanent_company_name = $_POST['employed_permanent_company_name'];
                        $employed_permanent_company_address = $_POST['employed_permanent_company_address'];
                        $employed_permanent_company_position = $_POST['employed_permanent_company_position'];
                        $submit_permanent = $mysqli->query("INSERT INTO work_experiences_employed (graduate_id, date_from, date_to, company_name, company_address, company_position, employment_status) VALUES ($graduate_id, '$employed_permanent_date_from', '$employed_permanent_date_to', '$employed_permanent_company_name', '$employed_permanent_company_address', '$employed_permanent_company_position', 'Permanent')");
                        if (!$submit_permanent) {
                            $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                            $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                            $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                            $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                            $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                            $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                            $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                            echo '<b>Error:</b> In the employed info. PERMANENT. ' . $mysqli->error;
                        }
                    }
                    if (isset($_POST['employed_contractual_chckbx'])) {
                        $employed_contractual_date_from = $_POST['employed_contractual_date_from'];
                        $employed_contractual_date_to = $_POST['employed_contractual_date_to'];
                        $employed_contractual_company_name = $_POST['employed_contractual_company_name'];
                        $employed_contractual_company_address =  $_POST['employed_contractual_company_address'];
                        $employed_contractual_company_position = $_POST['employed_contractual_company_position'];
                        $submit_contractual = $mysqli->query("INSERT INTO work_experiences_employed (graduate_id, date_from, date_to, company_name, company_address, company_position, employment_status) VALUES ($graduate_id, '$employed_contractual_date_from', '$employed_contractual_date_to', '$employed_contractual_company_name', '$employed_contractual_company_address', '$employed_contractual_company_position', 'Contractual')");
                        if (!$submit_contractual) {
                            $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                            $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                            $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                            $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                            $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                            $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                            $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                            echo '<b>Error:</b> In the employed info. CONTRACTUAL. ' . $mysqli->error;
                        }
                    }
                    if (isset($_POST['employed_casual_chckbx'])) {
                        $employed_casual_date_from = $_POST['employed_casual_date_from'];
                        $employed_casual_date_to = $_POST['employed_casual_date_to'];
                        $employed_casual_company_name = $_POST['employed_casual_company_name'];
                        $employed_casual_company_address = $_POST['employed_casual_company_address'];
                        $employed_casual_company_position = $_POST['employed_casual_company_position'];
                        $submit_casual = $mysqli->query("INSERT INTO work_experiences_employed (graduate_id, date_from, date_to, company_name, company_address, company_position, employment_status) VALUES ($graduate_id, '$employed_casual_date_from', '$employed_casual_date_to', '$employed_casual_company_name', '$employed_casual_company_address', '$employed_casual_company_position', 'Casual')");
                        if (!$submit_casual) {
                            $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                            $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                            $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                            $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                            $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                            $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                            $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                            echo '<b>Error:</b> In the employed info. CASUAL. ' . $mysqli->error;
                        }
                }                
                }
                /* If self employed */
                if (isset($_POST['self_employed_chckbx'])) {
                    $submit_self_Employed = $mysqli->query("INSERT INTO work_experiences_self_employed (graduate_id, self_employed_business_type, self_employed_date, self_employed_reasons) VALUES ($graduate_id, '".$_POST['self_employed_business_type']."', '".$_POST['self_employed_date']."', '".$_POST['self_employed_reasons']."')");
                    if (!$submit_self_Employed) {
                        $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                        $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                        $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                        $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                        echo "<b>Error:</b> In self employed info. " . $mysqli->error;
                    }
                }
                
                /* If unemployed */
                if (isset($_POST['unemployed_chckbx'])) {
                    $submit_unemployed = $mysqli->query("INSERT INTO work_experiences_unemployed (graduate_id, unemployed_date, unemployed_reasons) VALUES ($graduate_id, '".$_POST['unemployed_date']."', '".$_POST['unemployed_reasons']."')");
                    if (!$submit_unemployed) {
                        $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                        $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                        $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                        $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                        $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                        echo "<b>Error:</b> In unemployed info. " . $mysqli->error;
                    }
                }
                
                /* Questions */
                $submit_question_answers = $mysqli->query("INSERT INTO alumni_questions (graduate_id, job_satisfy, job_satisfy_reason, reasons_present_job, benefits_from_job) VALUES ($graduate_id, '$job_satisfy', '$job_satisfy_reasons', '$reasons_why_accept_present_job', '$benefits_from_job')") or die(mysqli_error($mysqli));
                if (!$submit_question_answers) {
                    $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                    $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                    $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                    $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                    echo "<b>Error:</b> In questions answers. " . $mysqli->error;
                }
                
                /* Default avatar */
                $submit_avatar = $mysqli->query("INSERT INTO profile_pictures (graduate_id, dp_link) VALUES ($graduate_id, 'photos/dp/user.png')") or die(mysqli_error($mysqli));
                if (!$submit_avatar) {
                    $delete_account = $mysqli->query("DELETE FROM alumni_accounts WHERE graduate_id = $graduate_id");
                    $delete_personalInfo = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                    $delete_educational = $mysqli->query("DELETE FROM alumni_educational_background WHERE graduate_id = $graduate_id");
                    $delete_alumni_questions  = $mysqli->query("DELETE FROM alumni_questions WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_employed = $mysqli->query("DELETE FROM work_experiences_employed WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_self_employed = $mysqli->query("DELETE FROM work_experiences_self_employed WHERE graduate_id = $graduate_id");
                    $delete_work_experiences_unemployed = $mysqli->query("DELETE FROM work_experiences_unemployed WHERE graduate_id = $graduate_id");
                    echo $mysqli->error;
                }
                
                if ($submit_accntInfo && $submit_personalInfo && $submit_question_answers && $submit_avatar) {
                    header("Location: success.php");
                }
        }
    }
    }
?>