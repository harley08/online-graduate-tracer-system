<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Smilies</title>
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/msgs.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="images/favicon.png" />
        <style>
            td {
                padding: 10px;
            }
        </style>
    </head>
    <body>
        <?php include 'header.php'; ?>
        <div class="container" style="padding-top: 10px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title"><img src="images/smilies/smile.png" width="15px" /> Smilies</h5>
                </div>
                <div class="panel-body">
                    <table width="100%">
                        <tr>
                            <td width="50%" align="right">
                                <b>Smiley</b>
                            </td>
                            <td width="50%" align="left">
                                <b>Code</b>
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/angel.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [angel]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/angry.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [angry]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/arrow.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                ->
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/at.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                @
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/biggrin.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [biggrin]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/blush.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                *^_^*
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/confused.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                :-s
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/cool.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                B-)
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/cry.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [cry]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/dodgy.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [dodgy]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/exclamation.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [!]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/heart.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                <3
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/huh.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [huh]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/lightbulb.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [lightbulb]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/my.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [my]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/rolleyes.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [rolleyes]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/sad.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                :(
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/shy.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [shy]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/sick.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [sick]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/sleepy.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [sleepy]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/smile.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                :)
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/tongue.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                :P
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/undecided.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [undecided]
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <img src="images/smilies/wink.png" alt=""/>
                            </td>
                            <td width="50%" align="left">
                                [wink]
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <?php include 'footer.php'; ?>
        <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/popover.js" type="text/javascript"></script>
    </body>
</html>
