<?php
    include '../../config.php';
    if ($not_logged) {
        header("Location: ../../login/?alert=not_logged");
    }
    if (isset($_GET['id'])) {        
        
        $event_id = $_GET['id'];
        
        /* Event data */
        $event = $mysqli->query("SELECT * FROM events WHERE event_id = $event_id");
        $event_data = $event->fetch_assoc();  
        
        /* Get the user who posted the event */
        $poster = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = ".$event_data['graduate_id']."");
        $poster_data = $poster->fetch_assoc();
                            
        /* Get the poster dp */
        $poster_dp = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = ".$event_data['graduate_id']." ORDER BY dp_id DESC");
        $poster_dp_data = $poster_dp->fetch_assoc();
        
        /* Count all participants */
        $count_participants = $mysqli->query("SELECT * FROM graduates WHERE graduate_batch = '".$event_data['graduate_batch']."'");
        $countParticipants = $count_participants->num_rows;
        
        /* Count comments */
        $count_comments = $mysqli->query("SELECT * FROM events_comments WHERE event_id = $event_id");
        $countComments = $count_comments->num_rows;
        
        /* Check if the user is the participant of the event */
        $check_if_participant = $mysqli->query("SELECT * FROM graduates WHERE graduate_batch = '".$event_data['graduate_batch']."' AND graduate_id = $user_graduate_id");
        $count_if_participant = $check_if_participant->num_rows;
        $event_participant = $count_if_participant >= 1 || $user_graduate_id = $event_data['graduate_id'];
    } else if (!isset ($_GET['id'])) {
        header("Location: ../index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $event_data['event_title'] ?></title>
        <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../../images/favicon.png" />
        <style>
            .comments {
                background-color: #ffffff;
                padding: 10px;
                margin-bottom: 10px;
            }
            .comment {
                padding: 10px;
                border-bottom: 1px solid #ecebeb;
            }
        </style>
    </head>
    <body>
        <?php include '../../header.php'; ?>
        <div id="tags" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" aria-hidden="true" data-dismiss="modal">x</button>
                        <h5 class="modal-title">Participants (<b><?php echo $countParticipants ?></b>)</h5>
                    </div>
                    <div class="modal-body">
                        <?php
                            $participants = $mysqli->query("SELECT * FROM graduates WHERE graduate_batch = '".$event_data['graduate_batch']."'");
                            while($participants_data =  $participants->fetch_assoc()) {
                                $participant_dp_link = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = '".$participants_data['graduate_id']."' ORDER BY dp_id DESC");
                                $participant_dp_link_data = $participant_dp_link->fetch_assoc();
                                ?>
                        <img src="<?php echo $base_url ?>/profile/<?php echo $participant_dp_link_data['dp_link'] ?>" width="15px" class="shadow" style="margin: 0px !important; border: 2px solid #ffffff;" /> <b><?php echo $participants_data['graduate_firstname'] . ' ' . $participants_data['graduate_middlename'] . ' ' . $participants_data['graduate_surname'] . '<br>'; ?></b>
                            <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="padding-top: 10px;">
            <div class="panel panel-default">
                <div class="event-cover-img">                       
                    <span style="position: absolute; margin-left: 20px; padding-top: 130px;"><h3><font style="color: #606060;"><span class="glyphicon glyphicon-calendar"></span> <?php echo $event_data['event_title'] ?></font> - <?php echo $event_data['event_date'] ?></h3></span>
                    <img src="../../images/event-cover-img.png" width="100%" />
                </div>
                <div class="panel-body">
                    <div style="margin-bottom: 10px;">
                        <img src="<?php echo $base_url ?>/profile/<?php echo $poster_dp_data['dp_link'] ?>" width="30px" class="shadow" style="margin: 0px !important; border: 2px solid #ffffff;" /> <b><?php echo $poster_data['alumn_firstname'] . ' ' . $poster_data['alum_middlename'] . ' ' . $poster_data['alum_surname']; ?></b>
                    </div>
                    <?php echo $event_data['event_description'] ?>
                </div>
                <div class="panel-footer">
                    Comments <b><?php echo $countComments ?></b> <a data-toggle="modal" data-target="#tags">Event Participants <b><?php echo $countParticipants ?></b></a><br><br>
                    <div class="comments">
                        <?php
                            if ($countComments == null) { ?>
                        <center>no comments yet</center>
                            <?php }
                        ?>
                        <?php
                            /* limits of comments to be displayed */
                            $comment_display_limit = 10;
                            
                            /* if the button page is triggered */
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'] + 1;
                                $offset = $comment_display_limit * $page;
                            } else {
                                $page = 0;
                                $offset = 0;
                            }    
                            
                            $left_comments = $countComments - ($page * $comment_display_limit); 
                            if ($left_comments < $comment_display_limit) {
                                $comment_display_limit = $left_comments;
                            }
                            ?>
                        <?php
                        if ($left_comments > $comment_display_limit) {
                            if ($page > 0) { 
                                $last = $page - 2?>
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>&page=<?php echo $last ?>">view previous</a>
                            </center>
                        </div> 
                            <?php } } else if ($left_comments < $comment_display_limit) { ?>
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>">view from the top</a>
                            </center>
                        </div>
                             <?php } else if ($left_comments == $comment_display_limit && $countComments != 0) { 
                                 $last = $page - 2?>
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>&page=<?php echo $last ?>">view previous</a>
                            </center>
                        </div>
                             <?php }
                        ?>
                            <?php 
                            $comments = $mysqli->query("SELECT * FROM events_comments WHERE event_id = $event_id ORDER BY comment_id ASC LIMIT $offset, $comment_display_limit") or die($mysqli->error);
                            
                            while ($comments_data = $comments->fetch_assoc()) {
                                /* get the user profile link */
                                $profile_link_comment = $mysqli->query("SELECT * FROM profile_links WHERE graduate_id = ".$comments_data['graduate_id']."");
                                $data_profile_link_comment = $profile_link_comment->fetch_assoc();
                                
                                $user_commented = $mysqli->query("SELECT * FROM graduates WHERE graduate_id = ".$comments_data['graduate_id']."");
                                $data_user_commented = $user_commented->fetch_assoc();
                                
                                $dp_commented_user = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = '".$comments_data['graduate_id']."' ORDER BY dp_id DESC");
                                $data_dp_commented_user = $dp_commented_user->fetch_assoc();
                                ?>
                        <div class="comment">
                            <img src="<?php echo $base_url ?>/profile/<?php echo $data_dp_commented_user['dp_link'] ?>" width="20px" class="shadow" style="margin: 0px !important; border: 2px solid #ffffff;" /> <b><a href="<?php echo $data_profile_link_comment['alumn_profile_hash_link'] ?>"><?php echo $data_user_commented['graduate_firstname'] . ' ' . $data_user_commented['graduate_middlename'] . ' ' . $data_user_commented['graduate_surname'] ?></a></b> <small><?php echo date("F j, Y h:i A", strtotime($comments_data['comment_date'])) ?></small><br>
                            <div style="padding-left: 27px; margin-top: 5px;">
                                <?php if ($comments_data['comment_photo_link'] != null) { ?>
                                <img src="<?php echo $base_url ?>/<?php echo $comments_data['comment_photo_link'] ?>" width="200px" style="margin-bottom: 5px;" /><br>
                                <?php } ?>
                                <?php echo $comments_data['comment_msg'] ?>
                            </div>
                        </div>
                            <?php }
                        ?>
                        <?php
                        if ($left_comments > $comment_display_limit) {
                            if ($page == 0) {
                                ?>                        
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>&page=<?php echo $page ?>">see more...</a>
                            </center>
                        </div>
                            <?php } else if ($page > 0 ) { ?>
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>&page=<?php echo $page ?>">see more...</a>
                            </center>
                        </div>   
                        <?php } } else if ($left_comments < $comment_display_limit) {
                               $last = $page - 2;?>
                        <div class="comment">
                            <center>
                                <a href="?id=<?php echo $event_id ?>&page=<?php echo $last ?>">view previews...</a>
                            </center>
                        </div>       
                           <?php }
                        ?>
                    </div>
                    <?php
                        if ($event_participant) {
                    ?>
                    <form method="post" action="comment.php?event_id=<?php echo $event_id ?>&<?php echo $event_data['event_title'] ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Quick Comment:</label>
                            <textarea style="margin-bottom: 5px;" class="form-control" name="comment"></textarea>
                            <input type="file" name="comment_photo" />
                        </div>
                        <button type="submit" class="btn btn-primary">Comment</button>
                    </form>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php include '../../footer.php'; ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
