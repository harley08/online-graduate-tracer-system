<?php
    include '../../config.php';
    if (isset($_POST['event_title'])) {
        $event_title = $_POST['event_title'];
        $event_description = $_POST['event_description'];
        $event_date = $_POST['event_date'];
        $batch = $_POST['batch'];
        
        $post_event = $mysqli->query("INSERT INTO events (graduate_id, event_title, event_description, event_date, graduate_batch) VALUES ($user_graduate_id, '$event_title', '$event_description', '$event_date', '$batch')");
        if ($post_event) {            
            $date_posted = date("F j, Y", strtotime($event_date));
            $events = $mysqli->query("SELECT * FROM events ORDER BY event_id DESC");
            $event_data = $events->fetch_assoc();
            $event_id = $event_data['event_id'];
            $post = '<div class="panel panel-default" style="margin-bottom: 0px !important;"><div class="panel-body"><b>'.$logged_fullname.'</b> added new event entitled as <a href="'.$base_url.'/events/action/view-event.php?id='.$event_id.'">'.$event_title.'</a> to be conducted on '.$date_posted.'.</div></div>';
            $post_shout = $mysqli->query("INSERT INTO shouts (graduate_id, shout_msg, shout_date) VALUES ($user_graduate_id, '$post', '$event_date')");
            if ($post_shout) {
                header("Location: ../");
            } else {
             echo $mysqli->error;   
            }
        } else if (!$post_event) {
            echo $mysqli->error;
        }
    }
?>