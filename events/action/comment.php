<?php
    include '../../config.php';
    if (isset($_GET['event_id'])) {
        $event_id = $_GET['event_id'];
        $comment_msg = $_POST['comment'];
        $date = date("Y-m-d H:i:s");
        
        if (isset($_FILES['comment_photo'])) {
            move_uploaded_file($_FILES['comment_photo']['tmp_name'], "../../images/events_photos/" . $_FILES['comment_photo']['name']);
            $photo_link = "";
            if ($_FILES['comment_photo']['name'] == null) {
                $photo_link = "";
            } else {
                $photo_link = "images/events_photos/" . $_FILES['comment_photo']['name'];
            }
            $comment = $mysqli->query("INSERT INTO events_comments (event_id, graduate_id, comment_msg, comment_date, comment_photo_link) VALUES ($event_id, $user_graduate_id, '$comment_msg', '$date', '$photo_link')");
            if ($comment) {
                header("Location: view-event.php?id=".$event_id."");
            }
        }
    } else if (!isset ($_GET['event_id'])) {
        header("Location: ../index.php");
    }
?>