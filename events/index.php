<?php
    include '../config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Events</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="../assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../images/favicon.png" />
        <style>
            .event-cover-img {
                
            }
            .panel-hovered {
                
            }
            .panel-hovered:hover {
                border-color: #66afe9;
                outline: 0;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
            }
        </style>
    </head>
    <body>
        <?php include '../header.php'; ?>
        <div id="postEvent" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h5 class="modal-title"><span class="glyphicon glyphicon-calendar"></span> Post New Event</h5>
                    </div>
                    <form method="post" action="action/post-event.php">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Title:</label>
                                <input type="text" name="event_title" class="form-control" placeholder="type your title here..." required />
                            </div>
                            <div class="form-group">
                                <label>Description:</label>
                                <textarea name="event_description" class="form-control" required></textarea>
                            </div>
                            <div class="form-group">
                                <label>Date of event:</label>
                                <input type="date" name="event_date" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label>Batch involved:</label>
                                <select name="batch" class="form-control" required>
                                    <option value="">-- Choose Batch --</option>
                                    <?php
                                        $batch = $mysqli->query("SELECT * FROM batch_folders");
                                        $count_batch = $batch->num_rows;
                                        for ($i = 1; $i <= $count_batch; $i++) {
                                            $data_batch_involved = $batch->fetch_assoc();
                                            echo '<option value="'.$data_batch_involved['batch_year'].'">'.$data_batch_involved['batch_year'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="reset" class="btn btn-danger">Reset</button>
                                <button type="submit" class="btn btn-primary">Post</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container" style="padding-top: 10px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="modal" data-target="#postEvent" class="btn btn-default pull-right">
                        <small>
                            <span class="glyphicon glyphicon-calendar"></span> Post an Event
                        </small>
                    </a>
                    <h5 class="panel-title">
                        <span class="glyphicon glyphicon-calendar"></span> Events
                    </h5>
                </div>
                <div class="panel-body">                    
                    <?php
                        $events = $mysqli->query("SELECT * FROM events ORDER BY event_id DESC");
                        while ($events_data = $events->fetch_assoc()) { 
                            /* Get the user who posted the event */
                            $poster = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = ".$events_data['graduate_id']."");
                            $poster_data = $poster->fetch_assoc();
                            
                            /* Get the poster dp */
                            $poster_dp = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = ".$events_data['graduate_id']." ORDER BY dp_id DESC");
                            $poster_dp_data = $poster_dp->fetch_assoc();
                            ?>                    
                    <div class="panel panel-default panel-hovered shadow">
                        <div class="event-cover-img">                       
                            <span style="position: absolute; margin-left: 20px; padding-top: 130px;"><h3><font style="color: #606060;"><span class="glyphicon glyphicon-calendar"></span> <a href="action/view-event.php?id=<?php echo $events_data['event_id']  ?>&?<?php echo $events_data['event_title'] ?>"><?php echo $events_data['event_title'] ?></a></font> - <?php echo date("F j, Y", strtotime($events_data['event_date'])); ?></h3></span>
                            <img src="../images/event-cover-img.png" width="100%" />
                        </div>
                        <div class="panel-body">
                            <div style="margin-bottom: 10px;">
                                <img src="<?php echo $base_url ?>/profile/<?php echo $poster_dp_data['dp_link'] ?>" width="30px" class="shadow" style="margin: 0px !important; border: 2px solid #ffffff;" /> <b><?php echo $poster_data['alumn_firstname'] . ' ' . $poster_data['alum_middlename'] . ' ' . $poster_data['alum_surname']; ?></b>
                            </div>
                            <?php echo $events_data['event_description'] ?>
                        </div>
                        <div class="panel-footer">
                            <b>Batch Involved:</b> <?php echo $events_data['graduate_batch'] ?> | <b>0</b> Total Discussions
                        </div>
                    </div>  
                        <?php }
                    ?>
                </div>
            </div>
        </div>
        <?php include '../footer.php'; ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
