<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Congrad</title>
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        <link href="assets/css/sticky-footer-navbar.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="assets/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <script src="assets/fancybox/jquery.fancybox.js" type="text/javascript"></script>
        <link rel="icon" href="images/favicon.png" />
        <script type="text/javascript">
            $("[data-fancybox]").fancybox({ });
        </script>
    </head>
    <body>
        <style>
            body {
                background-color: #eeeeee !important;
            }
        </style>
        <?php include 'header.php'; ?>
        <div class="jumbotron" style="padding-top: 10px !important;<?php if ($not_logged) { echo 'background-color: #ffffff !important; padding-bottom: 20px !important;'; } ?> margin-bottom: 0px !important; <?php if ($logged) { echo 'padding-bottom: 0px !important;'; } ?>">
            <div class="container" style="margin-top: 0px !important; margin-bottom: 0px !important; padding-bottom: 0px !important;">
                    <?php
                        if ($logged) { ?>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="panel-ava" align="center">
                                <img width="200px" src="<?php echo $base_url . '/profile/' . $user_dp_link ?>" class="panel-ava-img">
                                <div class="panel-ava-menu">
                                    <a href="<?php echo $user_profile_link ?>"><?php echo $logged_fullname ?></a>
                                </div>
                            </div>
                            <?php include 'sidebars/default-sidebar-left.php'; ?>
                        </div>
                        <div class="col-md-9">
                            <div class="panel-box">
                                <h5 style="margin: 0px;"><span class="glyphicon glyphicon-list"></span> News Feed</h5>
                            </div>
                            <div class="panel panel-default shadow" style="border-radius: 0px;">
                                <ul class="breadcrumb" style="border-radius: 0px; padding: 15px; margin: 0px;">
                                    <li><span class="glyphicon glyphicon-share"></span> Shout</li>
                                </ul>
                                <div class="panel-body" style="padding-bottom: 0px; border-bottom: 1px solid #efeeee;">
                                    <?php
                                        if (isset($_POST['shout'])) {
                                            
                                            $msg_shout = $_POST['shout'];
                                            $date_shout = date("Y-m-d H:i:s");
                                            
                                            $shout = $mysqli->query("INSERT into shouts (graduate_id, shout_msg, shout_date) VALUES ($user_graduate_id, '$msg_shout', '$date_shout')");
                                            if ($shout) {
                                            }
                                        }
                                    ?>
                                    <!-- Start modal add photo -->
                                    <div id="addPhoto" class="modal fade" tabindex="-1" ariaLabelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                                    <h5 class="modal-title"><span class="glyphicon glyphicon-camera"></span> Add Photo</h5>
                                                </div>
                                                <form method="post" action="post-photo.php" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    <input type="file" name="photo" /><br>
                                                    <textarea class="form-control" name="photo_shout"></textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="btn-group">
                                                        <button type="reset" class="btn btn-danger">Reset</button>
                                                        <button type="submit" class="btn btn-primary">Post</button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End modal add photo -->
                                    <form role="form" action="" method="post">
                                        <div class="input-group">
                                            <textarea name="shout" class="form-control" style="height: 57px;"></textarea>
                                            <div class="input-group-btn">
                                                <button style="padding: 18px;" type="submit" class="btn btn-primary">Shout</button>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin: 5px;">
                                            <a data-toggle="modal" data-target="#addPhoto"><span class="glyphicon glyphicon-camera"></span> Add Photo</a> | <a href="smilies.php"><img src="images/smilies/smile.png" width="15px" /> Smilies</a>
                                        </div>
                                    </form>
                                </div>
                                <div class="panel-body" style="padding-bottom: 0px;">
                                    <?php include 'shouts/shouts-list-index.php'; ?>
                                </div>
                                <div class="panel-footer">
                                    <?php
                                        if ($page == 0) { ?>
                                    <a data-loading-text="Loading..." class="btn btn-default btn-block js-loading-button" href="?page=<?php echo $page ?>">See More</a>
                                        <?php } else if ($page > 0) { ?>
                                    <a data-loading-text="Loading..." class="btn btn-default btn-block js-loading-button" href="?page=<?php echo $page ?>">See More</a>        
                                        <?php } else if ($left_shouts < $shout_display_limit) { ?>
                                            
                                        <?php }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                        <?php }
                    ?>
                    <?php
                    if ($not_logged) { ?>
                <div class="row" style="margin-top: 10px;">
                        <div class="col-md-9">
                            <style>
                                h3 {
                                    margin-bottom: ss10px;
                                }
                            </style>
                            <h3>Dear MBC Graduates:</h3>
                            <h3>Good Day!</h3>
                            <p>
                                The Mindoro State College of Agriculture and Technology Bongabong Campus is conducting a study on Employability Status of Graduates. Relative to this, may we request for your kind assistance and suppport to fill in the information needed in this survey instrument.
                            </p>
                            <p>
                                Information that will be gathered will be highly appreciated and which will in turn contribute much to the realization of determining the applicability and effectiveness of the course you have finished to your present work. Rest assured that all your answers are confidential.
                            </p>
                            <p>
                                Your cooperation is highly appreciated. Thank you and God Bless!
                            </p>
                            <p>
                                Very truly yours,
                            </p>
                            <p>MBC Guidance Counselor</p>
                            <a href="form/" class="btn btn-success" role="button" >
                                Continue &rightarrow;
                            </a>
                        </div>
                        <div class="col-md-3 pc-view">
                            <h5><span class="glyphicon glyphicon-calendar"></span> Events</h5>
                            <ul>
                                <?php
                                    $events = $mysqli->query("SELECT * FROM events ORDER BY event_date ASC");
                                    $count_events = $events->num_rows;
                                    if ($count_events == null) { ?>
                                <li style="padding: 10px;">
                                <center>no events posted yet</center>
                                </li>
                                    <?php }
                                    while($event_data = $events->fetch_assoc()) { ?>
                                <li style="margin-bottom: 10px;">
                                    <span class="glyphicon glyphicon-calendar"></span> <?php echo date("F j, Y", strtotime($event_data['event_date'])) ?> <a href="<?php echo $base_url ?>/events/action/view-event.php?id=<?php echo $event_data['event_id'] ?>&<?php echo $event_data['event_title'] ?>"><?php echo $event_data['event_title'] ?></a>
                                </li>
                                    <?php }
                                ?>
                            </ul>
                            <br>
                            <h5><span class="glyphicon glyphicon-user"></span> Alumni <b><?php $count = $mysqli->query("SELECT * FROM alumni_accounts WHERE graduate_id != 1 ORDER BY account_id ASC"); $all_user = $count->num_rows; echo $all_user; ?></b></h5>
                            <div class="nav nav-pills nav-stacked nav-email mb-20 rounded shadow" style="background-color: #ffffff; padding: 10px;">
                                <?php
                                    $dp = $mysqli->query("SELECT * FROM alumni_accounts WHERE graduate_id != 1 ORDER BY account_id ASC");
                                    $dp_count = $dp->num_rows;
                                    for ($i = 1; $i <= $dp_count; $i++) {
                                        $dp_data = $dp->fetch_assoc();
                                        $photo_graduate_id = $dp_data['graduate_id'];
                                        
                                        /* Avatar link */
                                        $get_photo = $mysqli->query("SELECT * FROM profile_pictures WHERE graduate_id = $photo_graduate_id ORDER BY dp_id DESC");
                                        $photo_data = $get_photo->fetch_assoc();                                        
                                        $dp_link = $photo_data['dp_link'];
                                        
                                        /* Get full name */
                                        $query_fullname = $mysqli->query("SELECT * FROM alumni_personal_info WHERE graduate_id = $photo_graduate_id");
                                        $fullname_data = $query_fullname->fetch_assoc();
                                        $surname = $fullname_data['alum_surname'];
                                        $firstname = $fullname_data['alumn_firstname'];
                                        $middlename = $fullname_data['alum_middlename'];
                                        $fullname = $firstname . ' ' . $middlename . ' ' . $surname;
                                        ?>
                                <a data-toggle="tooltip" data-placement="top" title="<?php echo $fullname ?>">
                                    <img src="<?php echo $base_url ?>/profile/<?php echo $dp_link ?>" width="30px" height="30px" />
                                </a>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>                    
                    <?php }
                    ?>
                </div>
            </div>
        <?php
        if ($not_logged)
        { ?>
        <div class="container" style="margin-top: 0px !important; margin-bottom: 0px !important; padding: 20px !important; padding-bottom: 5px !important;">
            <div class="row">
                <div class="col-xs-2 pc-view">
                    <img src="images/minscat logo transparent.png" alt="MinSCAT Logo" width="150px" />
                </div>
                <div class="col-xs-4 col-lg-4 col-space-left">
                    <b><span class="glyphicon glyphicon-education"></span> MinSCAT</b><br><br>
                    <p>
                        The Mindoro State College of Agriculture and Technology believes in the supremacy of God over His creation, and that man as His special creation has the capacity to learn and can be developed physically, mentally, socially and spiritually.
                    </p>
                </div>
                <div class="col-xs-3 col-lg-3">
                    <b><span class="glyphicon glyphicon-phone"></span> Contact</b><br><br>
                    <b>Tel #:</b> (043)-283 5570<br>
                    <b>Email:</b> minscat_bongabong@yahoo.com<br>
                    <b>Address:</b> Labasan, Bongabong, Oriental Mindoro<br>
                </div>
                <div class="col-xs-3 col-lg-3">
                    <b><span class="glyphicon glyphicon-user"></span> Want a job?</b> Find the latest hiring <a href="/jobportal/">here.</a><br><br>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php include 'footer.php'; ?>
        <script>
            $(function () {
            $('.js-loading-button').on('click', function () {
            var btn = $(this).button('loading')
            setTimeout(function (){
            btn.button('reset')
            }, 10000)
            })
            })
            
            $(function () {
            $('.js-popover').popover()
            })
            
            $(function () {
            $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/popover.js" type="text/javascript"></script>
    </body>
</html>